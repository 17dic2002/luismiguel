// Script to display other fields id other is selected from the dropdown. Only apply to Deparment and Certificate Degree

$("#department").change(function () {
    if ($("#department").val() == 'Other') {
        $("#inputOtherDept").prop("disabled", false);
    } else {
        $("#inputOtherDept").prop("disabled", true);
    }
});

function checkIfOtherDegree(id) {
    if (document.getElementById('inputDegree' + id).value == 'Other') {
        document.getElementById('otherDegree' + id).disabled = false;
    } else {
        document.getElementById('otherDegree' + id).disabled = true;
    }
}

$("#inputhearaboutus").change(function () {
    if ($("#inputhearaboutus").val() == 'Employee of the Company' )  {
        $("#inputotherhear").prop("disabled", false);
    } else if($("#inputhearaboutus").val() == 'Other' ) {
    	$("#inputotherhear").prop("disabled", false);
    }
    else {
        $("#inputotherhear").prop("disabled", true);
    }
});

