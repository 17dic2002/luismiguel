<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Educations;
use Faker\Generator as Faker;

$factory->define(Educations::class, function (Faker $faker) {

    return [
        'institute' => $faker->word,
        'study_area' => $faker->word,
        'certificate' => $faker->word,
        'other' => $faker->word,
        'date_from' => $faker->word,
        'date_to' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'application_id' => $faker->randomDigitNotNull
    ];
});
