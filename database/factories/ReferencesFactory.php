<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\References;
use Faker\Generator as Faker;

$factory->define(References::class, function (Faker $faker) {

    return [
        'name_referee' => $faker->word,
        'job_title' => $faker->word,
        'organization' => $faker->word,
        'relation' => $faker->word,
        'phone_number' => $faker->word,
        'address' => $faker->word,
        'application_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
