<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\WorkExperiences;
use Faker\Generator as Faker;

$factory->define(WorkExperiences::class, function (Faker $faker) {

    return [
        'employer_name' => $faker->word,
        'position' => $faker->word,
        'responsabilities' => $faker->word,
        'supervisors_name' => $faker->word,
        'position_start_date' => $faker->word,
        'position_end_date' => $faker->word,
        'reason_leaving' => $faker->word,
        'phone' => $faker->word,
        'addresss' => $faker->word,
        'application_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
