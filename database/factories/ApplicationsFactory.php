<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Applications;
use Faker\Generator as Faker;

$factory->define(Applications::class, function (Faker $faker) {

    return [
        'application_title' => $faker->word,
        'first_name' => $faker->word,
        'middle_name' => $faker->word,
        'last_name' => $faker->word,
        'email' => $faker->word,
        'home_phone' => $faker->word,
        'mobile_number' => $faker->word,
        'gender' => $faker->word,
        'category' => $faker->word,
        'other_category' => $faker->word,
        'type' => $faker->word,
        'years_of_experience' => $faker->word,
        'date_of_birth' => $faker->word,
        'currency' => $faker->word,
        'current_salary' => $faker->word,
        'period_cs' => $faker->word,
        'currency_expected' => $faker->word,
        'expected_salary' => $faker->word,
        'period_es' => $faker->word,
        'driver_license' => $faker->word,
        'address' => $faker->word,
        'resume' => $faker->word,
        'list_skills' => $faker->word,
        'professional_qualifications_skills' => $faker->word,
        'other_relevant_skills' => $faker->word,
        'hobbies' => $faker->word,
        'how_hear' => $faker->word,
        'tag' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'job_id' => $faker->randomDigitNotNull
    ];
});
