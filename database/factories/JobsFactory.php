<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Jobs;
use Faker\Generator as Faker;

$factory->define(Jobs::class, function (Faker $faker) {

    $locations = array('Corporate Office', 'Anguila', 'Antigua & Barbuda', 'Bahamas', 'Barbados', 'Belize', 'Bermuda', 'BVI', 'Cayman Island', 'Dominica', 'Dominican Republic', 'French Guiana', 'Grenada', 'Guadelope', 'Guyana', 'Jamaica', 'Martinique', 'Puerto Rico', 'St. Kitts & Nevis', 'St. Lucia', 'St. Maarten', 'St. Vincent & The Grenadines', 'Suriname');
    $index = array_rand($locations);

    return [
        'title' => $faker->word,
        'location' => $locations[$index],
        'status' => rand(0, 1) ? 'Active' : 'Inactive',
        'description' => 'blank.pdf',
        'stop_publish' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'start_publish' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
