<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Languages;
use Faker\Generator as Faker;

$factory->define(Languages::class, function (Faker $faker) {

    return [
        'language' => $faker->word,
        'understanding' => $faker->word,
        'writing' => $faker->word,
        'reading' => $faker->word,
        'where_learn' => $faker->word,
        'application_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
