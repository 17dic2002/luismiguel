<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\RequestAds;
use Faker\Generator as Faker;

$factory->define(RequestAds::class, function (Faker $faker) {

    return [
        'requester_name' => $faker->word,
        'requester_email' => $faker->word,
        'print_add' => $faker->word,
        'name_publication' => $faker->word,
        'ad_size' => $faker->word,
        'date_publication' => $faker->word,
        'template_print' => $faker->word,
        'comments_template' => $faker->word,
        'book_media' => $faker->word,
        'due_date' => $faker->word,
        'blue_print' => $faker->word,
        'budget' => $faker->word,
        'comments_bp' => $faker->word,
        'social_media' => $faker->word,
        'platforms' => $faker->word,
        'other_platform' => $faker->word,
        'template_social' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'job_id' => $faker->randomDigitNotNull
    ];
});
