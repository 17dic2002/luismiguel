<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequestAdsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string('requester_name');
            $table->string('requester_email');
            $table->string('print_add');
            $table->string('name_publication')->nullable();
            $table->string('ad_size')->nullable();
            $table->dateTime('date_publication')->nullable();
            $table->string('template_print')->nullable();
            $table->string('comments_template')->nullable();
            $table->string('book_media')->nullable();
            $table->string('due_date')->nullable();
            $table->string('blue_print')->nullable();
            $table->string('budget')->nullable();
            $table->string('comments_bp')->nullable();
            $table->string('social_media');
            $table->string('platforms')->nullable();
            $table->string('other_platform')->nullable();
            $table->string('template_social')->nullable();
            $table->integer('job_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('job_id')->references('id')->on('jobs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('request_ads');
    }
}
