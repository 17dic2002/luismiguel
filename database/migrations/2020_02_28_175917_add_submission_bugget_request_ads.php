<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSubmissionBuggetRequestAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_ads', function (Blueprint $table) {
            $table->string('submission_deadline')->nullable()->after('template_print')->nullable();
            $table->string('submission_deadline2')->nullable()->after('template_print')->nullable();
            $table->string('submission_deadline3')->nullable()->after('template_print')->nullable();
            $table->string('budget_social')->nullable()->after('template_print')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
