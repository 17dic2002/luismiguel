<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkExperiencesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employer_name');
            $table->string('position');
            $table->string('responsabilities');
            $table->string('supervisors_name');
            $table->date('position_start_date');
            $table->date('position_end_date');
            $table->string('reason_leaving');
            $table->string('phone');
            $table->string('address');
            $table->integer('application_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('application_id')->references('id')->on('applications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('work_experiences');
    }
}
