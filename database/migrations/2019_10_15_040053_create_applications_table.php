<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('application_title');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('home_phone');
            $table->string('mobile_number');
            $table->string('gender')->nullable();
            $table->string('category');
            $table->string('other_category')->nullable();
            $table->string('type');
            $table->string('years_of_experience')->nullable();
            $table->date('date_of_birth');
            $table->string('currency');
            $table->string('current_salary');
            $table->string('period_cs');
            $table->string('currency_expected');
            $table->string('expected_salary');
            $table->string('period_es');
            $table->string('driver_license');
            $table->string('address');
            $table->string('resume')->nullable();
            $table->string('list_skills')->nullable();
            $table->string('professional_qualifications_skills')->nullable();
            $table->string('other_relevant_skills')->nullable();
            $table->string('hobbies')->nullable();
            $table->string('how_hear')->nullable();
            $table->string('tag')->nullable();
            $table->integer('step')->nullable();
            $table->integer('job_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('job_id')->references('id')->on('jobs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applications');
    }
}
