<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsRequestAds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::table('categories', function (Blueprint $table) {
             //$table->increments('id')->change();
         });*/

        Schema::table('request_ads', function (Blueprint $table) {
            $table->string('orientation')->nullable()->after('template_print');
            $table->string('colour')->nullable()->after('template_print');
            $table->dateTime('date_finish')->nullable()->after('date_publication');


            $table->string('name_publication_2')->nullable();
            $table->string('ad_size_2')->nullable();
            $table->dateTime('date_publication_2')->nullable();
            $table->dateTime('date_finish_2')->nullable();
            $table->string('template_print_2')->nullable();
            $table->string('orientation_2')->nullable();
            $table->string('colour_2')->nullable();
            $table->string('comments_template_2')->nullable();

            $table->string('name_publication_3')->nullable();
            $table->string('ad_size_3')->nullable();
            $table->dateTime('date_publication_3')->nullable();
            $table->dateTime('date_finish_3')->nullable();
            $table->string('template_print_3')->nullable();
            $table->string('orientation_3')->nullable();
            $table->string('colour_3')->nullable();
            $table->string('comments_template_3')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_ads', function (Blueprint $table) {
        });
    }
}
