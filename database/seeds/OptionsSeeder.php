<?php

use Illuminate\Database\Seeder;

class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = \App\Models\Options::create([
            'admin_email' => 'demo@demo.com',
            'digital_ad_email' => 'demo@demo.com',
            'traditional_ad_email' => 'demo@demo.com'
        ]);
    }
}
