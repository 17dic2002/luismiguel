<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Front Routes
 */
Route::get('/', 'FrontController@indexFront')->name('/');
Route::get('/home', 'FrontController@indexFront')->name('home');

Route::get('careers', 'FrontController@careers')->name('careers');

Route::get('careers/internal', 'FrontController@careersInternal')->name('careers-internal');


Route::get('viewJob/{id}', 'FrontController@viewJob')->name('viewJob');
Route::get('thankyou', 'FrontController@thankyou')->name('thankyou');


/**
 * Should be authenticated to apply a job
 */

Route::group(['middleware' => ['auth']], function () {
    //Personal Information
    Route::get('applyForm/{id}/personal-information', 'FrontController@applyForm')->name('applyForm');
    Route::post('applyFormPost/{id}/personal-information', 'FrontController@applyFormPost')->name('applyFormPost');
    Route::post('applyForm/{id}/personal-information', 'FrontController@savePersonalInformation')->name('application.storePI');
    Route::patch('applyForm/{id}/personal-information', 'FrontController@updatePersonalInformation')->name('application.updatePI');

    //Education
    Route::get('applyForm/application/{id}/education', 'FrontController@applyFormEducation')->name('applyForm.education');
    Route::post('applyForm/application/{id}/education', 'FrontController@saveEducation')->name('applyForm.saveEducation');
    Route::patch('applyForm/application/{id}/education', 'FrontController@updateEducation')->name('applyForm.updateEducation');


    //Experience
    Route::get('applyForm/application/{id}/experience', 'FrontController@applyFormExperience')->name('applyForm.experience');
    Route::post('applyForm/application/{id}/experience', 'FrontController@saveExperience')->name('applyForm.saveExperience');
    Route::patch('applyForm/application/{id}/experience', 'FrontController@updateExperience')->name('applyForm.updateExperience');

    //Skills
    Route::get('applyForm/application/{id}/skills', 'FrontController@applyFormSkills')->name('applyForm.skills');
    Route::post('applyForm/application/{id}/skills', 'FrontController@saveSkills')->name('applyForm.saveSkills');
    Route::patch('applyForm/application/{id}/skills', 'FrontController@updateSkills')->name('applyForm.updateSkills');

    //References
    Route::get('applyForm/application/{id}/references', 'FrontController@applyFormReferences')->name('applyForm.references');
    Route::post('applyForm/application/{id}/references', 'FrontController@saveReferences')->name('applyForm.saveReferences');
    Route::patch('applyForm/application/{id}/references', 'FrontController@updateReferences')->name('applyForm.updateReferences');

    //Languages
    Route::get('applyForm/application/{id}/languages', 'FrontController@applyFormLanguages')->name('applyForm.languages');
    Route::post('applyForm/application/{id}/languages', 'FrontController@saveLanguages')->name('applyForm.saveLanguages');
    Route::patch('applyForm/application/{id}/languages', 'FrontController@updateLanguages')->name('applyForm.updateLanguages');

    //Resume
    Route::get('applyForm/application/{id}/resume', 'FrontController@applyFormResume')->name('applyForm.resume');
    Route::post('applyForm/application/{id}/resume', 'FrontController@saveResume')->name('applyForm.saveResume');
    Route::patch('applyForm/application/{id}/resume', 'FrontController@updateResume')->name('applyForm.updateResume');


});


/**
 * Auth routes
 */
Auth::routes();


/**
 * Dashboard Routes, only admin can see
 */

Route::group(['middleware' => ['permission:dashboard']], function () {

    Route::resource('users', 'UsersController');

    Route::get('dashboard', 'HomeController@index')->name('dashboard');

    Route::resource('jobs', 'JobsController');

    Route::resource('applications', 'ApplicationsController');

    Route::resource('educations', 'EducationsController');

    Route::resource('references', 'ReferencesController');

    Route::resource('languages', 'LanguagesController');

    Route::resource('workExperiences', 'WorkExperiencesController');

    Route::resource('requestAds', 'RequestAdsController');

    Route::resource('options', 'OptionsController');

    Route::get('/requestAds/{id}','RequestAdsController@requestAd')->name('requestAd.create');

    //Add/show tags for reject, save and add to short list applications
    Route::get('applications/reject/{id}', 'ApplicationsController@reject')->name('reject');
    Route::get('applications/shortlist/{id}', 'ApplicationsController@addShortList')->name('shortList');
    Route::get('applications/saved/{id}', 'ApplicationsController@saveForLater')->name('saveForLater');


    Route::get('applications/saved', 'ApplicationsController@indexSavedForLater')->name('applications.indexSaved');
    Route::get('applications/shortlist', 'ApplicationsController@indexShortList')->name('applications.indexShortList');
    Route::get('applications/rejected', 'ApplicationsController@indexSavedForLater')->name('applications.indexRejected');



});

