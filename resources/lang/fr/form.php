<?php
/**
 * Created by Luis Miguel.
 * User: Luis Miguel
 * Date: 1/9/20
 * Time: 4:52 p. m.
 */
return [

    /* Common */
    'choose' => 'Choisir...',
    'next' => 'Prochain',
    'yes' => 'Oui',
    'saved' => ' Enregistré avec succès',
    'save'=>'Enregistré avec succès',
    'remove'=>'Retirer',
    'addMore' => 'Ajouter plus',
    'declaration_1' => "Je déclare que les informations fournies dans ce formulaire de candidature sont véridiques et complètes. Je comprends que si j'ai donné des informations trompeuses sur ce formulaire ou fait des omissions, ce sera un motif suffisant pour mettre fin à mon emploi.",
    'declaration_2' => 'Les informations fournies sur ce formulaire en tant que candidat seront stockées sur des dossiers papier ou un système informatique conformément à toute réglementation locale sur la protection des données des employés et seront traitées uniquement dans le cadre du recrutement.',
    'submit' => 'Soumettre la demande',
    'thankYou_1' => 'Merci pour votre candidature.',
    'thankYou_2' => 'Nous confirmons la réception de votre demande, qui est en cours de traitement. Vous recevrez une notification par e-mail sous peu avec plus de détails. Merci pour ton intérêt.',
    'regards' => 'Cordialement.',
    'goHome' => 'Rentrer chez soi',
    'personalInfo' => 'Information personnelle',
    'education' => 'Éducation',
    'workExperience' => "L'expérience professionnelle",
    'skills' => 'Compétences',
    'references' => 'Références',
    'languages' => 'Langues',
    'resume' => 'Curriculum Vitae',

    /* Personal Information */

    'firstName' => '*Prénom',
    'middleName' => 'Deuxième nom',
    'lastName' => '*Nom de famille',
    'emailAddress' => '*Adresse e-mail',
    'homePhone' => '*Téléphone fixe',
    'mobileNumber' => '*Numéro de portable',
    /* Gender select */
    'gender' => 'Le sexe',
    'female' => 'Femelle',
    'male' => 'Mâle',
    'other' => 'Autre',
    'department' => '*Département',
    'accountingFinance' => 'Finance comptable',
    'marketing' => 'Le marketing',
    'operations' => 'Opérations',
    'hrAdministration' => 'Ressources Humaines et Administration',
    'corporate' => 'Entreprise',
    'supply' => 'Fourniture',
    'aviation' => 'Aviation',
    /*************************/

    'otherDepartment' => '*Autre département',

    /* Type select */
    'type' => '*Type',
    'fullTime' => 'À plein temps',
    'partTime' => 'À temps partiel',
    'contractors' => 'Entrepreneurs',
    /*************************/

    'yearsWorkExperience' => "Années d'expérience de travail",
    'dob' => 'Date de naissance',
    'noWorkExperience' => 'Aucune expérience de travail',
    'moreThan' => 'Plus de 25',
    'currentSalary' => '*Salaire actuel',
    'period' => '*Période',

    /* Period select */
    'annually' => 'Annuellement',
    'monthly' => 'Mensuel',
    'biWeekly' => 'Bihebdomadaire',
    'weekly' => 'Hebdomadaire',
    'twiceMonth' => 'Deux fois par mois',
    /*****************/
    'currency' => '*Devise',
    'expectedSalary' => '*Salaire attendu',
    'address' => 'Adresse',
    'validDriverLicense' => 'Avez-vous un permis de conduire valide?',

    /* Study Information */
    'university' => 'Université / Institut',
    'studyArea' => "Zone d'étude",
    'certificateDegree' => 'Diplôme obtenu',

    /* Certificate-Degree Select */
    'postGraduate' => "diplôme d'études supérieures",
    'graduate' => 'Diplômé',
    'highSchool' => 'Lycée / École secondaire',
    /*****************************/

    'otherCertificate' => 'Autre certificat / diplôme reçu',
    'dateFrom' => 'À partir',
    'dateTo' => 'À',

    /*Work Experience*/
    'startCurrentEmployment' => '*Commencez par votre expérience professionnelle actuelle et continuez par ordre chronologique.',
    'employerName' => "Nom de l'employeur",
    'position' => 'Position',
    'responsibilities' => 'Responsabilités',
    'supervisorName' => 'Nom du superviseur / gestionnaire',
    'positionStartDate' => 'Date de début du poste',
    'positionEndDate' => 'Date de fin du poste',
    'reasonLeaving' => 'Raison du départ ou souhait de partir',
    'phoneNumber' => 'Numéro de téléphone',

    /* Skills */
    'listSkills' => 'Lister toutes les compétences',
    'professionalQualifications' => 'Qualifications professionnelles actuellement détenues',
    'otherRelevantEducational' => "Autres cours d'éducation / de formation pertinents",
    'interestActivitiesHobbies' => 'Intérêt / Activités / Loisirs',

    /* References */
    'jobTitle' => "Titre d'emploi",
    'nameReferee' => "Nom de l'arbitre",
    'organization' => 'Organisation',
    'relationReferee' => "Relation avec l'arbitre",

    /* Languages */
    'languages' => 'Langages',
    'language' => 'Langue',
    'understandingProficiency' => 'Comprendre la compétence',
    'intermediate' => 'Intermédiaire',
    'upperIntermediate' => 'Intermédiaire supérieur',
    'advanced' => 'Avancé',
    'proficient' => 'Compétent',
    'writingProficiency' => 'Compétence en rédaction',
    'readingProficiency' => 'Maîtrise de la lecture',
    'whereDidYouLearnIt' => "Où est-ce que vous l'avez appris?",

    /* Resume */
    'resume' => 'Curriculum vitae',
    'howLearnAboutVacancy' => 'Comment apprenez-vous ce poste?',
    'careerCenter' => 'Centre de Carrière',
    'employeeCompany' => "Employé de l'entreprise",
    'jobBoard' => 'Tableau des emplois',
    'newspaper' => 'Journal',
    'word-of-Mouth' => 'Bouche à oreille',
    '' => '',
];

