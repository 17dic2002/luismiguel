<?php
/**
 * Created by Luis Miguel.
 * User: Luis Miguel
 * Date: 1/9/20
 * Time: 4:52 p. m.
 */
return [

    /* Common */
    'choose' => 'Elegir',
    'next' => 'Siguiente',
    'yes' => 'Sí',
    'saved' => ' guardado correctamente.',
    'save'=>'Save',
    'remove'=>'Remover',
    'addMore' => 'Añadir Más',
    'declaration_1' => 'Declaro que la información proporcionada en este formulario de solicitud es verdadera y completa. Entiendo que si proporcioné información engañosa en este formulario o hice alguna omisión, esto será motivo suficiente para terminar mi empleo.',
    'declaration_2' => 'La información proporcionada en este formulario como solicitante se almacenará en registros en papel o en un sistema informático de acuerdo con las reglamentaciones locales sobre la protección de los datos de los empleados y se procesará únicamente en relación con la contratación.',
    'submit' => 'Enviar Aplicación',
    'thankYou_1' => 'Gracias por tu aplicación.',
    'thankYou_2' => 'Confirmamos la recepción de su solicitud, que se está procesando. En breve recibirá una notificación por correo electrónico con más detalles. ',
    'regards' => 'Cordialmente',
    'goHome' => 'Ir al Inicio',


    /* Personal Information */
    'personalInfo' => 'Información Personal',
    'education' => 'Educación',
    'workExperience' => 'Experiencia Laboral',
    'skills' => 'Habilidades',
    'references' => 'Referencias',
    'languages' => 'Idiomas',
    'resume' => 'Resumen',
    'firstName' => '*Primer Nombre',
    'middleName' => 'Segundo Nombre',
    'lastName' => '*Apellido',
    'emailAddress' => '*Email',
    'homePhone' => '*Teléfono fijo',
    'mobileNumber' => '*Teléfono móvil',
    /* Gender select */
    'gender' => 'Género',
    'female' => 'Femenino',
    'male' => 'Masculino',
    'other' => 'Otro',
    'department' => '*Departmento',
    'accountingFinance' => 'Cuentas y finanzas',
    'marketing' => 'Marketing',
    'operations' => 'Operaciones',
    'hrAdministration' => 'RRHH & Administración',
    'corporate' => 'Corporativo',
    'supply' => 'Suministro',
    'aviation' => 'Aviación',
    /*************************/

    'otherDepartment' => '*Otro Departamento',

    /* Type select */
    'type' => '*Tipo',
    'fullTime' => 'Tiempo Completo',
    'partTime' => 'Tiempo Parcial',
    'contractors' => ' Contratistas',
    /*************************/

    'yearsWorkExperience' => 'Año(s) de experiencia laboral',
    'noWorkExperience' => 'No Work Experience',
    'moreThan' => 'Más de 25',
    'dob' => 'Fecha de nacimiento',
    'currentSalary' => '*Salario actual',
    'period' => '*Periodo',

    /* Period select */
    'annually' => 'Anualmente',
    'monthly' => 'Mensual',
    'biWeekly' => 'Bisemanal',
    'weekly' => 'Semanal',
    'twiceMonth' => 'Dos veces al mes',
    /*****************/
    'currency' => '*Moneda',
    'expectedSalary' => '*Sueldo esperado',
    'address' => 'Dirección',
    'validDriverLicense' => '¿Tiene una licencia de conducir válida?',

    /* Study Information */
    'university' => 'Universidad / Instituto',
    'studyArea' => 'Área de Estudio',
    'certificateDegree' => 'Certificado/Grado recibido',

    /* Certificate-Degree Select */
    'postGraduate' => 'Posgrado',
    'graduate' => 'Universitario',
    'highSchool' => 'Secundaria',
    /*****************************/

    'otherCertificate' => 'Otro Certificado/Grado recibido',
    'dateFrom' => 'Desde',
    'dateTo' => 'Hasta',

    /*Work Experience*/
    'startCurrentEmployment' => '*Comience con la experiencia laboral actual y continúe en orden cronológico.',
    'employerName' => 'Nombre del empleador',
    'position' => 'Posición',
    'responsibilities' => 'Responsabilidades',
    'supervisorName' => 'Nombre del Supervisor/Gerente',
    'positionStartDate' => 'Fecha de inicio del puesto',
    'positionEndDate' => 'Fecha de finalización del puesto',
    'reasonLeaving' => 'Motivo por el que se fue o desear irse',
    'phoneNumber' => 'Número de teléfono',

    /* Skills */
    'skills' => 'Habilidades',
    'listSkills' => 'Listar todas las habilidades',
    'professionalQualifications' => 'Calificaciones profesionales actualmente en vigencia',
    'otherRelevantEducational' => 'Otros cursos educativos o de capacitación relevantes',
    'interestActivitiesHobbies' => 'Intereses / Actividades / Pasatiempos',

    /* References */
    'references' => 'Referencias',
    'jobTitle' => 'Título profesional',
    'nameReferee' => 'Nombre',
    'organization' => 'Organización',
    'relationReferee' => 'Relación con la persona',

    /* Languages */
    'language' => 'Idioma',
    'understandingProficiency' => 'Comprensión de la competencia',
    'intermediate' => 'Intermedio',
    'upperIntermediate' => 'Intermedio superior',
    'advanced' => 'Avanzado',
    'proficient' => 'Competente',
    'writingProficiency' => 'Competencia en escritura',
    'readingProficiency' => 'Competencia en lectura',
    'whereDidYouLearnIt' => '¿Donde lo aprendiste?',

    /* Resume */
    'howLearnAboutVacancy' => '¿Cómo se enteró de esta vacante?',
    'careerCenter' => 'Centro de carreras',
    'employeeCompany' => 'Empleado de la empresa',
    'jobBoard' => 'Job Board',
    'newspaper' => 'Periódico',
    'word-of-Mouth' => 'Boca a boca',
    '' => '',

];
