<?php
/**
 * Created by Luis Miguel.
 * User: Luis Miguel
 * Date: 1/9/20
 * Time: 4:52 p. m.
 */
return [

    /* Common */
    'choose' => 'Choose...',
    'next' => 'Next',
    'yes' => 'Yes',
    'saved' => ' saved successfully.',
    'updated' => ' updated successfully.',
    'save'=>'Save',
    'remove'=>'Remove',
    'addMore' => 'Add More',
    'declaration_1' => 'I declare that the information given in this application form is true and complete. I understand that if I have given any misleading information on this form or made any omissions, this will be sufficient grounds for terminating my employment.',
    'declaration_2' => 'The information provided on this form as an applicant will be stored either on paper records or a computer system in accordance with any local regulations on the protection of Employee Data and will be processed solely in connection with recruitment.',
    'submit' => 'Submit Application',
    'thankYou_1' => 'Thank you for taking the time to apply for our position.',
    'thankYou_2' => 'Your application has been received and is being processed. You will soon receive an email notification with further details.',
    'goHome' => 'Go Home',
    'personalInfo' => 'Personal Information',
    'education' => 'Education',
    'workExperience' => 'Work Experience',
    'skills' => 'Skills',
    'references' => 'References',
    'languages' => 'Languages',
    'resume' => 'Resume',
    'experiences' => 'Experiences',

    /* Personal Information */

    'firstName' => '*First Name',
    'middleName' => 'Middle Name',
    'lastName' => '*Last Name',
    'emailAddress' => '*Email Address',
    'homePhone' => '*Home Phone',
    'mobileNumber' => '*Mobile Number',
    /* Gender select */
    'gender' => 'Gender',
    'female' => 'Female',
    'male' => 'Male',
    'other' => 'Other',
    'department' => '*Department',
    'accountingFinance' => 'Accounting Finance',
    'marketing' => 'Marketing',
    'operations' => 'Operations',
    'hrAdministration' => 'HR & Administration',
    'corporate' => 'Corporate',
    'supply' => 'Supply',
    'aviation' => 'Aviation',
    /*************************/

    'otherDepartment' => '*Other Department',

    /* Type select */
    'type' => '*Type',
    'fullTime' => 'Full Time',
    'partTime' => 'Part Time',
    'contractors' => ' Contractors',
    /*************************/

    'yearsWorkExperience' => 'Year(s) of Work Experience',
    'dob' => 'Date of Birth',
    'noWorkExperience' => 'No Work Experience',
    'moreThan' => 'More Than 25',
    'currentSalary' => '*Current Salary',
    'period' => '*Period',

    /* Period select */
    'annually' => 'Annually',
    'monthly' => 'Monthly',
    'biWeekly' => 'Bi-Weekly',
    'weekly' => 'Weekly',
    'twiceMonth' => 'Twice a Month',
    /*****************/
    'currency' => '*Currency',
    'expectedSalary' => '*Expected Salary',
    'address' => 'Address',
    'validDriverLicense' => 'Do you have a valid driver license?',

    /* Study Information */
    'university' => 'University/Institute',
    'studyArea' => 'Study Area',
    'certificateDegree' => 'Certificate-Degree received',

    /* Certificate-Degree Select */
    'postGraduate' => 'Post Graduate',
    'graduate' => 'Graduate',
    'highSchool' => 'High School/Secondary School',
    /*****************************/

    'otherCertificate' => 'Other Certificate/ Degree Received',
    'dateFrom' => 'Date From',
    'dateTo' => 'Date To',

    /*Work Experience*/
    'startCurrentEmployment' => '*Start with current employment experience and continue in chronological order.',
    'employerName' => 'Employer name',
    'position' => 'Position',
    'responsibilities' => 'Responsibilities',
    'supervisorName' => 'Supervisor/Manager\'s Name',
    'positionStartDate' => 'Position Start Date',
    'positionEndDate' => 'Position End Date',
    'reasonLeaving' => 'Reason for leaving or whising to leave',
    'phoneNumber' => 'Phone Number',

    /* Skills */
    'listSkills' => 'List all Skills',
    'professionalQualifications' => 'Professional Qualifications Currently Held',
    'otherRelevantEducational' => 'Other Relevant Educational / Training Courses',
    'interestActivitiesHobbies' => 'Interest / Activities/ Hobbies',

    /* References */
    'jobTitle' => 'Job Title',
    'nameReferee' => 'Name of referee',
    'organization' => 'Organization',
    'relationReferee' => 'Relation to Referee',

    /* Languages */
    'languages' => 'Languages',
    'language' => 'Language',
    'understandingProficiency' => 'Understanding Proficiency',
    'intermediate' => 'Intermediate',
    'upperIntermediate' => 'Upper Intermediate',
    'advanced' => 'Advanced',
    'proficient' => 'Proficient',
    'writingProficiency' => 'Writing Proficiency',
    'readingProficiency' => 'Reading Proficiency',
    'whereDidYouLearnIt' => 'Where did you learn it?',

    /* Resume */
    'resume' => 'Resume',
    'howLearnAboutVacancy' => 'How do you learn about this vacancy?',
    'careerCenter' => 'Career Center',
    'employeeCompany' => 'Employee of the Company',
    'jobBoard' => 'Job Board',
    'newspaper' => 'Newspaper',
    'word-of-Mouth' => 'Word-of-Mouth',
    '' => '',
];

