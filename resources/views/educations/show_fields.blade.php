<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $educations->id !!}</p>
</div>

<!-- Institute Field -->
<div class="form-group">
    {!! Form::label('institute', 'Institute:') !!}
    <p>{!! $educations->institute !!}</p>
</div>

<!-- Study Area Field -->
<div class="form-group">
    {!! Form::label('study_area', 'Study Area:') !!}
    <p>{!! $educations->study_area !!}</p>
</div>

<!-- Certificate Field -->
<div class="form-group">
    {!! Form::label('certificate', 'Certificate:') !!}
    <p>{!! $educations->certificate !!}</p>
</div>

<!-- Other Field -->
<div class="form-group">
    {!! Form::label('other', 'Other:') !!}
    <p>{!! $educations->other !!}</p>
</div>

<!-- Date From Field -->
<div class="form-group">
    {!! Form::label('date_from', 'Date From:') !!}
    <p>{!! $educations->date_from !!}</p>
</div>

<!-- Date To Field -->
<div class="form-group">
    {!! Form::label('date_to', 'Date To:') !!}
    <p>{!! $educations->date_to !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $educations->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $educations->updated_at !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $educations->application_id !!}</p>
</div>

