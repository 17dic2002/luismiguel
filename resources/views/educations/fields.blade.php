<!-- Institute Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute', 'Institute:') !!}
    {!! Form::text('institute', null, ['class' => 'form-control']) !!}
</div>

<!-- Study Area Field -->
<div class="form-group col-sm-6">
    {!! Form::label('study_area', 'Study Area:') !!}
    {!! Form::text('study_area', null, ['class' => 'form-control']) !!}
</div>

<!-- Certificate Field -->
<div class="form-group col-sm-6">
    {!! Form::label('certificate', 'Certificate:') !!}
    {!! Form::select('certificate', ['Choose...' => 'Choose...', 'Post Graduate' => 'Post Graduate', 'Graduate' => 'Graduate', 'High School/Secondary School' => 'High School/Secondary School', 'Other' => 'Other'], null, ['class' => 'form-control']) !!}
</div>

<!-- Other Field -->
<div class="form-group col-sm-6">
    {!! Form::label('other', 'Other:') !!}
    {!! Form::text('other', null, ['class' => 'form-control']) !!}
</div>

<!-- Date From Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_from', 'Date From:') !!}
    {!! Form::date('date_from', null, ['class' => 'form-control','id'=>'date_from']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_from').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Date To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_to', 'Date To:') !!}
    {!! Form::date('date_to', null, ['class' => 'form-control','id'=>'date_to']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_to').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::number('application_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('educations.index') !!}" class="btn btn-default">Cancel</a>
</div>
