@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Jobs</h1>
        </br></br>
        <p class="pull-left">Use this feature to create and post job vacancies you wish to fulfill. </p>
        </br></br>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('jobs.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

