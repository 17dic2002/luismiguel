<div class="row">
    <div class="col-md-2">
        {!! Form::label('id', 'Id:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->id !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('title', 'Title:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->title !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('location', 'Location:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->location !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('status', 'Status:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->status !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('description', 'Description:') !!}
    </div>
    <div class="col-md-6">
        <embed src="{{ asset('uploads/jobs/'.$jobs->description) }}" style="width:600px; height:800px;" frameborder="0">
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        {!! Form::label('start_publish', 'Start Publish:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->start_publish !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('stop_publish', 'Stop Publish:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->stop_publish !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('created_at', 'Created At:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->created_at !!}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        {!! Form::label('updated_at', 'Updated At:') !!}
    </div>
    <div class="col-md-6">
        <p>{!! $jobs->updated_at !!}</p>
    </div>
</div>

