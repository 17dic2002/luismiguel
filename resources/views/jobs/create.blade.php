@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Jobs
        </h1>
        </br>
        <p>Use this feature to create and post job vacancies you wish to fulfill. </p>
        </br></br>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'jobs.store', 'files' => true]) !!}

                        @include('jobs.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
