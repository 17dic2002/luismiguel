<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Job Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::select('location', ['Select a Country...' => 'Select a Country...', 'To be Determined' => 'To be Determined', 'Corporate Office' => 'Corporate Office', 'Anguila' => 'Anguila', 'Antigua &amp; Barbuda' => 'Antigua &amp; Barbuda', 'Bahamas' => 'Bahamas', 'Barbados' => 'Barbados', 'Belize' => 'Belize', 'Bermuda' => 'Bermuda', 'BVI' => 'BVI', 'Cayman Island' => 'Cayman Island', 'Dominica' => 'Dominica', 'Dominican Republic' => 'Dominican Republic', 'French Guiana' => 'French Guiana', 'Grenada' => 'Grenada', 'Guadelope' => 'Guadelope', 'Guyana' => 'Guyana', 'Jamaica' => 'Jamaica', 'Martinique' => 'Martinique', 'Puerto Rico' => 'Puerto Rico', 'St. Kitts &amp; Nevis' => 'St. Kitts &amp; Nevis', 'St. Lucia' => 'St. Lucia', 'St. Maarten' => 'St. Maarten', 'St. Vincent &amp; The Grenadines' => 'St. Vincent &amp; The Grenadines', 'Suriname' => 'Suriname'], null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Job Description:') !!}
    {!! Form::file('description') !!}
</div>
<div class="clearfix"></div>

<!-- Start Publish Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_publish', 'Start Publish:') !!}
    {!! Form::date('start_publish', null, ['class' => 'form-control','id'=>'start_publish']) !!}
</div>

<!-- Stop Publish Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stop_publish', 'Stop Publish:') !!}
    {!! Form::date('stop_publish', null, ['class' => 'form-control','id'=>'stop_publish']) !!}
</div>

<!-- Internal Job? Field -->
<div class="form-group col-sm-6">
    <h4 class="pull-left">Note: The job will be published by default in All Jobs section, if you don't want to, please uncheck this option. </h4>

    {!! Form::label('all_jobs', 'Publish this job in "All Jobs" Section?:') !!}
    {!! Form::checkbox('all_jobs', 1 , true) !!}
    <br>
    {!! Form::label('internal', 'Do you wish to upload the JD to Internal jobs also? :') !!}
    {!! Form::checkbox('internal', 1 , false) !!}

</div>

<!-- All Jobs? Field -->
<div class="form-group col-sm-6">
    
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('jobs.index') !!}" class="btn btn-default">Cancel</a>
</div>
