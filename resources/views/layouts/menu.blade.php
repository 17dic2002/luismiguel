
<li class="{{ Request::is('users*') ? 'active' : '' }}">

    <a href="{!! route('users.index') !!}"><i class="fa fa-users"></i><span>Users</span></a>
</li>
<li class="{{ Request::is('jobs*') ? 'active' : '' }}">
    <a href="{!! route('jobs.index') !!}"><i class="fa fa-edit"></i><span>Post a Job</span></a>
</li>

<li class="{{ Request::is('requestAds*') ? 'active' : '' }}">
    <a href="{!! route('requestAds.index') !!}"><i class="fa fa-edit"></i><span>Request Ads</span></a>
</li>

<li class="{{ Request::is('applications*') ? 'active' : '' }}">
    <a href="{!! route('applications.index') !!}"><i class="fa fa-edit"></i><span>Jobs Applications</span></a>
</li>

<li class="{{ Request::is('options*') ? 'active' : '' }}">
    <a href="{!! route('options.index') !!}"><i class="fa fa-edit"></i><span>Options</span></a>
</li>



<!--
<li class="{{ Request::is('applications*') ? 'active' : '' }}">
    <a href="{!! route('applications.indexSaved') !!}"><i class="fa fa-edit"></i><span>Applications Saved</span></a>
</li>

<li class="{{ Request::is('applications*') ? 'active' : '' }}">
    <a href="{!! route('applications.indexShortList') !!}"><i class="fa fa-edit"></i><span>Applications Short List</span></a>
</li>

<li class="{{ Request::is('applications') ? 'active' : '' }}">
    <a href="{!! route('applications.indexRejected') !!}"><i class="fa fa-edit"></i><span>Applications Rejected</span></a>
</li>

-->
