<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Email - Registration Confirmation</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <style>

        .centered {
            width: 600px;
            display: block;
            margin: 0 auto;
        }

        .ptext {
            padding: 5% 5% 5% 7%;
        }

    </style>

</head>

<body>
<div class="container">

    <div class="centered">

        <img class=" mx-auto d-block"
             src="https://paradigmassociates.s3.us-east-2.amazonaws.com/SOL/HR/RegistrationConfirmation-Header.jpg"
             alt=""/>

        <div class="ptext">

            <p class="text-justify">Thank you {{$data['name']}} for your interest in joining our team. We encourage you
                to check back often to see all available career opportunities at Sol, as we consistently seek talented
                individuals to strengthen our role as a world-class operation.</p>
            </br>

            <a href="{{env('APP_PROD_URL')}}/careers"><h4>CLICK HERE TO BEGIN YOUR SEARCH NOW</h4></a>
            </br>

            <p>Regards</p>
            <p>The Sol Team</p>

        </div>

    </div>
</div>
</body>
</html>
