<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Email - Application Confirmation</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <style>

        .centered {
            width: 600px;
            display: block;
            margin: 0 auto;
        }

        .ptext {
            padding: 5% 5% 5% 7%;
        }

        hr {
            border: 5px solid #0077be;
        }
    </style>

</head>

<body>
<div class="container">

    <div class="centered">

        <img class=" mx-auto d-block"
             src="https://paradigmassociates.s3.us-east-2.amazonaws.com/SOL/HR/Digital-Media-%E2%80%93-Paradigm-Header.jpg"
             alt=""/>

        <div class="ptext">

            <p class="text-justify">Hello {{$data['requester_name']}},</br>Your request to place an online/digital
                advertisement is confirmed. Below is a
                review of your request:</p>
            </br>
            <p><b>Job:</b> {{$data['job_title']}}</p>
            @if(isset($data['platforms']) or $data['platforms'] != null)
                <p><b>Platforms:</b> {{$data['platforms']}} and {{$data['other_platform']}} </p>
            @endif
            @if(isset($data['template_social']) or $data['template_social'] != null)
                <p><b>Template:</b> {{$data['template_social']}}</p>
            @endif
            @if(isset($data['budget_social']) or $data['budget_social'] !=null)
                <p><b>Bugdet:</b> {{$data['budget_social']}}</p>
                @endif

                </br>

                <p>Your request will be fulfilled by Paradigm Associates. You will receive a follow-
                    up confirmation message once the ad run is complete.</p>
                <p>If the above information is incorrect, please click <a href="{{env('APP_PROD_URL')}}/requestAds/">here</a> search and
                    edit/review the
                    request, or contact Paradigm Associates <a href="https://www.paradigmpr.com/"
                                                               target="_blank">here</a>.
                </p>

                </br>

                <p>Regards</p>
                <p>The Sol Team</p>

        </div>
        </br>
        </br>
        <hr>
    </div>


</div>
</body>
</html>
