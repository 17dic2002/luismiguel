<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Email - Application COnfirmation</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<style>

		.centered {
			width: 600px;
			display:block;
			margin: 0 auto;
		}
		.ptext {
			padding: 5% 5% 5% 7%;
		}
		hr {
			border: 5px solid #0077be;
		}
	</style>

</head>
<body>
	<div class="container">
		<div class="centered">
            <img class=" mx-auto d-block" src="https://paradigmassociates.s3.us-east-2.amazonaws.com/SOL/HR/Confirmation-%E2%80%93-New-job-posting-Header.jpg" alt="" />
			<div class="ptext">
				<p class="text-justify">Your job posting to the Sol Petroleum Career site was successful. Below is a review of your post:</p>
				</br>
				<p><b>Title: </b>{{$data['title']}}</p>
				<p><b>Location: </b>{{$data['location']}}</p>
				<p><b>Start Publish: </b>{{$data['start_publish']}}</p>
				<p><b>Stop Publish: </b>{{$data['stop_publish']}}</p>
				</br>
				<p>If the above information is incorrect, please click here to edit/review the request, or contact Sol
                    <a href="{{env('APP_PROD_URL')}}/jobs/{{$data['id']}}/edit">here</a>.</p>
				</br>
				<p>Regards</p>
				<p>The Sol Team</p>
			</div>
			</br>
			</br>
			<hr>
		</div>
	</div>
</body>
</html>
