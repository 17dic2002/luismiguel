<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Email - Media Request Confirmation</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

    <style>

        .centered {
            width: 600px;
            display: block;
            margin: 0 auto;
        }

        .ptext {
            padding: 5% 5% 5% 7%;
        }

        hr {
            border: 5px solid #0077be;
        }
    </style>

</head>

<body>
<div class="container">

    <div class="centered">

        <img class=" mx-auto d-block"
             src="https://paradigmassociates.s3.us-east-2.amazonaws.com/SOL/HR/Offline-Media-%E2%80%93-BPC-Header.jpg"
             alt=""/>

        <div class="ptext">

            <p class="text-justify">Hello {{$data['requester_name']}},</br> Your request to place an offline/traditional
                advertisement is confirmed. Below is
                a review of your request:</p>
            </br>
            <p><b>Job Title: </b>{{$data['job_title']}}</p>
            <hr>
            <b>Media Placemente #1</b>
            <p><b>Name of Publication: </b>{{$data['name_publication']}}</p>
            <p><b>Ad Size: </b>{{$data['ad_size']}}</p>
            <p><b>Ad Start Date: </b>{{$data['date_publication']}}</p>
            <p><b>Ad End Date: </b>{{$data['date_finish']}}</p>
            <p><b>Template: </b>{{$data['template_print']}}</p>
            <p><b>Orientation: </b>{{$data['orientation']}}</p>
            <p><b>Color Type: </b>{{$data['color_type']}}</p>
            <p><b>Allocate Budget: </b>{{$data['budget']}}</p>
            <p><b>Submission Deadline: </b>{{$data['submission_deadline']}}</p>
            </br>

            @if($data['name_publication_2'] != null)
                <hr>
                <b>Media Placemente #2</b>
                <p><b>Name of Publication: </b>{{$data['name_publication_2']}}</p>
                <p><b>Ad Size: </b>{{$data['ad_size_2']}}</p>
                <p><b>Ad Start Date: </b>{{$data['date_publication_2']}}</p>
                <p><b>Ad End Date: </b>{{$data['date_finish_2']}}</p>
                <p><b>Template: </b>{{$data['template_print_2']}}</p>
                <p><b>Orientation: </b>{{$data['orientation_2']}}</p>
                <p><b>Color Type: </b>{{$data['color_type_2']}}</p>
                <p><b>Allocate Budget: </b>{{$data['budget']}}</p>
                <p><b>Submission Deadline: </b>{{$data['submission_deadline2']}}</p>

                </br>
            @endif
            @if($data['name_publication_3'] != null)
                <hr>
                <b>Media Placemente #3</b>
                <p><b>Name of Publication: </b>{{$data['name_publication_3']}}</p>
                <p><b>Ad Size: </b>{{$data['ad_size_3']}}</p>
                <p><b>Ad Start Date: </b>{{$data['date_publication_3']}}</p>
                <p><b>Ad End Date: </b>{{$data['date_finish_3']}}</p>
                <p><b>Template: </b>{{$data['template_print_3']}}</p>
                <p><b>Orientation: </b>{{$data['orientation_3']}}</p>
                <p><b>Color Type: </b>{{$data['color_type_3']}}</p>
                <p><b>Allocate Budget: </b>{{$data['budget']}}</p>
                <p><b>Submission Deadline: </b>{{$data['submission_deadline3']}}</p>
                </br>
            @endif

            <p>Your request will be fulfilled by Blueprint Creative. You will receive a follow-up
                confirmation message once the ad run is complete.</p>
            <p>If the above information is incorrect, please click <a href="{{ENV('APP_PROD_URL')}}/requestAds/">here</a> search and edit/review the
                request, or contact Blueprint Creative <a href="#">here</a>.</p>

            </br>

            <p>Regards</p>
            <p>The Sol Team</p>

        </div>
        </br>
        </br>
        <hr>
    </div>


</div>
</body>
</html>
