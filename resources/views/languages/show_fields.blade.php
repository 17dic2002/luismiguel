<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $languages->id !!}</p>
</div>

<!-- Language Field -->
<div class="form-group">
    {!! Form::label('language', 'Language:') !!}
    <p>{!! $languages->language !!}</p>
</div>

<!-- Understanding Field -->
<div class="form-group">
    {!! Form::label('understanding', 'Understanding:') !!}
    <p>{!! $languages->understanding !!}</p>
</div>

<!-- Writing Field -->
<div class="form-group">
    {!! Form::label('writing', 'Writing:') !!}
    <p>{!! $languages->writing !!}</p>
</div>

<!-- Reading Field -->
<div class="form-group">
    {!! Form::label('reading', 'Reading:') !!}
    <p>{!! $languages->reading !!}</p>
</div>

<!-- Where Learn Field -->
<div class="form-group">
    {!! Form::label('where_learn', 'Where Learn:') !!}
    <p>{!! $languages->where_learn !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $languages->application_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $languages->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $languages->updated_at !!}</p>
</div>

