<!-- Language Field -->
<div class="form-group col-sm-6">
    {!! Form::label('language', 'Language:') !!}
    {!! Form::text('language', null, ['class' => 'form-control']) !!}
</div>

<!-- Understanding Field -->
<div class="form-group col-sm-6">
    {!! Form::label('understanding', 'Understanding:') !!}
    {!! Form::select('understanding', ['Choose...' => 'Choose...', 'Intermediate' => 'Intermediate', 'Upper Intermediate' => 'Upper Intermediate', 'Advanced' => 'Advanced', 'Proficient' => 'Proficient'], null, ['class' => 'form-control']) !!}
</div>

<!-- Writing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('writing', 'Writing:') !!}
    {!! Form::select('writing', ['Choose...' => 'Choose...', 'Intermediate' => 'Intermediate', 'Upper Intermediate' => 'Upper Intermediate', 'Advanced' => 'Advanced', 'Proficient' => 'Proficient'], null, ['class' => 'form-control']) !!}
</div>

<!-- Reading Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reading', 'Reading:') !!}
    {!! Form::select('reading', ['Choose...' => 'Choose...', 'Intermediate' => 'Intermediate', 'Upper Intermediate' => 'Upper Intermediate', 'Advanced' => 'Advanced', 'Proficient' => 'Proficient'], null, ['class' => 'form-control']) !!}
</div>

<!-- Where Learn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('where_learn', 'Where Learn:') !!}
    {!! Form::text('where_learn', null, ['class' => 'form-control']) !!}
</div>

<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::number('application_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('languages.index') !!}" class="btn btn-default">Cancel</a>
</div>
