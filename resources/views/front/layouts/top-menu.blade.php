<nav class="navbar navbar-expand-lg">
    <img src="https://solpetroleum.wpengine.com/wp-content/uploads/2017/03/sol-logow.png">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="navbar-toggler-icon icon-bar"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="https://solpetroleum.com/">Home <span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="https://solpetroleum.com/about-us/" id="navbarDropdown"
                    role="button" data-toggle="dropdown" aria-expanded="false">
                    About Us
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="https://solpetroleum.com/about-us/mission-values-promise/">Our
                        Mission</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/about-us/">Brand Stroy</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/about-us/our-network/">Our Network</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/about-us/in-the-community/">In The
                        community</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/about-us/hse-commitment/">HSE
                        Commitment</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="https://solpetroleum.com/about-us/" id="navbarDropdown"
                    role="button" data-toggle="dropdown" aria-expanded="false">
                    Products & Services
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/retail-fuels/">Retail
                        Fuels</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/lpg/">LPG</a>
                    <a class="dropdown-item" href="<a class=" dropdown-item"
                        href="https://solpetroleum.com/products-services-2/lubricants/">Lubricants</a>
                    <a class="dropdown-item"
                        href="https://solpetroleum.com/products-services-2/convenience-retailing/">Convinience
                        Retailing</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/aviation/">Aviation</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/marine/">Marine</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/bitumen/">Bitumen</a>
                    <a class="dropdown-item" href="https://fleetbb.solpetroleum.com/welcome">Sol Card</a>
                    <a class="dropdown-item" href="https://www.moremilesrewards.com/">More Miles Journie Rewards</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://solpetroleum.com/news-2/">Media</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://careers.solpetroleum.com/">Careers</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="https://solpetroleum.com/about-us/" id="navbarDropdown"
                    role="button" data-toggle="dropdown" aria-expanded="false">
                    Ordering
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="https://grenada.solpetroleum.com/">Grenada</a>
                    <a class="dropdown-item" href="https://barbados.solpetroleum.com/">Barbados</a>
                    <a class="dropdown-item" href="https://suriname.solpetroleum.com/">Suriname</a>
                    <a class="dropdown-item"
                        href="https://solpetroleum.com/products-services-2/convenience-retailing/">Convinience
                        Retailing</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/aviation/">Aviation</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/marine/">Marine</a>
                    <a class="dropdown-item" href="https://solpetroleum.com/products-services-2/bitumen/">Bitumen</a>
                    <a class="dropdown-item" href="https://fleetbb.solpetroleum.com/welcome">Sol Card</a>
                    <a class="dropdown-item" href="https://www.moremilesrewards.com/">More Miles Journie Rewards</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://solpetroleum.com/contact-2/">Contact</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="https://solpetroleum.com/about-us/" id="navbarDropdown"
                    role="button" data-toggle="dropdown" aria-expanded="false">
                    English
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Nederlands</a>
                    <a class="dropdown-item" href="#">Francais</a>
                    <a class="dropdown-item" href="#">Espa単ol</a>
                </div>
            </li>
            @if (Auth::check())
                @can('dashboard')
                    <li class="nav-item">
                        <a class="nav-link item" href="{{ route('dashboard') }}">
                            DASHBOARD
                        </a>
                    </li>
                @endcan
                <li class="nav-item">
                    <div class="pull-right">
                        <a href="{!! url('/logout') !!}" class="btn btn-default btn-flat"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Sign out
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link item" href="{{ route('register') }}">
                        Register
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link item" href="{{ route('login') }}">
                        Log In
                    </a>
                </li>
            @endif
        </ul>
    </div>
</nav>
