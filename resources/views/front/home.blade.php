@extends('front.layouts.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
@endsection

@section('content-front')

<div class="container-block">
    <div class="row row-home">
        <div class="container col-md-12 col-sm-6">

            <main role="main">

                <div class="jumbotron inner-banner has-dot-pattern">
                    <div class="container">
                        <h1 class="">Join Our Team</h1>
                        <p class="lead">Are you driven to succeed?</p>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            @include('flash::message')

                            <h4>Careers</h4>
                            <p class="text-justify">The Sol Group, a member of the Parkland family, is the leading
                                supplier of
                                petroleum based products and energy solutions across 22 territories in the
                                Caribbean, Central
                                and South America. Sol is committed to the region and provides rewarding careers for
                                a diverse
                                range of professions. People are our passion and our Sol teams are appropriately
                                recruited,
                                compensated, trained, developed and motivated in order to achieve Sol&apos;s
                                business
                                objectives.</p>
                            <p class="text-justify">We provide career growth opportunities in a fair and
                                collaborative
                                environment built on our Values of Safety, Integrity, Community, and Respect. We are
                                building a
                                team of industry leaders, growing in our size and strengths, and leading in
                                technology
                                investment.</p>
                            <p class="text-justify">We provide challenging and meaningful careers for the right
                                people. We are
                                an equal opportunity employer and aim to create a world-class operation through the
                                development
                                and retention of talented people across the region.</p>
                            <p class="text-justify">Iterested in learning more about who we are?</p>
                            <img style="width: 50%;" src="{{asset('images/powering-journeys-logo.png')}}" class="rounded float-left" alt="...">
                            <img src="https://solpetroleum.com/wp-content/uploads/2019/09/Parkland_Final_Logo_Stacked_CMYK-150x150.jpg" class="rounded float-left" alt="...">
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4">

                            <h4>Current Opportunities</h4>
                            <hr>

                            <a href="{{route('careers')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-disabled="true">                             Job Opportunities</a>

                            <a href="{{route('careers-internal')}}" class="btn btn-secondary btn-lg btn-block" role="button" aria-disabled="true">Jobs for Sol Staff</a>
                        </div>

                        </br>
                        <div class="row">
                            <div class="col-12 col-md-3">
                                <p class="text-justify">At Sol we work together to be the most dynamic and trusted
                                    partner to
                                    each other and our customers, committed to delivering creative solutions that
                                    power journeys
                                    and communities. At Sol, we offer more than your average career. Apply to join
                                    us today!</p>
                            </div>
                            <div class="col-12 col-md-3">
                                <h4>Our Purpose</h4>
                                <hr>
                                <p>Energizing people and business to get them where they want to go.</p>
                            </div>
                            <div class="col-12 col-md-3">
                                <h4>Our Vision</h4>
                                <hr>
                                <p>Energizing people and business to get them where they want to go.</p>
                            </div>
                            <div class="col-12 col-md-3">
                                <h4>Who we are</h4>
                                <hr>
                                <p>Energizing people and business to get them where they want to go.</p>
                            </div>
                        </div>
                        <div class="row">
                            <h4>The Sol Internship Programme</h4>
                            <p class="text-justify">We are looking for interns who demonstrate the ability to think
                                innovatively, act courageously and lead fearlessly. We are looking for interns that
                                can become
                                leaders in their field of study and leaders in their communities. If this sounds
                                like you, then
                                we are looking for you!</p>
                        </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
</div>
@endsection
