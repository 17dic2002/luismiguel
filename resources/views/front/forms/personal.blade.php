@extends('front.layouts.layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/careersStyle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <meta name="_token" content="{{ csrf_token() }}" />
@endsection

@section('content-front')
    @include('front.forms.steps')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="col-lg-12">
                    @include('flash::message')

                    @if ($edit === true)
                        {!! Form::model($application, ['route' => ['application.updatePI', $application->job_id], 'method' => 'patch', 'files' => true]) !!}
                        <input type="hidden" value="true" name="edit"></input>
                        <input type="hidden" value="{{ $application->id }}" name="id"></input>
                    @else
                        <form action="{{ route('application.updatePI', $job->id) }}" method="post" name="piForm"
                            class="form-group">
                    @endif
                    {!! csrf_field() !!}
                    <h2>{{ __('form.personalInfo') }}</h2>
                    <div class="form-row Regform">
                        <div class="form-group col-md-4">
                            <label for="first_name">{{ __('form.firstName') }}</label>
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group col-md-4">
                            <label for="middle_name">{{ __('form.middleName') }}</label>
                            {!! Form::text('middle_name', null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group col-md-4">
                            <label for="last_name">{{ __('form.lastName') }}</label>
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required']) !!}

                        </div>

                        <div class="form-group col-md-6">
                            <label for="email">{{ __('form.emailAddress') }}</label>
                            <input type="email" class="form-control" id="email" name="email" disabled
                                value="{{ Auth::user()->email }}">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="home_phone">{{ __('form.homePhone') }}</label>
                            {!! Form::text('home_phone', null, ['class' => 'form-control', 'required' => 'required']) !!}

                        </div>

                        @if ($hideFields === true)
                            <div class="form-group col-md-12">
                            @else
                                <div class="form-group col-md-6">
                        @endif
                        <label for="mobile_number">{{ __('form.mobileNumber') }}</label>
                        {!! Form::text('mobile_number', null, ['class' => 'form-control', 'required' => 'required']) !!}

                    </div>

                    @if (!($hideFields === true))
                        <!-- <div class="form-group col-md-4">
                                                        <label for="gender">{{ __('form.gender') }}</label>
                                                        {!! Form::select('gender', ['Choose...' => __('form.choose'), 'Female' => __('form.female'), 'Male' => __('form.male'), 'Other' => __('form.other')], null, ['class' => 'form-control']) !!}
                                                    </div>-->
                    @endif
                    <div class="form-group col-md-4">
                        <label for="category">{{ __('form.department') }}</label>
                        {!! Form::select('category', ['Choose...' => __('form.choose'), 'Accounting Finance' => __('form.accountingFinance'), 'Marketing' => __('form.marketing'), 'Operations' => __('form.operations'), 'HR & Administration' => __('form.hrAdministration'), 'Corporate' => __('form.corporate'), 'Supply' => __('form.supply'), 'Aviation' => __('form.aviation'), 'Other' => __('form.other')], null, ['class' => 'form-control', 'id' => 'department', 'required' => 'required']) !!}
                    </div>

                    <div class="form-group col-md-4">
                        <label for="other_category">{{ __('form.otherDepartment') }}</label>
                        {!! Form::text('other_category', null, ['id' => 'inputOtherDept', 'class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled', 'placeholder' => 'Other']) !!}
                    </div>

                    <div class="form-group col-md-4">
                        <label for="type">{{ __('form.type') }}</label>
                        {!! Form::select('type', ['Choose...' => __('form.choose'), 'Full Time' => __('form.fullTime'), 'Part Time' => __('form.partTime'), ' Contractors' => __('form.contractors')], null, ['class' => 'form-control', 'required' => 'required']) !!}
                    </div>

                    <div class="form-group col-md-4">
                        <label for="years_of_experience">{{ __('form.yearsWorkExperience') }}</label>
                        {!! Form::select('years_of_experience', ['Choose...' => __('form.choose'), 'No Work Experience' => __('form.noWorkExperience'), '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '25' => '25', 'More Than 25' => __('form.moreThan')], null, ['class' => 'form-control', 'required' => 'required']) !!}
                    </div>

                    <!--<div class="form-group col-md-4">
                                                    <label for="date_of_birth">{{ __('form.dob') }}</label>
                                                    {!! Form::date('date_of_birth', null, ['class' => 'form-control', 'id' => 'date_of_birth']) !!}
                                                </div> -->

                    <div class="form-group col-md-12"></div>

                    <div class="form-group col-sm-2">
                        <label for="currency">{{ __('form.currency') }}</label>
                        {!! Form::select('currency', ['Choose...' => __('form.choose'), 'BSD (Bahamas)' => 'BSD (Bahamas)', 'DOP (Dominican Peso)' => 'DOP (Dominican Peso)', 'BBD (Barbados)' => 'BBD (Barbados)', 'BMD (Bermuda)' => 'BMD (Bermuda)', 'JMD (Jamaica)' => 'JMD (Jamaica)', 'XCD (Eastern Caribbean)' => 'XCD (Eastern Caribbean)', 'USD (United States)' => 'USD (United States)', 'EURO (Eropean Territories)' => 'EURO (Eropean Territories)', 'BZD (Belize)' => 'BZD (Belize)', 'NAF (Netherlands)' => 'NAF (Netherlands)'], null, ['class' => 'form-control', 'required' => 'required', 'required' => 'required']) !!}
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="inputCsalaryAnual">{{ __('form.currentSalary') }}</label>
                        {!! Form::text('current_salary', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => '00,000', 'required']) !!}
                    </div>

                    <div class="form-group col-sm-3">
                        <label for="period_cs">{{ __('form.period') }}</label>
                        {!! Form::select('period_cs', ['Choose...' => __('form.choose'), 'Annually' => __('form.annually'), 'Monthly' => __('form.monthly'), 'Bi-Weekly' => __('form.biWeekly'), 'Weekly' => __('form.weekly'), 'Twice a Month' => __('form.twiceMonth')], null, ['class' => 'form-control', 'required' => 'required']) !!}


                    </div>

                    <div class="form-group col-sm-2">
                        <label for="currency_expected">{{ __('form.currency') }}</label>
                        {!! Form::select('currency_expected', ['Choose...' => __('form.choose'), 'BSD (Bahamas)' => 'BSD (Bahamas)', 'DOP (Dominican Peso)' => 'DOP (Dominican Peso)', 'BBD (Barbados)' => 'BBD (Barbados)', 'BMD (Bermuda)' => 'BMD (Bermuda)', 'JMD (Jamaica)' => 'JMD (Jamaica)', 'XCD (Eastern Caribbean)' => 'XCD (Eastern Caribbean)', 'USD (United States)' => 'USD (United States)', 'EURO (Eropean Territories)' => 'EURO (Eropean Territories)', 'BZD (Belize)' => 'BZD (Belize)', 'NAF (Netherlands)' => 'NAF (Netherlands)'], null, ['class' => 'form-control', 'required' => 'required']) !!}
                    </div>

                    <div class="form-group col-sm-6">
                        <label for="inputEsalaryAnual">{{ __('form.expectedSalary') }}</label>
                        {!! Form::text('expected_salary', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => '00,000', 'required']) !!}


                    </div>

                    <div class="form-group col-sm-3">
                        <label for="inputEsalaryType">{{ __('form.period') }}</label>
                        {!! Form::select('period_es', ['Choose...' => __('form.choose'), 'Annually' => __('form.annually'), 'Monthly' => __('form.monthly'), 'Bi-Weekly' => __('form.biWeekly'), 'Weekly' => __('form.weekly'), 'Twice a Month' => __('form.twiceMonth')], null, ['class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        <label for="inputAddress">{{ __('form.address') }}</label>
                        {!! Form::text('address', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Ave Boston 1234', 'required']) !!}


                    </div>

                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label"
                        for="inlineRadio1">{{ __('form.validDriverLicense') }}</label>&nbsp&nbsp
                    <input class="form-check-input" type="radio" name="driver_license" id="driverLicenseY" value="Yes"
                        @if ($application->driver_license == 'Yes') checked @endif required>
                    <label class="form-check-label" for="driverLicenseY">{{ __('form.yes') }}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" @if ($application->driver_license == 'No') checked @endif
                        name="driver_license" id="driverLicenseN" value="No">
                    <label class="form-check-label" for="driverLicenseN">No</label>
                </div>
                <br><br>
                <button type="submit" class="btn btn-primary " name="btnApply">{{ __('form.next') }}
                </button>

                </form>
            </div>

        </div>
    </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/forms.js') }}"></script>
@endsection
