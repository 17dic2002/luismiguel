@extends('front.layouts.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta name="_token" content="{{csrf_token()}}"/>
@endsection

@section('content-front')
    @include('front.forms.steps')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="Container col-lg-12-6 ">
                            <div id="emplyform">
                                @if($edit===true)
                                    {!! Form::model($data, ['route' => ['applyForm.updateExperience', $application->id], 'method' => 'patch']) !!}
                                    {!! csrf_field() !!}
                                    @include('flash::message')

                                    <h2>{{__('form.workExperience')}}</h2>
                                    </br>
                                    <p>
                                        <span>{{__('form.startCurrentEmployment')}}</span>
                                    </p>
                                    </br>
                                    <div id="dynamicExperience">
                                        @for ($i = 0; $i < count($data); $i++)

                                            {{ Form::hidden('idApp'. $i, $data[$i]->id) }}

                                            <div id="div_row{{$i}}" class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    <label for="EmplName">{{__('form.employerName')}}</label>
                                                    <input type="text" class="form-control" id="EmplName"
                                                           value="{{$data[$i]->employer_name}}"
                                                           required
                                                           name="experience[{{$i}}][employer_name]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputPosition">{{__('form.position')}}</label>
                                                    <input type="text" class="form-control" id="inputPosition"
                                                           value="{{$data[$i]->position}}"
                                                           required
                                                           name="experience[{{$i}}][position]">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="respon">{{__('form.responsibilities')}}</label>
                                                    <textarea class="form-control" id="respon" rows="3"
                                                              required
                                                              name="experience[{{$i}}][responsabilities]"
                                                              maxlength="2000"
                                                              onkeyup="countChar(this, '{{$i}}-respon' )">{{$data[$i]->responsabilities}}</textarea>
                                                    <div class="text-danger" id="charNum-{{$i}}-respon"></div>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputManagerName">{{__('form.supervisorName')}}</label>
                                                    <input type="text" class="form-control" id="inputManagerName"
                                                           required
                                                           value="{{$data[$i]->supervisors_name}}"
                                                           name="experience[{{$i}}][supervisors_name]">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputPosStartDate">{{__('form.positionStartDate')}}</label>
                                                    <input type="date" class="form-control" id="inputPosStartDate"
                                                           required
                                                           value="{{Carbon\Carbon::parse($data[$i]->position_start_date)->format('Y-m-d')}}"
                                                           name="experience[{{$i}}][position_start_date]">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="posenddate">{{__('form.positionEndDate')}}</label>
                                                    <input type="date" class="form-control" id="posenddate"
                                                           required
                                                           value="{{Carbon\Carbon::parse($data[$i]->position_end_date)->format('Y-m-d')}}"
                                                           name="experience[{{$i}}][position_end_date]">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="inputReasonLeaving">{{__('form.reasonLeaving')}}</label>
                                                    <textarea class="form-control" id="inputReasonLeaving" rows="3"
                                                              required
                                                              name="experience[{{$i}}][reason_leaving]"
                                                              maxlength="2000"
                                                              onkeyup="countChar(this, '{{$i}}-leaving' )">{{$data[$i]->reason_leaving}}</textarea>
                                                    <div class="text-danger" id="charNum-{{$i}}-leaving"></div>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="emplyphone">{{__('form.phoneNumber')}}</label>
                                                    <input type="text" class="form-control" id="emplyphone"
                                                           required
                                                           value="{{$data[$i]->phone}}"
                                                           name="experience[{{$i}}][phone]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="emplyAddress">{{__('form.address')}}</label>
                                                    <input type="text" class="form-control" id="emplyAddress"
                                                           required
                                                           value="{{$data[$i]->address}}"
                                                           name="experience[{{$i}}][address]">
                                                </div>
                                                @if($i != 0)
                                                    <button type="button" id="{{$i}}"
                                                            class="btn btn-danger btn_remove">{{__('form.remove')}}</button>
                                                @endif                                            </div>
                                            <hr id="separator{{$i}}">
                                        @endfor
                                    </div>
                                    <br>
                                    <input type="submit" name="submitEmpl" id="submitEmpl" class="btn btn-info"
                                           value="{{__('form.next')}}"/>
                                    </form>
                                @else
                                    <form action="{{ route('applyForm.saveExperience', $application->id) }}"
                                          method="post"
                                          name="empl_form" id="empl_form">
                                        {!! csrf_field() !!}
                                        @include('flash::message')

                                        <h2>{{__('form.workExperience')}}</h2>
                                        </br>
                                        <p>
                                            <span>{{__('form.startCurrentEmployment')}}</span>
                                        </p>
                                        </br>
                                        <div id="dynamicExperience">
                                            <div class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    <label for="EmplName">{{__('form.employerName')}}</label>
                                                    <input type="text" class="form-control" id="EmplName"
                                                           required
                                                           name="experience[0][employer_name]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputPosition">{{__('form.position')}}</label>
                                                    <input type="text" class="form-control" id="inputPosition"
                                                           name="experience[0][position]">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="respon">{{__('form.responsibilities')}}</label>
                                                    <textarea class="form-control" id="respon" rows="3"
                                                              required
                                                              maxlength="2000"
                                                              name="experience[0][responsabilities]"></textarea>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="inputManagerName">{{__('form.supervisorName')}}</label>
                                                    <input type="text" class="form-control" id="inputManagerName"
                                                           required
                                                           name="experience[0][supervisors_name]">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputPosStartDate">{{__('form.positionStartDate')}}</label>
                                                    <input type="date" class="form-control" id="inputPosStartDate"
                                                           required
                                                           name="experience[0][position_start_date]">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="posenddate">{{__('form.positionEndDate')}}</label>
                                                    <input type="date" class="form-control" id="posenddate"
                                                           required
                                                           name="experience[0][position_end_date]">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <label for="inputReasonLeaving">{{__('form.reasonLeaving')}}</label>
                                                    <textarea class="form-control" id="inputReasonLeaving" rows="3"
                                                              required
                                                              maxlength="2000"
                                                              name="experience[0][reason_leaving]"></textarea>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="emplyphone">{{__('form.phoneNumber')}}</label>
                                                    <input type="text" class="form-control" id="emplyphone"
                                                           required
                                                           name="experience[0][phone]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="emplyAddress">{{__('form.address')}}</label>
                                                    <input type="text" class="form-control" id="emplyAddress"
                                                           required
                                                           name="experience[0][address]">
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <input type="submit" name="submitEmpl" id="submitEmpl" class="btn btn-info"
                                               value="{{__('form.next')}}"/>
                                    </form>
                                @endif
                                <div class="form-group" style="margin-top:10px">

                                    <button class="btn btn-secondary"
                                            id="addExperience">{{__('form.addMore')}} {{__('form.workExperience')}}
                                    </button>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/forms.js')}}"></script>
    <script>

        function countChar(val, index) {
            var len = val.value.length;
            if (len > 2000) {
                val.value = val.value.substring(0, 2000);
            } else {
                charCounter = '#charNum-' + index;
                $(charCounter).text('Characters remaining: ' + (2000-len));
            }
        };

        /**
         * Experience
         * */

            @if($edit===true)
        var i = {{$i}};
            @else
        var i = 1;
        @endif
        $(document).ready(function () {
            $('#addExperience').click(function () {
                $('#dynamicExperience').append('<hr id="separator' + i + '"><div id="div_row' + i + '" class="form-row Regform">\n' +
                    '    <div class="form-group col-md-6">\n' +
                    '        <label for="EmplName">{{__('form.employerName')}}</label>\n' +
                    '        <input type="text" class="form-control" id="EmplName"\n' +
                    '               name="experience[' + i + '][employer_name]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-6">\n' +
                    '        <label for="inputPosition">{{__('form.position')}}</label>\n' +
                    '        <input type="text" class="form-control" id="inputPosition"\n' +
                    '               name="experience[' + i + '][position]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-12">\n' +
                    '        <label for="respon">{{__('form.responsibilities')}}</label>\n' +
                    '        <textarea class="form-control" id="respon" rows="3"\n' +
                    '                  name="experience[' + i + '][responsabilities]"' +
                    '                  maxlength="2000"' +
                    '                  onkeyup="countChar(this,\''+ i +'-respon\')"></textarea>\n' +
                    '        <div><div id="charNum-'+ i +'-respon"></div></div>\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-4">\n' +
                    '        <label for="inputManagerName">{{__('form.supervisorName')}}</label>\n' +
                    '        <input type="text" class="form-control" id="inputManagerName"\n' +
                    '               name="experience[' + i + '][supervisors_name]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-4">\n' +
                    '        <label for="inputPosStartDate">{{__('form.positionStartDate')}}</label>\n' +
                    '        <input type="date" class="form-control" id="inputPosStartDate"\n' +
                    '               name="experience[' + i + '][position_start_date]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-4">\n' +
                    '        <label for="posenddate">{{__('form.positionEndDate')}}</label>\n' +
                    '        <input type="date" class="form-control" id="posenddate"\n' +
                    '               name="experience[' + i + '][position_end_date]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-12">\n' +
                    '        <label for="inputReasonLeaving">{{__('form.reasonLeaving')}}</label>\n' +
                    '        <textarea class="form-control" id="inputReasonLeaving" rows="3"\n'+
                    '                  name="experience[' + i + '][reason_leaving]"' +
                    '                  maxlength="2000"' +
                    '                  onkeyup="countChar(this,\''+ i +'-leaving\')"></textarea>\n' +
                    '           <div><div id="charNum-'+ i +'-leaving"></div></div>\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-6">\n' +
                    '        <label for="emplyphone">{{__('form.phoneNumber')}}</label>\n' +
                    '        <input type="text" class="form-control" id="emplyphone"\n' +
                    '               name="experience[' + i + '][phone]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-6">\n' +
                    '        <label for="emplyAddress">{{__('form.address')}}</label>\n' +
                    '        <input type="text" class="form-control" id="emplyAddress"\n' +
                    '               name="experience[' + i + '][address]">\n' +
                    '    </div>\n' +
                    '     <button type="button" id="' + i + '" class="btn btn-danger btn_remove">{{__('form.remove')}}</button>' +
                    '</div>'
                );
                i++;

            });

            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#div_row' + button_id + '').remove();
                $('#separator' + button_id).remove();
                i--;
            });
        });
    </script>
@endsection
