@extends('front.layouts.layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta name="_token" content="{{csrf_token()}}"/>
@endsection

@section('content-front')
    @include('front.forms.steps')


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="Container col-lg-12-6 ">
                            <div id="refenform">
                                @if($edit===true)
                                    {!! Form::model($data, ['route' => ['applyForm.updateReferences', $application->id], 'method' => 'patch']) !!}

                                    {!! csrf_field() !!}
                                    @include('flash::message')

                                    <h2>{{__('form.references')}}</h2>
                                    <div id="dynamicReferences">
                                        @for ($i = 0; $i < count($data); $i++)

                                            {{ Form::hidden('idApp'. $i, $data[$i]->id) }}
                                            <div id="div_row{{$i}}" class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    <label for="inputRefereeName">{{__('form.nameReferee')}}</label>
                                                    <input type="text" class="form-control" id="inputRefereeName"
                                                           value="{{$data[$i]->name_referee}}"
                                                           name="references[{{$i}}][name_referee]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputJobTitle">{{__('form.jobTitle')}}</label>
                                                    <input type="text" class="form-control" id="inputJobTitle"
                                                           value="{{$data[$i]->job_title}}"
                                                           name="references[{{$i}}][job_title]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputOrganization">{{__('form.organization')}}</label>
                                                    <input type="text" class="form-control" id="inputOrganization"
                                                           value="{{$data[$i]->organization}}"
                                                           name="references[{{$i}}][organization]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label
                                                        for="inputRefeRelation">{{__('form.relationReferee')}}</label>
                                                    <input type="text" class="form-control" id="inputRefeRelation"
                                                           value="{{$data[$i]->relation}}"
                                                           name="references[{{$i}}][relation]" >
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="refenphone">{{__('form.phoneNumber')}}</label>
                                                    <input type="text" class="form-control" id="refenphone"
                                                           value="{{$data[$i]->phone_number}}"
                                                           name="references[{{$i}}][phone_number]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="refenAddress">{{__('form.address')}}</label>
                                                    <input type="text" class="form-control" id="refenAddress"
                                                           value="{{$data[$i]->address}}"
                                                           name="references[{{$i}}][address]" >
                                                </div>

                                                @if($i != 0)
                                                    <button type="button" id="{{$i}}"
                                                            class="btn btn-danger btn_remove">{{__('form.remove')}}</button>
                                                @endif
                                            </div>
                                            <hr id="separator{{$i}}">
                                        @endfor

                                    </div>
                                    <input type="submit" name="submitRef" id="submiRef" class="btn btn-info"
                                           value="{{__('form.next')}}"/>
                                    </form>

                                @else

                                    <form action="{{ route('applyForm.saveReferences', $application->id) }}"
                                          method="post"
                                          name="ref_form" id="ref_form">
                                        {!! csrf_field() !!}
                                        @include('flash::message')

                                        <h2>{{__('form.references')}}</h2>
                                        <div id="dynamicReferences">
                                            <div class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    <label for="inputRefereeName">{{__('form.nameReferee')}}</label>
                                                    <input type="text" class="form-control" id="inputRefereeName"
                                                           name="references[0][name_referee]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputJobTitle">{{__('form.jobTitle')}}</label>
                                                    <input type="text" class="form-control" id="inputJobTitle"
                                                           name="references[0][job_title]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="inputOrganization">{{__('form.organization')}}</label>
                                                    <input type="text" class="form-control" id="inputOrganization"
                                                           name="references[0][organization]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label
                                                        for="inputRefeRelation">{{__('form.relationReferee')}}</label>
                                                    <input type="text" class="form-control" id="inputRefeRelation"
                                                           name="references[0][relation]" >
                                                </div>

                                                <div class="form-group col-md-6">
                                                    <label for="refenphone">{{__('form.phoneNumber')}}</label>
                                                    <input type="text" class="form-control" id="refenphone"
                                                           name="references[0][phone_number]" >
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="refenAddress">{{__('form.address')}}</label>
                                                    <input type="text" class="form-control" id="refenAddress"
                                                           name="references[0][address]" >
                                                </div>


                                            </div>
                                        </div>
                                        <input type="submit" name="submitRef" id="submiRef" class="btn btn-info"
                                               value="{{__('form.next')}}"/>
                                    </form>
                                @endif
                                <div class="form-group" style="margin-top:10px">

                                    <button class="btn btn-secondary"
                                            id="addReferences">{{__('form.addMore')}} {{__('form.references')}}
                                    </button>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/forms.js')}}"></script>
    <script>
        /**
         * References
         * */
            @if($edit===true)
        var i = {{$i}};
            @else
        var i = 1;
        @endif
        $(document).ready(function () {
            $('#addReferences').click(function () {
                $('#dynamicReferences').append('<hr id="separator' + i + '"><div id="div_row' + i + '" class="form-row Regform">\n' +
                    '      <div class="form-group col-md-6">\n' +
                    '          <label for="inputRefereeName">{{__('form.nameReferee')}}</label>\n' +
                    '          <input type="text" class="form-control" id="inputRefereeName"\n' +
                    '                 name="references[' + i + '][name_referee]">\n' +
                    '      </div>\n' +
                    '      <div class="form-group col-md-6">\n' +
                    '          <label for="inputJobTitle">{{__('form.jobTitle')}}</label>\n' +
                    '          <input type="text" class="form-control" id="inputJobTitle"\n' +
                    '                 name="references[' + i + '][job_title]">\n' +
                    '      </div>\n' +
                    '      <div class="form-group col-md-6">\n' +
                    '          <label for="inputOrganization">{{__('form.organization')}}</label>\n' +
                    '          <input type="text" class="form-control" id="inputOrganization"\n' +
                    '                 name="references[' + i + '][organization]">\n' +
                    '      </div>\n' +
                    '      <div class="form-group col-md-6">\n' +
                    '          <label for="inputRefeRelation">{{__('form.relationReferee')}}</label>\n' +
                    '          <input type="text" class="form-control" id="inputRefeRelation"\n' +
                    '                 name="references[' + i + '][relation]">\n' +
                    '      </div>\n' +
                    '      <div class="form-group col-md-6">\n' +
                    '          <label for="refenphone">{{__('form.phoneNumber')}}</label>\n' +
                    '          <input type="text" class="form-control" id="refenphone"\n' +
                    '                 name="references[' + i + '][phone_number]">\n' +
                    '      </div>\n' +
                    '      <div class="form-group col-md-6">\n' +
                    '          <label for="refenAddress">{{__('form.address')}}</label>\n' +
                    '          <input type="text" class="form-control" id="refenAddress"\n' +
                    '                 name="references[' + i + '][address]">\n' +
                    '      </div>\n' +
                    '     <button type="button" id="' + i + '" class="btn btn-danger btn_remove">{{__('form.remove')}}</button>' +
                    '  </div> '
                );
                i++;

            });

            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#div_row' + button_id + '').remove();
                $('#separator' + button_id).remove();
                i--;
            });
        });


    </script>
@endsection
