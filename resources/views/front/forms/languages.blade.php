@extends('front.layouts.layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta name="_token" content="{{csrf_token()}}"/>
@endsection

@section('content-front')
    @include('front.forms.steps')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="Container col-lg-12-6 ">
                            <div id="langform">
                                @if($edit===true)
                                    {!! Form::model($data, ['route' => ['applyForm.updateLanguages', $application->id], 'method' => 'patch']) !!}
                                    {!! csrf_field() !!}
                                    @include('flash::message')

                                    <h2>{{__('form.languages')}}</h2>


                                    <div id="dynamicLanguages">
                                        @for ($i = 0; $i < count($data); $i++)


                                            <div id="div_row{{$i}}" class="form-row Regform">
                                                {{ Form::hidden('idApp'. $i, $data[$i]->id) }}

                                                <div class="form-group col-md-6">
                                                    <label for="inputLanguage">{{__('form.language')}}</label>
                                                    <input type="text" class="form-control" id="inputLanguage"
                                                           value="{{$data[$i]->language}}"
                                                           required
                                                           name="languages[{{$i}}][language]">
                                                </div>
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputUnderstandProf">{{__('form.understandingProficiency')}}</label>
                                                    <select id="inputUnderstandProf" class="form-control"
                                                            required
                                                            name="languages[{{$i}}][understanding]">
                                                        <option>{{__('form.choose')}}</option>
                                                        @if($data[$i]->understanding == __('form.intermediate'))
                                                            <option selected>{{__('form.intermediate')}}</option>
                                                        @else
                                                            <option>{{__('form.intermediate')}}</option>
                                                        @endif

                                                        @if($data[$i]->understanding == __('form.upperIntermediate'))
                                                            <option selected>{{__('form.upperIntermediate')}}</option>
                                                        @else
                                                            <option>{{__('form.upperIntermediate')}}</option>
                                                        @endif

                                                        @if($data[$i]->understanding == __('form.advanced'))
                                                            <option selected>{{__('form.advanced')}}</option>
                                                        @else
                                                            <option>{{__('form.advanced')}}</option>
                                                        @endif

                                                        @if($data[$i]->understanding == __('form.proficient'))
                                                            <option selected>{{__('form.proficient')}}</option>
                                                        @else
                                                            <option>{{__('form.proficient')}}</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputWritingProf">{{__('form.writingProficiency')}}</label>
                                                    <select id="inputWritingProf" class="form-control"
                                                            required name="languages[{{$i}}][writing]">
                                                        <option>{{__('form.choose')}}</option>
                                                        @if($data[$i]->writing == __('form.intermediate'))
                                                            <option selected>{{__('form.intermediate')}}</option>
                                                        @else
                                                            <option>{{__('form.intermediate')}}</option>
                                                        @endif

                                                        @if($data[$i]->writing == __('form.upperIntermediate'))
                                                            <option selected>{{__('form.upperIntermediate')}}</option>
                                                        @else
                                                            <option>{{__('form.upperIntermediate')}}</option>
                                                        @endif

                                                        @if($data[$i]->writing == __('form.advanced'))
                                                            <option selected>{{__('form.advanced')}}</option>
                                                        @else
                                                            <option>{{__('form.advanced')}}</option>
                                                        @endif

                                                        @if($data[$i]->writing == __('form.proficient'))
                                                            <option selected>{{__('form.proficient')}}</option>
                                                        @else
                                                            <option>{{__('form.proficient')}}</option>
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputReadingProf">{{__('form.readingProficiency')}}</label>
                                                    <select id="inputReadingProf"
                                                            required class="form-control"
                                                            name="languages[{{$i}}][reading]">
                                                        <option>{{__('form.choose')}}</option>
                                                        @if($data[$i]->reading == __('form.intermediate'))
                                                            <option selected>{{__('form.intermediate')}}</option>
                                                        @else
                                                            <option>{{__('form.intermediate')}}</option>
                                                        @endif

                                                        @if($data[$i]->reading == __('form.upperIntermediate'))
                                                            <option selected>{{__('form.upperIntermediate')}}</option>
                                                        @else
                                                            <option>{{__('form.upperIntermediate')}}</option>
                                                        @endif

                                                        @if($data[$i]->reading == __('form.advanced'))
                                                            <option selected>{{__('form.advanced')}}</option>
                                                        @else
                                                            <option>{{__('form.advanced')}}</option>
                                                        @endif

                                                        @if($data[$i]->reading == __('form.proficient'))
                                                            <option selected>{{__('form.proficient')}}</option>
                                                        @else
                                                            <option>{{__('form.proficient')}}</option>
                                                        @endif
                                                    </select>
                                                </div>


                                                <div class="form-group col-md-12">
                                                    <label
                                                        for="inputPlaceLearned">{{__('form.whereDidYouLearnIt')}}</label>
                                                    <input type="text"
                                                           value="{{$data[$i]->where_learn}}"
                                                           required class="form-control" id="inputPlaceLearned"
                                                           name="languages[{{$i}}][where_learn]">
                                                </div>


                                                @if($i != 0)
                                                    <button type="button" id="{{$i}}"
                                                            class="btn btn-danger btn_remove">{{__('form.remove')}}</button>
                                                @endif
                                            </div>

                                            <hr id="separator{{$i}}">
                                        @endfor
                                    </div>
                                    <input type="submit" name="submitLang" id="submiLang" class="btn btn-info"
                                           value="{{__('form.next')}}"/>
                                    </form>
                                @else
                                    <form action="{{ route('applyForm.saveLanguages', $application->id) }}"
                                          method="post"
                                          name="lang_form" id="lang_form">
                                        {!! csrf_field() !!}
                                        @include('flash::message')

                                        <h2>{{__('form.languages')}}</h2>


                                        <div id="dynamicLanguages">
                                            <div class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    <label for="inputLanguage">{{__('form.language')}}</label>
                                                    <input type="text" class="form-control" id="inputLanguage"
                                                           required
                                                           name="languages[0][language]">
                                                </div>
                                                <div class="form-group col-md-6"></div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputUnderstandProf">{{__('form.understandingProficiency')}}</label>
                                                    <select id="inputUnderstandProf" class="form-control"
                                                            required
                                                            name="languages[0][understanding]">
                                                        <option selected>{{__('form.choose')}}</option>
                                                        <option>{{__('form.intermediate')}}</option>
                                                        <option>{{__('form.upperIntermediate')}}</option>
                                                        <option>{{__('form.advanced')}}</option>
                                                        <option>{{__('form.proficient')}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputWritingProf">{{__('form.writingProficiency')}}</label>
                                                    <select id="inputWritingProf" class="form-control"
                                                            required name="languages[0][writing]">
                                                        <option selected>{{__('form.choose')}}</option>
                                                        <option>{{__('form.intermediate')}}</option>
                                                        <option>{{__('form.upperIntermediate')}}</option>
                                                        <option>{{__('form.advanced')}}</option>
                                                        <option>{{__('form.proficient')}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label
                                                        for="inputReadingProf">{{__('form.readingProficiency')}}</label>
                                                    <select id="inputReadingProf"
                                                            required class="form-control" name="languages[0][reading]">
                                                        <option selected>{{__('form.choose')}}</option>
                                                        <option>{{__('form.intermediate')}}</option>
                                                        <option>{{__('form.upperIntermediate')}}</option>
                                                        <option>{{__('form.advanced')}}</option>
                                                        <option>{{__('form.proficient')}}</option>
                                                    </select>
                                                </div>


                                                <div class="form-group col-md-12">
                                                    <label
                                                        for="inputPlaceLearned">{{__('form.whereDidYouLearnIt')}}</label>
                                                    <input type="text"
                                                           required class="form-control" id="inputPlaceLearned"
                                                           name="languages[0][where_learn]">
                                                </div>


                                            </div>
                                        </div>
                                        <input type="submit" name="submitLang" id="submiLang" class="btn btn-info"
                                               value="{{__('form.next')}}"/>
                                    </form>
                                @endif
                                <div class="form-group" style="margin-top:10px">

                                    <button class="btn btn-secondary"
                                            id="addLanguages">{{__('form.addMore')}} {{__('form.languages')}}
                                    </button>
                                </div>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/forms.js')}}"></script>
    <script>
        /**
         * Languages
         * */

        var i = 1;
        $(document).ready(function () {
            $('#addLanguages').click(function () {
                $('#dynamicLanguages').append('<hr id="separator' + i + '"><div id="div_row' + i + '" class="form-row Regform">\n' +
                    '    <div class="form-group col-md-6">\n' +
                    '        <label for="inputLanguage">{{__('form.language')}}</label>\n' +
                    '        <input type="text" class="form-control" id="inputLanguage"\n' +
                    '               name="languages[' + i + '][language]">\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-6"></div>\n' +
                    '    <div class="form-group col-md-4">\n' +
                    '        <label for="inputUnderstandProf">{{__('form.understandingProficiency')}}</label>\n' +
                    '        <select id="inputUnderstandProf" class="form-control"\n' +
                    '                name="languages[' + i + '][understanding]">\n' +
                    '            <option selected>{{__('form.choose')}}</option>\n' +
                    '            <option>{{__('form.intermediate')}}</option>\n' +
                    '            <option>{{__('form.upperIntermediate')}}</option>\n' +
                    '            <option>{{__('form.advanced')}}</option>\n' +
                    '            <option>{{__('form.proficient')}}</option>\n' +
                    '        </select>\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-4">\n' +
                    '        <label for="inputWritingProf">{{__('form.writingProficiency')}}</label>\n' +
                    '        <select id="inputWritingProf" class="form-control" name="languages[' + i + '][writing]">\n' +
                    '             <option selected>{{__('form.choose')}}</option>\n' +
                    '            <option>{{__('form.intermediate')}}</option>\n' +
                    '            <option>{{__('form.upperIntermediate')}}</option>\n' +
                    '            <option>{{__('form.advanced')}}</option>\n' +
                    '            <option>{{__('form.proficient')}}</option>\n' +
                    '        </select>\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-4">\n' +
                    '        <label for="inputReadingProf">{{__('form.readingProficiency')}}</label>\n' +
                    '        <select id="inputReadingProf" class="form-control" name="languages[' + i + '][reading]">\n' +
                    '             <option selected>{{__('form.choose')}}</option>\n' +
                    '            <option>{{__('form.intermediate')}}</option>\n' +
                    '            <option>{{__('form.upperIntermediate')}}</option>\n' +
                    '            <option>{{__('form.advanced')}}</option>\n' +
                    '            <option>{{__('form.proficient')}}</option>\n' +
                    '        </select>\n' +
                    '    </div>\n' +
                    '    <div class="form-group col-md-12">\n' +
                    '        <label for="inputPlaceLearned">{{__('form.whereDidYouLearnIt')}}</label>\n' +
                    '        <input type="text" class="form-control" id="inputPlaceLearned"\n' +
                    '               name="languages[' + i + '][where_learn]">\n' +
                    '    </div>\n' +
                    '     <button type="button" id="' + i + '" class="btn btn-danger btn_remove">{{__('form.remove')}}</button>' +
                    '</div> '
                );
                i++;

            });

            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#div_row' + button_id + '').remove();
                $('#separator' + button_id).remove();
                i--;
            });
        });

    </script>
@endsection
