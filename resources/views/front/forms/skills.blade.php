@extends('front.layouts.layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/careersStyle.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <meta name="_token" content="{{ csrf_token() }}" />
@endsection

@section('content-front')
    @include('front.forms.steps')


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="Container col-lg-12-6 ">
                            <div id="skilsform">
                                @if ($edit === true)
                                    {!! Form::model($data, ['route' => ['applyForm.updateSkills', $application->id], 'method' => 'patch']) !!}
                                @else
                                    <form action="{{ route('applyForm.saveSkills', $application->id) }}" method="post"
                                        name="skill_form" id="skill_form">
                                @endif
                                {!! csrf_field() !!}
                                @include('flash::message')

                                <h2>{{ __('form.skills') }}</h2>
                                <div id="dySkill_field">
                                    <div class="form-row Regform">
                                            <div class="form-group col-md-12">
                                                <label for="list_skills">{{__('form.listSkills')}}</label>
                                                <textarea class="form-control" id="list_skills" rows="3"
                                                          required
                                                          name="list_skills"
                                                          maxlength="2000"
                                                          onkeyup="countChar(this, '1-respon' )">{{$data->list_skills}}</textarea>
                                                <div class="text" id="charNum-1-respon"></div>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="professional_qualifications_skills">{{__('form.professionalQualifications')}}</label>
                                                <textarea class="form-control" id="professional_qualifications_skills" rows="3"
                                                          required
                                                          name="professional_qualifications_skills"
                                                          maxlength="2000"
                                                          onkeyup="countChar(this, '2-respon' )">{{$data->professional_qualifications_skills}}</textarea>
                                                <div class="text" id="charNum-2-respon"></div>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label for="other_relevant_skills">{{__('form.otherRelevantEducational')}}</label>
                                                <textarea class="form-control" id="other_relevant_skills" rows="3"
                                                          required
                                                          name="other_relevant_skills"
                                                          maxlength="2000"
                                                          onkeyup="countChar(this, '3-respon' )">{{$data->other_relevant_skills}}</textarea>
                                                <div class="text" id="charNum-3-respon"></div>
                                            </div>

                                                {{-- {!! Form::textarea('other_relevant_skills', null, ['class' => 'form-control', 'required' => 'required', 'rows' => '3', 'maxlength' => '2000']) !!}
                                            </div> --}}
                                            <div class="form-group col-md-12">
                                                <label for="hobbies">{{__('form.interestActivitiesHobbies')}}</label>
                                                <textarea class="form-control" id="hobbies" rows="3"
                                                          required
                                                          name="hobbies"
                                                          maxlength="2000"
                                                          onkeyup="countChar(this, '4-respon' )">{{$data->hobbies}}</textarea>
                                                <div class="text" id="charNum-4-respon"></div>
                                            </div>
                                            {{-- <div class="form-group col-md-12">
                                                {!! Form::label('hobbies', __('form.interestActivitiesHobbies')) !!}
                                                {!! Form::textarea('hobbies', null, ['class' => 'form-control', 'required' => 'required', 'rows' => '3', 'maxlength' => '2000']) !!}
                                            </div> --}}
                                        </div>
                                    </div>
                                    <input type="submit" name="submitSkill" id="submitSkill" class="btn btn-info"
                                        value="{{ __('form.save') }} {{ __('form.skills') }}" />
                                    </form>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @section('scripts')
        <script>
            function countChar(val, index) {
            var len = val.value.length;
            if (len > 2000) {
                val.value = val.value.substring(0, 2000);
            } else {
                charCounter = '#charNum-' + index;
                $(charCounter).text('Characters remaining: ' + (2000-len));
            }
        };
        </script>
    @endsection
