@extends('front.layouts.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta name="_token" content="{{csrf_token()}}"/>
@endsection

@section('content-front')
    @include('front.forms.steps')

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="Container col-lg-12-6 ">

                            <div id="eduform">
                                @include('flash::message')
                                @if($edit===true)
                                    {!! Form::model($data, ['route' => ['applyForm.updateEducation', $application->id], 'method' => 'patch']) !!}

                                    {!! csrf_field() !!}

                                    <h2>{{__('form.education')}}</h2>

                                    <div id="dynamic_field">

                                        @for ($i = 0; $i < count($data); $i++)
                                            <div id="div_row{{$i}}" class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    {{ Form::hidden('idApp'. $i, $data[$i]->id) }}
                                                    <label for="Institud">{{__('form.university')}}</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$data[$i]->institute}}"
                                                           id="institute"
                                                           required
                                                           name="education[{{$i}}][institute]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="studyarea">{{__('form.studyArea')}}</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$data[$i]->study_area}}"
                                                           id="studyarea"
                                                           required
                                                           name="education[{{$i}}][study_area]">
                                                </div>
                                                <div class="form-group col-sm-8">
                                                    <label for="inputDegree">{{__('form.certificateDegree')}}</label>
                                                    <select onchange='checkIfOtherDegree({{$i}})'
                                                            id="inputDegree{{$i}}"
                                                            value="{{$data[$i]->certificate}}"
                                                            class="form-control"
                                                            name="education[{{$i}}][certificate]" required>
                                                        <option>{{__('form.choose')}}</option>
                                                        @if($data[$i]->certificate == __('form.postGraduate'))
                                                            <option selected>{{__('form.postGraduate')}}</option>
                                                        @else
                                                            <option>{{__('form.postGraduate')}}</option>
                                                        @endif

                                                        @if($data[$i]->certificate == __('form.graduate'))
                                                            <option selected>{{__('form.graduate')}}</option>
                                                        @else
                                                            <option>{{__('form.graduate')}}</option>
                                                        @endif

                                                        @if($data[$i]->certificate == __('form.highSchool'))
                                                            <option selected>{{__('form.highSchool')}}</option>
                                                        @else
                                                            <option>{{__('form.highSchool')}}l</option>
                                                        @endif

                                                        @if($data[$i]->certificate == __('form.other'))
                                                            <option selected>{{__('form.other')}}</option>
                                                        @else
                                                            <option>{{__('form.other')}}</option>
                                                        @endif

                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="otherDegree">{{__('form.otherCertificate')}}</label>
                                                    <input type="text" class="form-control"
                                                           value="{{$data[$i]->other}}"
                                                           id="otherDegree{{$i}}"
                                                           required
                                                           name="education[{{$i}}][other]" disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="datefrom">{{__('form.dateFrom')}}</label>
                                                    <input type="date" class="form-control"
                                                           value="{{Carbon\Carbon::parse($data[$i]->date_from)->format('Y-m-d')}}"
                                                           id="date_from"
                                                           required
                                                           name="education[{{$i}}][date_from]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="dateto">{{__('form.dateTo')}}</label>
                                                    <input type="date" class="form-control" id="dateto"
                                                           value="{{Carbon\Carbon::parse($data[$i]->date_to)->format('Y-m-d')}}"
                                                           required
                                                           name="education[{{$i}}][date_to]">
                                                </div>

                                                @if($i != 0)
                                                    <button type="button" id="{{$i}}"
                                                            class="btn btn-danger btn_remove">{{__('form.remove')}}</button>
                                                @endif
                                            </div>

                                            <hr id="separator{{$i}}">
                                        @endfor
                                    </div>
                                    <input type="submit" name="submit" id="submit"
                                           class="btn btn-info"
                                           value="{{__('form.next')}}" style="margin-top:10px"/>
                                    </form>
                                @else
                                    <form action="{{ route('applyForm.saveEducation', $application->id) }}"
                                          method="post"
                                          name="edu_form">
                                        {!! csrf_field() !!}

                                        <h2>{{__('form.education')}}</h2>
                                        <div id="dynamic_field">
                                            <div class="form-row Regform">
                                                <div class="form-group col-md-6">
                                                    <label for="Institud">{{__('form.university')}}</label>
                                                    <input type="text" class="form-control"
                                                           id="institute"
                                                           required
                                                           name="education[0][institute]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="studyarea">{{__('form.studyArea')}}</label>
                                                    <input type="text" class="form-control"
                                                           id="studyarea"
                                                           required
                                                           name="education[0][study_area]">
                                                </div>
                                                <div class="form-group col-sm-8">
                                                    <label for="inputDegree">{{__('form.certificateDegree')}}</label>
                                                    <select onchange='checkIfOtherDegree(0)'
                                                            id="inputDegree0"
                                                            required
                                                            class="form-control"
                                                            name="education[0][certificate]">
                                                        <option selected>{{__('form.choose')}}</option>
                                                        <option>{{__('form.postGraduate')}}</option>
                                                        <option>{{__('form.graduate')}}</option>
                                                        <option>{{__('form.highSchool')}}l</option>
                                                        <option>{{__('form.other')}}</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="otherDegree">{{__('form.otherCertificate')}}</label>
                                                    <input type="text" class="form-control"
                                                           id="otherDegree0"
                                                           required
                                                           name="education[0][other]" disabled>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="datefrom">{{__('form.dateFrom')}}</label>
                                                    <input type="date" class="form-control"
                                                           id="date_from"
                                                           required
                                                           name="education[0][date_from]">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="dateto">{{__('form.dateTo')}}</label>
                                                    <input type="date" class="form-control" id="dateto"
                                                           required
                                                           name="education[0][date_to]">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="submit" name="submit" id="submit"
                                               class="btn btn-info"
                                               value="{{__('form.next')}}" style="margin-top:10px"/>
                                    </form>
                                @endif
                                <div class="form-group" style="margin-top:10px">

                                    <button class="btn btn-secondary"
                                            id="addelement">{{__('form.addMore')}} {{__('form.education')}}
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
@endsection

@section('scripts')
    <script src="{{asset('js/forms.js')}}"></script>
    <script>
        /**
         * Education
         * */
        $(document).ready(function () {
                @if($edit===true)
            var i = {{$i}};
                @else
            var i = 1;
            @endif
            $('#addelement').click(function () {

                $('#dynamic_field').append('<hr id="separator' + i + '"><div id="div_row' + i + '" class="form-row Regform">\n' +
                    '         <div class="form-group col-md-6">\n' +
                    '             <label for="Institud"> {{__('form.university')}}</label>\n' +
                    '             <input type="text" class="form-control"\n' +
                    '                    id="institute"\n' +
                    '                    name="education[' + i + '][institute]">\n' +
                    '         </div>\n' +
                    '         <div class="form-group col-md-6">\n' +
                    '             <label for="studyarea">{{__('form.studyArea')}}</label>\n' +
                    '             <input type="text" class="form-control"\n' +
                    '                    id="studyarea"\n' +
                    '                    name="education[' + i + '][study_area]">\n' +
                    '         </div>\n' +
                    '         <div class="form-group col-sm-8">\n' +
                    '             <label for="inputDegree">{{__('form.certificateDegree')}}\n' +
                    '                 </label>\n' +
                    '             <select onchange=\'checkIfOtherDegree(' + i + ')\'\n' +
                    '                     id="inputDegree' + i + '"\n' +
                    '                     class="form-control"\n' +
                    '                     name="education[' + i + '][certificate]">\n' +
                    '                 <option selected>{{__('form.choose')}}</option>\n' +
                    '                 <option>{{__('form.postGraduate')}}</option>\n' +
                    '                 <option>{{__('form.graduate')}}</option>\n' +
                    '                 <option>{{__('form.highSchool')}}</option>\n' +
                    '                 <option>{{__('form.other')}}</option>\n' +
                    '             </select>\n' +
                    '         </div>\n' +
                    '         <div class="form-group col-sm-4">\n' +
                    '             <label for="otherDegree">{{__('form.otherCertificate')}}\n' +
                    '                 received</label>\n' +
                    '             <input type="text" class="form-control"\n' +
                    '                    id="otherDegree' + i + '"\n' +
                    '                    name="education[' + i + '][other]" disabled>\n' +
                    '         </div>\n' +
                    '         <div class="form-group col-md-6">\n' +
                    '             <label for="datefrom">{{__('form.dateFrom')}}</label>\n' +
                    '             <input type="date" class="form-control"\n' +
                    '                    id="datefrom"\n' +
                    '                    name="education[' + i + '][date_from]">\n' +
                    '         </div>\n' +
                    '         <div class="form-group col-md-6">\n' +
                    '             <label for="dateto">{{__('form.dateTo')}}</label>\n' +
                    '             <input type="date" class="form-control" id="dateto"\n' +
                    '                    name="education[' + i + '][date_to]">\n' +
                    '         </div>\n' +
                    '     <button type="button" id="' + i + '" class="btn btn-danger btn_remove">{{__('form.remove')}}</button>' +
                    '</div>');
                i++;
            });
            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#div_row' + button_id + '').remove();
                $('#separator' + button_id).remove();
                i--;
            });
        });
    </script>

@endsection
