@extends('front.layouts.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta name="_token" content="{{csrf_token()}}"/>
@endsection

@section('content-front')


    <div class="row">
        <div class="card">
            <div class="Container col-lg-12-6 ">
                <div id="eduform">

                    @include('flash::message')

                    <h2>{{__('form.thankYou_1')}}</h2>
                    <h2>{{__('form.thankYou_2')}}</h2>
                    <!--<h2>{{__('form.regards')}}</h2>-->
                </div>
                <div class="col text-center">
                    <a href="{{route('home')}}"><button class="btn btn-primary">{{__('form.goHome')}}</button></a>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('js/forms.js')}}"></script>
@endsection
