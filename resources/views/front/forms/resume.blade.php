@extends('front.layouts.layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <meta name="_token" content="{{csrf_token()}}"/>
@endsection

@section('content-front')
    @include('front.forms.steps')


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="form-row">
                    <div class="col-lg-12">
                        <div class="Container col-lg-12-6 ">
                            <div id="langform">
                                @if($edit===true)
                                    {!! Form::model($data, ['route' => ['applyForm.updateResume', $application->id], 'method' => 'patch','files' => 'true','enctype'=>'multipart/form-data']) !!}
                                @else
                                <form action="{{ route('applyForm.saveResume', $application->id) }}" method="post"
                                      enctype="multipart/form-data"
                                      name="CusForm" id="skill_form">
                                    @endif
                                    {!! csrf_field() !!}
                                    @include('flash::message')
                                    <h2>{{__('form.resume')}}</h2>

                                    <div class="form-row Regform">
                                        <div class="form-group col-md-6">
                                            <label for="how_hear">{{__('form.howLearnAboutVacancy')}}</label>
                                            {!! Form::select('how_hear', ['Choose...' => __('form.choose'), 'Career Center' => __('form.careerCenter'), 'E-Blast' => 'E-Blast', 'Employee of the Company' => __('form.employeeCompany'), 'Facebook' => 'Facebook', 'Instagram' => 'Instagram', 'Job Board' => __('form.jobBoard'), 'LinkedIn' => 'LinkedIn', 'Newspaper' => __('form.newspaper'), 'Word-of-Mouth' => __('form.word-of-Mouth'), 'Other' => __('form.other')], null, ['class' => 'form-control','id'=>'inputhearaboutus','required'=>'required']) !!}
                                        </div>
                                        <div class="form-group col-md-6">
                                            {!! Form::label('otherhear', __('form.other')) !!}
                                            {!! Form::text('otherhear', null, ['class' => 'form-control','disabled'=>'disabled', 'id'=>'inputotherhear']) !!}
                                        </div>

                                        <div class="form-group col-md-12">
                                            {!! Form::label('resume', __('form.resume')) !!}
                                            <!-- File input field -->
                                            <input type="file" id="resume" name="resume" class="form-control-file" accept=".pdf"required />
                                            {{-- {!! Form::file('resume',['class' => 'form-control-file', 'required'=>'required']) !!} --}}
                                        </div>
                                        <div class="form-group form-check col-md-12">
                                            <input type="checkbox" class="form-check-input" id="checkDeclaration"
                                                   name="checkDeclaration" required>
                                            <label class="form-check-label"
                                                   for="checkDeclaration">{{__('form.declaration_1')}}</label>
                                        </div>
                                        </br>
                                        <p>{{__('form.declaration_2')}}</p>

                                        <input type="hidden" value="{{app()->getLocale()}}" name="locale">
                                        <button type="submit" class="btn btn-primary"
                                                name="btnSubResume">{{__('form.submit')}}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    </script>
@endsection
