<div class="row">
    <div class="col-lg-12">

        <ul class="step d-flex flex-nowrap">
            <li class="step-item @if(Request::is('*/personal-information'))active @endif">
                <a href="{{ route('applyForm', $job->id) }}" class="">{{__('form.personalInfo')}}</a>
            </li>

            @if(isset($application))
                @if( $application->step >= 1)
                    <li class="step-item @if(Request::is('*/education')) active @endif">
                        <a href="{{ route('applyForm.education', $application->id) }}"
                           class="">{{__('form.education')}}</a>
                    </li>
                @endif
                @if( $application->step >= 2)
                    <li class="step-item @if(Request::is('*/experience'))active @endif">
                        <a href="{{ route('applyForm.experience', $application->id) }}"
                           class="">{{__('form.workExperience')}}</a>
                    </li>
                @else
                    <li class="step-item ">
                        <a href="#!" class="">{{__('form.workExperience')}}</a>
                    </li>
                @endif

                @if( $application->step >= 3)
                    <li class="step-item @if(Request::is('*/skills'))active @endif">
                        <a href="{{ route('applyForm.skills', $application->id) }}" class="">{{__('form.skills')}}</a>
                    </li>
                @else
                    <li class="step-item ">
                        <a href="#!" class="">{{__('form.skills')}}</a>
                    </li>
                @endif

                @if( $application->step >= 4)
                    <li class="step-item @if(Request::is('*/references'))active @endif">
                        <a href="{{ route('applyForm.references', $application->id) }}"
                           class="">{{__('form.references')}}</a>
                    </li>
                @else
                    <li class="step-item ">
                        <a href="#!" class="">{{__('form.references')}}</a>
                    </li>
                @endif
                @if( $application->step >= 5)
                    <li class="step-item @if(Request::is('*/languages'))active @endif">
                        <a href="{{ route('applyForm.languages', $application->id) }}" class="">{{__('form.languages')}}</a>
                    </li>
                @else
                    <li class="step-item ">
                        <a href="#" class="">{{__('form.languages')}}</a>
                    </li>
                @endif
                @if( $application->step >= 6)
                    <li class="step-item @if(Request::is('*/resume'))active @endif">
                        <a href="{{ route('applyForm.resume', $application->id) }}" class="">{{__('form.resume')}}</a>
                    </li>
                @else
                    <li class="step-item ">
                        <a href="#!" class="">{{__('form.resume')}}</a>
                    </li>
                @endif
            @else
                <li class="step-item ">
                    <a href="" class="">{{__('form.education')}}</a>
                </li>

                <li class="step-item ">
                    <a href="#!" class="">{{__('form.workExperience')}}</a>
                </li>

                <li class="step-item ">
                    <a href="#!" class="">{{__('form.skills')}}</a>
                </li>
                <li class="step-item ">
                    <a href="#!" class="">{{__('form.references')}}</a>
                </li>
                <li class="step-item ">
                    <a href="#!" class="">{{__('form.languages')}}</a>
                </li>
                <li class="step-item ">
                    <a href="#!" class="">{{__('form.resume')}}</a>
                </li>
            @endif
        </ul>
    </div>
</div>
