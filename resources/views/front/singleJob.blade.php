@extends('front.layouts.layout')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/viewjob.css')}}">
   
@endsection

@section('content-front')
    <div class="content">
        <div class="returnbutton">
            <a href="{{route('careers')}}">

                <button type="button" class="btn btn-outline-primary btn-lg"><i class="fa fa-arrow-left"></i> Back to
                    Careers
                </button>
            </a>
        </div>

        <div class="jumbotron">

            <h1 class="display-4">{{$job->title}}</h1>
            <p class="lead"></p>

        </div>
        <div class="jdescript">
            <div id="jsummary">
                <h4 id="hsum">Summary</h4>
                <ul style="list-style-type:none">
                    <li><span class="heading">Job ID:</span>{{$job->id}}</li>
                    <hr>
                    <li><span class="heading">Title:</span>{{$job->title}}</li>
                    <hr>
                    <li><span class="heading">Location:</span> {{$job->location}}</li>
                    <hr>
                    <li><span class="heading">Status:</span> {{$job->status}}
                        @if($job->status == 'Active')
                            <span class="dot" style="background-color:green;"></span>
                        @else
                            <span class="dot"></span>
                        @endif
                    </li>
                </ul>
            </div>
            <div class="jdescript">
                <p><h4>Description:</h4> </p>
            </div>

            <div class="jdescript">
                <div class="container">
                    <embed src="{{ asset('uploads/jobs/'.$job->description) }}" style="width:100%; height:800px;"
                           frameborder="0">
                </div>
            </div>
            <form action="{{ route('applyFormPost',['id'=>$job->id]) }}" method="post" name="piForm" class="form-group">
                @csrf
                <div id="btnapply" class="form-group">
                    <label for="langSelector">Select the form language:</label>
                    <select name="lang" id="langSelector" class="form-control" style="margin: auto; width: inherit;">
                        <option value="en">English</option>
                        <option value="es">Spanish</option>
                        <option value="fr">French</option>
                    </select>
                    <br>
                    <input type="submit" class="btn btn-outline-primary btn-lg" value="Apply">
                </div>
            </form>
        </div>
    </div>
@endsection
