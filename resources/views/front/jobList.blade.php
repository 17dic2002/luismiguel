@extends('front.layouts.layout')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/solHomeHR.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/careersStyle.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
@endsection

@section('content-front')
<div class="content-home-hr">
    <h1 class="center">
        @if(Request::is('internal/careers'))
            All Internal Jobs for Sol Staff
        @else
            All Jobs
        @endif
    </h1>
    <div class="main-container col-lg-12-6">

        <div class="table-responsive">
            <div class="table-responsive">
                <table class="table table-striped" id="jobtable" border="0">
                    <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Title</th>
                            <th scope="col">Territory</th>
                            <th scope="col">Start Date</th>
                            <th scope="col">End Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jobs as $job)
                        <tr>

                            <td>{{$job->id}}</td>
                            <td>{{$job->title}}</td>
                            <td>{{$job->location}}</td>
                            <td>{{$job->start_publish}}</td>
                            <td>{{$job->stop_publish}}</td>
                            <td>{{$job->status}}</td>
                            <td width="10%"><a href="{{route('viewJob',['id'=>$job->id])}}" class="button buttonW button1 btn" role="button">View</a>
                            </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
