@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

       <div class="container">
		<div>
			</br>
			</br>
			<h1 class="display-4">Welcome to the Sol HR Dashboard</h1>
			<hr>
			</br>

			<p>This tool was created to help you manage all tasks required to post a job in a simple and efficient manner, in order to support your objective of obtaining the best talent for the company. The Sol HR Dashboard can be used to:</p>


			<h3>Post a Job</h3>
			<ul>
				<li>Create and post the job vacancy you wish to fulfill to Sol’s Career Web Page</li>
			</ul>

			<h3>Request Ads</h3>
			<ul>
				<li>Request ads for Traditional and Social Media placements for job postings.</li>
			</ul>

			<h3>Jobs Applications</h3>
			<ul>
				<li>View and monitor applications of interested candidates.</li>
			</ul>
		</div>

		</br>
		</br>

		<div>

			<h2>How to Post a Job</h2>
			<hr>
			<ul>
				<li>To post a job on the Sol Careers Page, click the “Create” button and follow the instructions.</li>
				<li>To download a list of active (previously uploaded) job postings to your computer, click on the “Export” button and choose the file format you prefer.</li>
				<li>In the populated list, located/shown on the main screen, you can click one of several icons to:
				</br>
					<ul style="list-style: none;">
						<li style="margin-top:1%;"><i class="fa fa-envelope"></i> View the email of the person requesting that the job be posted.</li>
						<li style="margin-top:1%;"><i class="fa fa-eye"></i> View the list of all active job postings</li>
						<li style="margin-top:1%;"><i class="fa fa-edit"></i> Edit the job posting</li>
						<li style="margin-top:1%;"><i class="fa fa-trash"></i> Delete the job posting</li>


					</ul>

				</li>

			</ul>

			<p><strong>Note for application deadline extensions</strong></p>
			<p> In the event that persons want to extend the submission deadline for a job vacancy, please feel free to include this information in the comments section of the "Request Ads" tab.</p>

		</div>

		</br>


		<div>

			<h2>How to Request Ads</h2>
			<hr>
			<ul>
				<li>To request a Recruitment Ad or a Job Posting on Digital and Social Media or Traditional Media, click the “Create” button and follow the instructions.</li>
				<li>To download a list of active advertisements to your computer, click on the “Export” button and choose the file format you prefer.</li>
				<li> In the populated list on the main screen, you can click one of several icons to:
				</br>
					<ul style="list-style: none;">
						<li style="margin-top:1%;"><i class="fa fa-eye"></i> View the list of all active advertisements</li>
						<li style="margin-top:1%;"><i class="fa fa-edit"></i> Edit the advertisement</li>
						<li style="margin-top:1%;"><i class="fa fa-trash"></i> Delete the advertisement</li>

					</ul>

				</li>

			</ul>

		</div>
		</br>

		<div>

			<h2>How to access Job Applications</h2>
			<hr>
			<ul>
				<li>To download a list of active job applications that are being processed, click on the “Export” button and choose the file format you prefer.</li>
				<li> In the populated list on the main screen, you can click one of several icons to:
				</br>
					<ul style="list-style: none;">
						<li style="margin-top:1%;"><i class="fa fa-eye"></i> View the list of all active job applications that are being processed</li>
						<li style="margin-top:1%;"><i class="fa fa-edit"></i> Edit the job application that is being processed</li>
						<li style="margin-top:1%;"><i class="fa fa-trash"></i> Delete the job application that is being processed</li>

					</ul>

				</li>

			</ul>

		</div>

	</div>

    </div>
</div>
@endsection
