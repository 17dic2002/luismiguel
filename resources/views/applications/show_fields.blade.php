<div class="container">
    <h2>Personal Information</h2>
    <hr>
    <dl class="row">

        <dt class="col-sm-3 text-right">Email:</dt>
        <dd class="col-sm-9"><p>{!! $applications->email !!}</p></dd>

        <dt class="col-sm-3 text-right">Home Phone:</dt>
        <dd class="col-sm-9"><p>{!! $applications->home_phone !!}</p></dd>

        <dt class="col-sm-3 text-right">Mobile Phone:</dt>
        <dd class="col-sm-9"><p>{!! $applications->home_phone !!}</p></dd>

        <!-- <dt class="col-sm-3 text-right">Gender:</dt>
        <dd class="col-sm-9"><p>{!! $applications->gender !!}</p></dd>

        <dt class="col-sm-3 text-right">Date of Birth:</dt>
        <dd class="col-sm-9"><p>{!! $applications->date_of_birth !!}</p></dd>-->

        <dt class="col-sm-3 text-right">Address:</dt>
        <dd class="col-sm-9"><p>{!! $applications->address !!}</p></dd>

        <dt class="col-sm-3 text-right">Driver License:</dt>
        <dd class="col-sm-9"><p>{!! $applications->driver_license !!}</p></dd>
    </dl>
    <br>
    <hr>
    <h2>Job Information</h2>
    <hr>
    <dl class="row">
        <dt class="col-sm-3 text-right">Job Title:</dt>
        <dd class="col-sm-9"><p>{!! $applications->application_title !!}</p></dd>

        <dt class="col-sm-3 text-right">Department:</dt>
        @if($applications->category != 'Other')
            <dd class="col-sm-9"><p>{!! $applications->category !!}</p></dd>
        @else
            <dd class="col-sm-9"><p>{!! $applications->other_category !!}</p></dd>
        @endif
        <dt class="col-sm-3 text-right">Type:</dt>
        <dd class="col-sm-9"><p>{!! $applications->type !!}</p></dd>

        <dt class="col-sm-3 text-right">Current Salary:</dt>
        <dd class="col-sm-9"><p>{!! $applications->current_salary !!} {!! $applications->currency !!}</p></dd>

        <dt class="col-sm-3 text-right">Expected Salary:</dt>
        <dd class="col-sm-9"><p>{!! $applications->expected_salary !!} {!! $applications->currency_expected !!} </p>
        </dd>
    </dl>

    </br>
    <h2>Education</h2>
    @foreach($applications->educations as $education)
        <hr>
        <dl class="row">
            <dt class="col-sm-3 text-right">Institute:</dt>
            <dd class="col-sm-9"><p>{!! $education->institute !!}</p></dd>

            <dt class="col-sm-3 text-right">Study Area:</dt>
            <dd class="col-sm-9"><p>{!! $education->study_area !!}</p></dd>

            <dt class="col-sm-3 text-right">Certificate/Degree:</dt>
            <dd class="col-sm-9"><p>{!! $education->certificate !!}</p></dd>

            <dt class="col-sm-3 text-right">Date From:</dt>
            <dd class="col-sm-9"><p>{!! $education->date_from !!}</p></dd>

            <dt class="col-sm-3 text-right">Date To:</dt>
            <dd class="col-sm-9"><p>{!! $education->date_to !!}</p></dd>
        </dl>
        @endforeach
        </br>
        <h2>Work Experience</h2>
        <dl class="row">
            <dt class="col-sm-3 text-right">Work Experience:</dt>
            <dd class="col-sm-9"><p>{!! $applications->years_of_experience !!}</p></dd>
        </dl>
        @foreach($applications->workExperiences as $exp)
            <hr>
            <dl class="row">

                <dt class="col-sm-3 text-right">Employer Name:</dt>
                <dd class="col-sm-9"><p>{!! $exp->employer_name !!}</p></dd>

                <dt class="col-sm-3 text-right">Position:</dt>
                <dd class="col-sm-9"><p>{!! $exp->position !!}</p></dd>

                <dt class="col-sm-3 text-right">Responsabilities:</dt>
                <dd class="col-sm-9"><p>{!! $exp->responsabilities !!}</p></dd>

                <dt class="col-sm-3 text-right">Manager Name:</dt>
                <dd class="col-sm-9"><p>{!! $exp->supervisors_name !!}</p></dd>

                <dt class="col-sm-3 text-right">Position Start Date:</dt>
                <dd class="col-sm-9"><p>{!! $exp->position_start_date !!}</p></dd>

                <dt class="col-sm-3 text-right">Position End Date:</dt>
                <dd class="col-sm-9"><p>{!! $exp->position_end_date !!}</p></dd>

                <dt class="col-sm-3 text-right">Reason to Leave:</dt>
                <dd class="col-sm-9"><p>{!! $exp->reason_leaving !!}</p></dd>

                <dt class="col-sm-3 text-right">Employer Phone:</dt>
                <dd class="col-sm-9"><p>{!! $exp->phone !!}</p></dd>

                <dt class="col-sm-3 text-right">Employer Address:</dt>
                <dd class="col-sm-9"><p>{!! $exp->addresss !!}</p></dd>
            </dl>
            @endforeach
            </br>
            <h2>References</h2>
            @foreach($applications->references as $ref)
                <hr>
                <dl class="row">

                    <dt class="col-sm-3 text-right">Name of referee:</dt>
                    <dd class="col-sm-9"><p>{!! $ref->name_referee !!}</p></dd>

                    <dt class="col-sm-3 text-right">Job Title:</dt>
                    <dd class="col-sm-9"><p>{!! $ref->job_title !!}</p></dd>

                    <dt class="col-sm-3 text-right">Organization:</dt>
                    <dd class="col-sm-9"><p>{!! $ref->organization !!}</p></dd>

                    <dt class="col-sm-3 text-right">Relation to Referee:</dt>
                    <dd class="col-sm-9"><p>{!! $ref->relation !!}</p></dd>

                    <dt class="col-sm-3 text-right">Phone Number:</dt>
                    <dd class="col-sm-9"><p>{!! $ref->phone_number !!}</p></dd>

                    <dt class="col-sm-3 text-right">Address:</dt>
                    <dd class="col-sm-9"><p>{!! $ref->address !!}</p></dd>

                </dl>
                @endforeach
                </br>
                <h2>Languages</h2>
                @foreach($applications->languages as $lang)
                    <hr>
                    <dl class="row">

                        <dt class="col-sm-3 text-right">Language:</dt>
                        <dd class="col-sm-9"><p>{!! $lang->language !!}</p></dd>

                        <dt class="col-sm-3 text-right">Understanding Proficiency:</dt>
                        <dd class="col-sm-9"><p>{!! $lang->understanding !!}</p></dd>

                        <dt class="col-sm-3 text-right">Writing Proficiency:</dt>
                        <dd class="col-sm-9"><p>{!! $lang->writing !!}</p></dd>

                        <dt class="col-sm-3 text-right">Reading Proficiency:</dt>
                        <dd class="col-sm-9"><p>{!! $lang->reading !!}</p></dd>

                        <dt class="col-sm-3 text-right">Where did you learn it?:</dt>
                        <dd class="col-sm-9"><p>{!! $lang->where_learn !!}</p></dd>
                    </dl>
                    @endforeach

                    </br>
                    <br>
                    <hr>
                    <h2>Skills</h2>
                    <hr>
                    <dl class="row">
                        <dt class="col-sm-3 text-right">List all Skills:</dt>
                        <dd class="col-sm-9"><p>{!! $applications->list_skills !!}</p></dd>

                        <dt class="col-sm-3 text-right">Professional Qualifications:</dt>
                        <dd class="col-sm-9"><p>{!! $applications->professional_qualifications_skills !!}</p></dd>

                        <dt class="col-sm-3 text-right">Interest/Activities/ Hobbies:</dt>
                        <dd class="col-sm-9"><p>{!! $applications->hobbies !!} </p></dd>

                        <dt class="col-sm-3 text-right">Other Relevant Educational / Training Courses:</dt>
                        <dd class="col-sm-9"><p>{!! $applications->other_relevant_skills !!}</p></dd>
                    </dl>
                    <br>
                    <hr>
                    <dl class="row">
                        <dt class="col-sm-3 text-right">How did you hear about this vacancy?</dt>
                        <dd class="col-sm-9"><p>{!! $applications->how_hear !!}</p></dd>
                    </dl>

                    <br>
                    <h2>Resume</h2>
                    <hr>

                    <div class="col-md-6">
                        <embed src="{{ asset('uploads/applications/'.$applications->resume) }}"
                               style="width:600px; height:800px;"
                               frameborder="0">
                    </div>
</div>

