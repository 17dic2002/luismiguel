@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Applications
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('applications.show_fields')
                    <a href="{!! route('applications.index') !!}" class="btn btn-default">Back</a>
                    <a href="{!! route('saveForLater', $applications->id) !!}" class="btn btn-primary">Save For Later</a>
                    <a href="{!! route('shortList', $applications->id) !!}" class="btn btn-success">Add to Short List</a>
                    <a href="{!! route('reject', $applications->id) !!}" class="btn btn-danger">Reject</a>

                </div>
            </div>
        </div>
    </div>
@endsection
