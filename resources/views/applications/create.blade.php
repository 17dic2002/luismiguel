@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Applications
        </h1>
        </br>
        <p>Below is a list of job vacancies that are currently being applied for by interested candidates.</p>
        </br>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'applications.store', 'files' => true]) !!}

                        @include('applications.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
