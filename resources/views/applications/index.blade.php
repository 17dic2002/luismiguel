@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Applications</h1>
        </br></br>
        <p>Below is a list of applications by interested candidates for job vacancies that are currently still active.</p>
        </br>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('applications.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

