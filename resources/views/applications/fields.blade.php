<!-- Application Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_title', 'Application Title:') !!}
    {!! Form::text('application_title', null, ['class' => 'form-control']) !!}
</div>

<!-- First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('first_name', 'First Name:') !!}
    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Middle Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('middle_name', 'Middle Name:') !!}
    {!! Form::text('middle_name', null, ['class' => 'form-control', 'required'=> 'required']) !!}
</div>

<!-- Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('last_name', 'Last Name:') !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Home Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('home_phone', 'Home Phone:') !!}
    {!! Form::text('home_phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Mobile Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mobile_number', 'Mobile Number:') !!}
    {!! Form::text('mobile_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field 
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender:') !!}
    {!! Form::select('gender', ['Female' => 'Female', 'Male' => 'Male', 'Other' => 'Other'], null, ['class' => 'form-control']) !!}
</div>-->

<!-- Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category', ['Choose...' => 'Choose...', 'Accounting Finance' => 'Accounting Finance', 'Marketing' => 'Marketing', 'Operations' => 'Operations', 'HR & Administration' => 'HR & Administration', 'Corporate' => 'Corporate', 'Supply' => 'Supply', 'Aviation' => 'Aviation', 'Other' => 'Other'], null, ['class' => 'form-control']) !!}
</div>

<!-- Other Category Field -->
<div class="form-group col-sm-6">
    {!! Form::label('other_category', 'Other Category:') !!}
    {!! Form::select('other_category', ['Choose...' => 'Choose...', 'Accounting Finance' => 'Accounting Finance', 'Marketing' => 'Marketing', 'Operations' => 'Operations', 'HR & Administration' => 'HR & Administration', 'Corporate' => 'Corporate', 'Supply' => 'Supply', 'Aviation' => 'Aviation', 'Other' => 'Other'], null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::select('type', ['Choose...' => 'Choose...', 'Full Time' => 'Full Time', 'Part Time' => 'Part Time', ' Contractors' => ' Contractors'], null, ['class' => 'form-control']) !!}
</div>

<!-- Years Of Experience Field -->
<div class="form-group col-sm-6">
    {!! Form::label('years_of_experience', 'Years Of Experience:') !!}
    {!! Form::select('years_of_experience', ['Choose...' => 'Choose...', 'No Work Experience' => 'No Work Experience', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '25' => '25', 'More Than 25' => 'More Than 25'], null, ['class' => 'form-control']) !!}
</div>

<!-- Date Of Birth Field 
<div class="form-group col-sm-6">
    {!! Form::label('date_of_birth', 'Date Of Birth:') !!}
    {!! Form::date('date_of_birth', null, ['class' => 'form-control','id'=>'date_of_birth']) !!}
</div>-->

@section('scripts')
    <script type="text/javascript">
        $('#date_of_birth').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Currency Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency', 'Currency:') !!}
    {!! Form::select('currency', ['Choose...' => 'Choose...', 'BSD' => 'BSD', 'DOP' => 'DOP', 'BBD' => 'BBD', 'BMD' => 'BMD', 'JMD' => 'JMD', 'XCD' => 'XCD', 'USD' => 'USD', 'Euro' => 'Euro', 'BZD' => 'BZD', 'NAF' => 'NAF'], null, ['class' => 'form-control']) !!}
</div>

<!-- Period Cs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('period_cs', 'Period Cs:') !!}
    {!! Form::select('period_cs', ['Choose...' => 'Choose...', 'Annualy' => 'Annualy', 'Monthly' => 'Monthly', 'Bi-Weekly' => 'Bi-Weekly', 'Weekly' => 'Weekly', 'Twice a Month' => 'Twice a Month'], null, ['class' => 'form-control']) !!}
</div>

<!-- Currency Expected Field -->
<div class="form-group col-sm-6">
    {!! Form::label('currency_expected', 'Currency Expected:') !!}
    {!! Form::select('currency_expected', ['Choose...' => 'Choose...', 'BSD' => 'BSD', 'DOP' => 'DOP', 'BBD' => 'BBD', 'BMD' => 'BMD', 'JMD' => 'JMD', 'XCD' => 'XCD', 'USD' => 'USD', 'Euro' => 'Euro', 'BZD' => 'BZD', 'NAF' => 'NAF'], null, ['class' => 'form-control']) !!}
</div>

<!-- Period Es Field -->
<div class="form-group col-sm-6">
    {!! Form::label('period_es', 'Period Es:') !!}
    {!! Form::select('period_es', ['Choose...' => 'Choose...', 'Annualy' => 'Annualy', 'Monthly' => 'Monthly', 'Bi-Weekly' => 'Bi-Weekly', 'Weekly' => 'Weekly', 'Twice a Month' => 'Twice a Month'], null, ['class' => 'form-control']) !!}
</div>

<!-- Driver License Field -->
<div class="form-group col-sm-12">
    {!! Form::label('driver_license', 'Driver License:') !!}
    <label class="radio-inline">
        {!! Form::radio('driver_license', "1", null) !!} Yes
    </label>

    <label class="radio-inline">
        {!! Form::radio('driver_license', "0", null) !!} No
    </label>

</div>

<!-- Resume Field -->
<div class="form-group col-sm-6">
    {!! Form::label('resume', 'Resume:') !!}
    {!! Form::file('resume') !!}
</div>
<div class="clearfix"></div>

<!-- List Skills Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('list_skills', 'List Skills:') !!}
    {!! Form::textarea('list_skills', null, ['class' => 'form-control','maxlength'=>'2000']) !!}
</div>

<!-- Professional Qualifications Skills Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('professional_qualifications_skills', 'Professional Qualifications Skills:') !!}
    {!! Form::textarea('professional_qualifications_skills', null, ['class' => 'form-control','maxlength'=>'2000']) !!}
</div>

<!-- Other Relevant Skills Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('other_relevant_skills', 'Other Relevant Skills:') !!}
    {!! Form::textarea('other_relevant_skills', null, ['class' => 'form-control','maxlength'=>'2000']) !!}
</div>

<!-- Hobbies Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('hobbies', 'Hobbies:') !!}
    {!! Form::textarea('hobbies', null, ['class' => 'form-control','maxlength'=>'2000']) !!}
</div>

<!-- How Hear Field -->
<div class="form-group col-sm-6">
    {!! Form::label('how_hear', 'How Hear:') !!}
    {!! Form::select('how_hear', ['Choose...' => 'Choose...', 'Career Center' => 'Career Center', 'E-Blast' => 'E-Blast', 'Employee of the Company' => 'Employee of the Company', 'Facebook' => 'Facebook', 'Instagram' => 'Instagram', 'Job Board' => 'Job Board', 'LinkedIn' => 'LinkedIn', 'Newspaper' => 'Newspaper', 'Word-of-Mouth' => 'Word-of-Mouth', 'Other' => 'Other'], null, ['class' => 'form-control']) !!}
</div>

<!-- Job Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job_id', 'Job Id:') !!}
    {!! Form::number('job_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('applications.index') !!}" class="btn btn-default">Cancel</a>
</div>
