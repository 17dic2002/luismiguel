<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $workExperiences->id !!}</p>
</div>

<!-- Employer Name Field -->
<div class="form-group">
    {!! Form::label('employer_name', 'Employer Name:') !!}
    <p>{!! $workExperiences->employer_name !!}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    <p>{!! $workExperiences->position !!}</p>
</div>

<!-- Responsabilities Field -->
<div class="form-group">
    {!! Form::label('responsabilities', 'Responsabilities:') !!}
    <p>{!! $workExperiences->responsabilities !!}</p>
</div>

<!-- Supervisors Name Field -->
<div class="form-group">
    {!! Form::label('supervisors_name', 'Supervisors Name:') !!}
    <p>{!! $workExperiences->supervisors_name !!}</p>
</div>

<!-- Position Start Date Field -->
<div class="form-group">
    {!! Form::label('position_start_date', 'Position Start Date:') !!}
    <p>{!! $workExperiences->position_start_date !!}</p>
</div>

<!-- Position End Date Field -->
<div class="form-group">
    {!! Form::label('position_end_date', 'Position End Date:') !!}
    <p>{!! $workExperiences->position_end_date !!}</p>
</div>

<!-- Reason Leaving Field -->
<div class="form-group">
    {!! Form::label('reason_leaving', 'Reason Leaving:') !!}
    <p>{!! $workExperiences->reason_leaving !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $workExperiences->phone !!}</p>
</div>

<!-- Addresss Field -->
<div class="form-group">
    {!! Form::label('addresss', 'Addresss:') !!}
    <p>{!! $workExperiences->addresss !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $workExperiences->application_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $workExperiences->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $workExperiences->updated_at !!}</p>
</div>

