<!-- Employer Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employer_name', 'Employer Name:') !!}
    {!! Form::text('employer_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Position Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position', 'Position:') !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
</div>

<!-- Responsabilities Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('responsabilities', 'Responsabilities:') !!}
    {!! Form::textarea('responsabilities', null, ['class' => 'form-control','maxlength'=>'2000']) !!}
</div>

<!-- Supervisors Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supervisors_name', 'Supervisors Name:') !!}
    {!! Form::text('supervisors_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Position Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position_start_date', 'Position Start Date:') !!}
    {!! Form::date('position_start_date', null, ['class' => 'form-control','id'=>'position_start_date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#position_start_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Position End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('position_end_date', 'Position End Date:') !!}
    {!! Form::date('position_end_date', null, ['class' => 'form-control','id'=>'position_end_date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#position_end_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Reason Leaving Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('reason_leaving', 'Reason Leaving:') !!}
    {!! Form::textarea('reason_leaving', null, ['class' => 'form-control','maxlength'=>'2000']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Addresss Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addresss', 'Addresss:') !!}
    {!! Form::text('addresss', null, ['class' => 'form-control']) !!}
</div>

<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::number('application_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('workExperiences.index') !!}" class="btn btn-default">Cancel</a>
</div>
