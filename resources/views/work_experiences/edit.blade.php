@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Work Experiences
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($workExperiences, ['route' => ['workExperiences.update', $workExperiences->id], 'method' => 'patch']) !!}

                        @include('work_experiences.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection