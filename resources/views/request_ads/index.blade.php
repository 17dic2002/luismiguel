@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Request Ads</h1>
        </br></br>
        <p class="pull-left">In this area, you can request that ads be placed in offline or online media outlets through our partner communication agencies.</p></br></br>
        <p>*These are required fields</p>
        </br></br>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('request_ads.table')
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

