@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Request Ads
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="margin:0 !important;">
                   {!! Form::model($requestAds, ['route' => ['requestAds.update', $requestAds->id], 'method' => 'patch']) !!}

                    @include('request_ads.requestForm')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!--    <link rel="stylesheet" type="text/css" href="{{asset('css/soladmin.css')}}">-->
@endsection

