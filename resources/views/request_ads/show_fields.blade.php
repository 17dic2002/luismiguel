<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $requestAds->id !!}</p>
</div>

<!-- Requester Name Field -->
<div class="form-group">
    {!! Form::label('requester_name', 'Requester Name:') !!}
    <p>{!! $requestAds->requester_name !!}</p>
</div>

<!-- Requester Email Field -->
<div class="form-group">
    {!! Form::label('requester_email', 'Requester Email:') !!}
    <p>{!! $requestAds->requester_email !!}</p>
</div>

<!-- Print Add Field -->
<div class="form-group">
    {!! Form::label('print_add', 'Print Add:') !!}
    <p>{!! $requestAds->print_add !!}</p>
</div>

<!-- Name Publication Field -->
<div class="form-group">
    {!! Form::label('name_publication', 'Name Publication:') !!}
    <p>{!! $requestAds->name_publication !!}</p>
</div>

<!-- Ad Size Field -->
<div class="form-group">
    {!! Form::label('ad_size', 'Ad Size:') !!}
    <p>{!! $requestAds->ad_size !!}</p>
</div>

<!-- Date Publication Field -->
<div class="form-group">
    {!! Form::label('date_publication', 'Date Publication:') !!}
    <p>{!! $requestAds->date_publication !!}</p>
</div>

<!-- Template Print Field -->
<div class="form-group">
    {!! Form::label('template_print', 'Template Print:') !!}
    <p>{!! $requestAds->template_print !!}</p>
</div>

<!-- Comments Template Field -->
<div class="form-group">
    {!! Form::label('comments_template', 'Comments Template:') !!}
    <p>{!! $requestAds->comments_template !!}</p>
</div>

<!-- Color Type Field -->
<div class="form-group">
    {!! Form::label('color_type', 'Color Type:') !!}
    <p>{!! $requestAds->color_type !!}</p>
</div>

<!-- Book Media Field -->
<div class="form-group">
    {!! Form::label('book_media', 'Book Media:') !!}
    <p>{!! $requestAds->book_media !!}</p>
</div>

<!-- Due Date Field -->
<div class="form-group">
    {!! Form::label('due_date', 'Due Date:') !!}
    <p>{!! $requestAds->due_date !!}</p>
</div>

<!-- Blue Print Field -->
<div class="form-group">
    {!! Form::label('blue_print', 'Blue Print:') !!}
    <p>{!! $requestAds->blue_print !!}</p>
</div>

<!-- Budget Field -->
<div class="form-group">
    {!! Form::label('budget', 'Budget:') !!}
    <p>{!! $requestAds->budget !!}</p>
</div>

<!-- Comments Bp Field -->
<div class="form-group">
    {!! Form::label('comments_bp', 'Comments Bp:') !!}
    <p>{!! $requestAds->comments_bp !!}</p>
</div>

<!-- Social Media Field -->
<div class="form-group">
    {!! Form::label('social_media', 'Social Media:') !!}
    <p>{!! $requestAds->social_media !!}</p>
</div>

<!-- Platforms Field -->
<div class="form-group">
    {!! Form::label('platforms', 'Platforms:') !!}
    <p>{!! $requestAds->platforms !!}</p>
</div>

<!-- Other Platform Field -->
<div class="form-group">
    {!! Form::label('other_platform', 'Other Platform:') !!}
    <p>{!! $requestAds->other_platform !!}</p>
</div>

<!-- Template Social Field -->
<div class="form-group">
    {!! Form::label('template_social', 'Template Social:') !!}
    <p>{!! $requestAds->template_social !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $requestAds->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $requestAds->updated_at !!}</p>
</div>

<!-- Job Id Field -->
<div class="form-group">
    {!! Form::label('job_id', 'Job Id:') !!}
    <p>{!! $requestAds->job_id !!}</p>
</div>

