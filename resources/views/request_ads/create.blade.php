@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Request Ads
        </h1>
        </br>
        <p>In this area you can request placement of job postings in offline and online media outlets through our partner communication agencies.</p>
        <p>*These are required fields</p>
        </br>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row" style="margin:0 !important;">
                    {!! Form::open(['route' => 'requestAds.store']) !!}

                    @include('request_ads.requestForm')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

