<div class="form-group col-sm-12">
    <label for="departamento_id">Select the Job:</label>
    <p>This menu features a list of jobs that have already been posted. To request an ad for a new job not seen here,
        please go to the ‘Post a Job’ section and create the post there first.</p>
    {!! Form::select('job_id', array_pluck($jobs, 'title', 'id'),null, ['class' => 'form-control']) !!}
</div>

<p>Please complete the information in this form to schedule a job posting, request a
    recruitment ad or media placement.</p>

<!-- Google Form replicated-->
<div class="form-group col-md-6">
    {!! Form::label('requester_name', '*Requester Complete Name:') !!}
    {!! Form::text('requester_name', null, ['class' => 'form-control','required'=>'required']) !!}


</div>
<div class="form-group col-md-6">
    {!! Form::label('requester_email', '*Requester Email Address:') !!}
    {!! Form::text('requester_email', null, ['class' => 'form-control','required'=>'required','placeholder'=>'example@example.com']) !!}


</div>


<h2 style="text-decoration: underline;">Digital and Social Media</h2>


</div>

<div class="form-group col-md-12">
    <div class="form-check form-check-inline">
        <label class="form-check-label" for="inlineRadio1">Digital and Social Media
            Advertising needed? </label>&nbsp&nbsp
        <input class="form-check-input" onchange="checkIfDigiAdY()" type="radio"
               name="social_media" id="digiadY" value="Yes">
        <label class="form-check-label" for="digiadY">Yes</label>
        <input class="form-check-input" onchange="checkIfDigiAdN()" type="radio"
               name="social_media" id="digiadN" value="No">
        <label class="form-check-label" for="digiadN">No</label>
    </div>

</div>

<div class="form-group col-md-12">
    <p><strong>Note</strong> that Paradigm Associates will also post this job organically by default in the platforms
        you indicate below. Please ensure
        that you allow the team 2 to 3 working days to fully implement this media
        placement.</p>
</div>

<div class="form-group col-md-12">
    <p>Please select your preferred platforms</p>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="facebook" id="digiadF"
               name="platforms[]" disabled>
        <label class="form-check-label" for="digiadF">Facebook</label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="instagram" id="digiadI"
               name="platforms[]" disabled>
        <label class="form-check-label" for="digiadI">Instagram</label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="linkedin" id="digiadL"
               name="platforms[]" disabled>
        <label class="form-check-label" for="digiadL">LinkedIn</label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="indeed" id="digiadIndeed"
               name="platforms[]" disabled>
        <label class="form-check-label" for="digiadIndeed">Indeed</label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox"
               value="other" id="digiadOther" name="platforms[]" disabled>
        <label class="form-check-label" for="digiadOther">Other</label>
    </div>

</div>

<div class="form-group col-md-4">
    <label for="inputdigiadOther"></label>
    <input type="text" class="form-control" id="inputdigiadOther" placeholder="Other"
           name="other_platform" disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputBudgetSocial">What is the US$-Budget?</label>
    <input type="text" class="form-control" id="inputBudgetSocial" name="budget_social"
           disabled>

</div>
<div class="form-group col-md-12">
    <p>The below are pre-approved photo options that can be used to create the
        Digital and Social Media post. Please select which photo option best represent the job.</p>
    </br>

</div>

<div class="form-group col-md-12" id="inputAdPhoto">
    <div class="cc-selector">
        <input id="1" type="radio" name="template_social" value="1"/>
        <label class="drinkcard-cc ph1" for="1"></label>
        <input id="2" type="radio" name="template_social" value="2"/>
        <label class="drinkcard-cc ph2" for="2"></label>
        <input id="3" type="radio" name="template_social" value="3"/>
        <label class="drinkcard-cc ph3" for="3"></label>
        <input id="4" type="radio" name="template_social" value="4"/>
        <label class="drinkcard-cc ph4" for="4"></label>
    </div>
    <div class="cc-selector">
        <input id="5" type="radio" name="template_social" value="5"/>
        <label class="drinkcard-cc ph5" for="5"></label>
        <input id="6" type="radio" name="template_social" value="6"/>
        <label class="drinkcard-cc ph6" for="6"></label>
        <input id="7" type="radio" name="template_social" value="7"/>
        <label class="drinkcard-cc ph7" for="7"></label>
        <input id="8" type="radio" name="template_social" value="8"/>
        <label class="drinkcard-cc ph8" for="8"></label>
    </div>
    <div class="cc-selector">
        <input id="9" type="radio" name="template_social" value="9"/>
        <label class="drinkcard-cc ph9" for="9"></label>
        <input id="10" type="radio" name="template_social" value="10"/>
        <label class="drinkcard-cc ph10" for="10"></label>
        <input id="11" type="radio" name="template_social" value="11"/>
        <label class="drinkcard-cc ph11" for="11"></label>
        <input id="12" type="radio" name="template_social" value="12"/>
        <label class="drinkcard-cc ph12" for="12"></label>
    </div>
</div>




<div class="form-group col-md-12">

    <h2 style="text-decoration: underline;">Print Ad (Artwork) and/or Traditional Media Placement*.</h2>
    </br>
    <p>This first section is strictly for the placement of print (traditional media) job advertisements. Further below
        you will be
        able to post an advertisement in digital media if you so desire</p>

</div>
<div class="form-group col-md-12">
    <div class="form-check form-check-inline">
        <label class="form-check-label" for="inlineRadio1">Print Ad Needed? </label>&nbsp&nbsp
        <input class="form-check-input" onchange="checkIfPrintAdY()" type="radio"
               name="print_add" id="printadY" value="Yes">
        <label class="form-check-label" for="printadY">Yes</label>
        <input class="form-check-input" onchange="checkIfPrintAdN()" type="radio"
               name="print_add" id="printadN" value="No">
        <label class="form-check-label" for="printadN">No</label>
    </div>
</div>

<div class="form-group col-md-12">
    <p>Please ensure that you allow the team 2 to 4 working days (upon receipt of all
        details) to deliver this request.</p>
</div>

<hr>

{{-- ---------------- Media Placement #1 ------------------ --}}

<div class="form-group col-md-12 media-placement">
    <h2>Media Placement #1</h2>
</div>
<div class="form-group col-md-4">
    <label for="inputAdName">*Name of Publication</label>
    <input type="text" class="form-control" id="inputAdName" name="name_publication"
           disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputAdSize">*Specific Ad Size</label>
    <input type="text" class="form-control" id="inputAdSize" name="ad_size"
           disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputAdDate">Publication Period</label>
    <input type="text" class="form-control" id="inputAdDate" name="date_publication"
           disabled>
</div>
<div class="form-group col-md-6">
    <label for="template_print">Template</label>
    {!! Form::select('template_print', ['Template 1 – Cover Image – BOLD & Values Icons', 'Template 2 – Cover Image', 'Template 3 – BOLD & Values Icons','Template 4 – No Cover Image and No BOLD Icons'], null, ['class' => 'form-control', 'disabled','id'=>'inputAdTemplate']) !!}
</div>
<div class="form-group col-md-6">
    <label for="orientation">Orientation</label>
    {!! Form::select('orientation', ['Horizontal', 'Vertical'], null, ['class' => 'form-control', 'disabled', 'id'=>'orientation']) !!}
</div>
<div class="form-group col-md-6">
    <label for="color_type">Color Type</label>
    {!! Form::select('color_type', ['Black & White'=>'Black & White','Full Color'=>'Full Color'], null, ['class' => 'form-control', 'disabled','id'=>'color_type', 'disabled']) !!}

</div>
<div class="form-group col-md-6">
    <label for="submission_deadline">Submission Deadline</label>
    <input type="text" class="form-control" id="submission_deadline" name="submission_deadline" autocomplete="off"
           disabled>
</div>
<div class="form-group col-md-12">
    <label for="inputAdOtherComm">Comments</label>
    <textarea rows=3 class="form-control" id="inputAdOtherComm" name="comments_template" maxlength="2000"

              disabled></textarea>
</div>
{{-- ---------------- End Media Placement #1 ------------------ --}}


{{-- ---------------- Media Placement #2 ------------------ --}}

<div class="form-group col-md-12 media-placement">
    <h2>Media Placement #2</h2>
</div>
<div class="form-group col-md-4">
    <label for="inputAdName">*Name of Publication</label>
    <input type="text" class="form-control" id="inputAdName2" name="name_publication_2"
           disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputAdSize">*Specific Ad Size</label>
    <input type="text" class="form-control" id="inputAdSize2" name="ad_size_2"
           disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputAdDate">Publication Period</label>
    <input type="text" class="form-control" id="inputAdDate2" name="date_publication_2"
           disabled>
</div>
<div class="form-group col-md-6">
    <label for="template_print">Template</label>
    {!! Form::select('template_print_2', ['Template 1 – Cover Image – BOLD & Values Icons', 'Template 2 – Cover Image', 'Template 3 – BOLD & Values Icons','Template 4 – No Cover Image and No BOLD Icons'], null, ['class' => 'form-control', 'disabled','id'=>'inputAdTemplate2']) !!}
</div>
<div class="form-group col-md-6">
    <label for="orientation">Orientation</label>
    {!! Form::select('orientation_2', ['Horizontal', 'Vertical'], null, ['class' => 'form-control', 'disabled', 'id'=>'orientation2']) !!}
</div>
<div class="form-group col-md-6">
    <label for="color_type">Color Type</label>
    {!! Form::select('color_type_2', ['Black & White'=>'Black & White','Full Color'=>'Full Color'], null, ['class' => 'form-control', 'disabled','id'=>'color_type2', 'disabled', ]) !!}

</div>
<div class="form-group col-md-6">
    <label for="submission_deadline2">Submission Deadline</label>
    <input type="text" class="form-control" id="submission_deadline2" name="submission_deadline2" autocomplete="off"
           disabled>
</div>
<div class="form-group col-md-12">
    <label for="inputAdOtherComm">Comments</label>
    <textarea rows=3 class="form-control" id="inputAdOtherComm2" name="comments_template_2"
    maxlength="2000"
    disabled></textarea>
</div>
{{-- ---------------- End Media Placement #2 ------------------ --}}

{{-- ---------------- Media Placement #3 ------------------ --}}

<div class="form-group col-md-12 media-placement">
    <h2>Media Placement #3</h2>
</div>
<div class="form-group col-md-4">
    <label for="inputAdName">*Name of Publication</label>
    <input type="text" class="form-control" id="inputAdName3" name="name_publication_3"
           disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputAdSize">*Specific Ad Size</label>
    <input type="text" class="form-control" id="inputAdSize3" name="ad_size_3"
           disabled>
</div>
<div class="form-group col-md-4">
    <label for="inputAdDate">Publication Period</label>
    <input type="text" class="form-control" id="inputAdDate3" name="date_publication_3"
           disabled>
</div>

<div class="form-group col-md-6">
    <label for="template_print">Template</label>
    {!! Form::select('template_print_3', ['Template 1 – Cover Image – BOLD & Values Icons', 'Template 2 – Cover Image', 'Template 3 – BOLD & Values Icons','Template 4 – No Cover Image and No BOLD Icons'], null, ['class' => 'form-control', 'disabled','id'=>'inputAdTemplate3']) !!}
</div>
<div class="form-group col-md-6">
    <label for="orientation">Orientation</label>
    {!! Form::select('orientation_3', ['Horizontal', 'Vertical'], null, ['class' => 'form-control', 'disabled', 'id'=>'orientation3']) !!}
</div>
<div class="form-group col-md-6">
    <label for="color_type">Color Type</label>
    {!! Form::select('color_type_3', ['Black & White'=>'Black & White','Full Color'=>'Full Color'], null, ['class' => 'form-control', 'disabled','id'=>'color_type', 'disabled', 'id'=>'color_type3']) !!}

</div>
<div class="form-group col-md-6">
    <label for="submission_deadline3">Submission Deadline</label>
    <input type="text" class="form-control" id="submission_deadline3" name="submission_deadline3" autocomplete="off"
           disabled>
</div>
<div class="form-group col-md-12">
    <label for="inputAdOtherComm">Comments</label>
    <textarea rows=3 class="form-control" id="inputAdOtherComm3" name="comments_template_3"
    maxlength="2000"
    disabled></textarea>
</div>
{{-- ---------------- End Media Placement #3 ------------------ --}}

<hr>
<div class="form-group col-md-12">
    <p>Please see the below set of templates prepared as a visual for what your ad could look like.
    </p>
    </br>
</div>

<div class="form-group col-md-12">

    <img style="width:80%;" src="{{ asset('images/request_ads/Numbered-Pics_16_templates.png') }}"/>

</div>
<hr>

<!------------------- Add traditional media placement section -------------------------------->
<div style="background-color: rgba(0,119,190,.3); width: 100%; border-radius: 15px;padding: 15px"
     class="form-group col-md-12">
    <div class="form-group col-md-12">
        <div class="form-check form-check-inline">
            <label class="form-check-label" for="inlineRadio2">Will you book your media
                placement locally?</label>&nbsp&nbsp
            <input class="form-check-input" type="radio" name="book_media"
                   id="printAdLocaly" value="Yes">
            <label class="form-check-label" for="printAdLocally">Yes</label>
            <input class="form-check-input" type="radio" name="book_media"
                   id="printAdLocaln" value="No">
            <label class="form-check-label" for="printAdLocaln">No</label>
        </div>
    </div>
    <div class="form-group col-md-5">
        <label for="inputAdDueDate">When is the due date and time to submit your Recruitment Ad to the media house?</label>
        <input type="text" class="form-control" id="inputAdDueDate" name="due_date" autocomplete="off"
               disabled>

    </div>
    <div class="form-group col-md-12">
        <div class="form-check form-check-inline">
            <label class="form-check-label" for="inlineRadio3">Do you require Blueprint
                Creative to coordinate media placement for your requested Ad
                (s)? </label>&nbsp&nbsp
            <input class="form-check-input" type="radio" name="blue_print"
                   id="printAdBPCY" value="Yes">
            <label class="form-check-label" for="printAdBPCY">Yes</label>
            <input class="form-check-input" type="radio" name="blue_print"
                   id="printAdBPC" value="No">
            <label class="form-check-label" for="printAdBPCN">No</label>
        </div>

    </div>
    <div class="form-group col-md-4">
        <label for="inputBudget">What is the US$-Budget?</label>
        <input type="text" class="form-control" id="inputBudget" name="budget"
               disabled>

    </div>
    <div class="form-group col-md-4">
        <label for="inputBudgetComm">Comments</label>
        <input type="text" class="form-control" id="inputBudgetComm" name="comments_bp"
               disabled>
    </div>
</div>
<!------------------- Add traditional media placement section End -------------------------------->
<div class="form-group col-md-12">

    
<div class="form-group col-md-12">
    <h4>Final Steps</h4>
    <p>Once you schedule your Job Post on the Sol Careers tab (including the job description upload),
        this will trigger an email request to the Brands team, Paradigm Associates and Blueprint Creative for support with
        artworks, traditional, digital and social media placements.</p>

</div>

<div class="form-group col-md-12">
    {!! Form::submit('Schedule Ad', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('requestAds.index') !!}" class="btn btn-default">Cancel</a>
</div>

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/jquery.datetimepicker.css') }}" />

{{--
    https://xdsoft.net/jqplugins/datetimepicker/
--}}
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>

    <script>
        $(function () {
            $('input[name="date_publication"]').daterangepicker({
                timePicker: true,
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY/MM/DD hh:mm A'
                }
            });

            $('#inputAdDueDate').datetimepicker();
            $('#submission_deadline').datetimepicker();
            $('#submission_deadline2').datetimepicker();
            $('#submission_deadline3').datetimepicker();

            $('input[name="date_publication_2"]').daterangepicker({
                timePicker: true,
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY/MM/DD hh:mm A'
                }            });
            $('input[name="date_publication_3"]').daterangepicker({
                timePicker: true,
                startDate: moment().startOf('hour'),
                endDate: moment().startOf('hour').add(32, 'hour'),
                timePicker24Hour: true,
                locale: {
                    format: 'YYYY/MM/DD hh:mm A'
                }            });
        });


        function checkIfPrintAdY() {
            if (document.getElementById('printadY').value == 'Yes') {
                document.getElementById('inputAdName').disabled = false;
                document.getElementById('inputAdSize').disabled = false;
                document.getElementById('inputAdDate').disabled = false;
                document.getElementById('inputAdTemplate').disabled = false;
                document.getElementById('orientation').disabled = false;
                document.getElementById('inputAdOtherComm').disabled = false;
                document.getElementById('submission_deadline').disabled = false;

                document.getElementById('inputAdName2').disabled = false;
                document.getElementById('inputAdSize2').disabled = false;
                document.getElementById('inputAdDate2').disabled = false;
                document.getElementById('inputAdTemplate2').disabled = false;
                document.getElementById('orientation2').disabled = false;
                document.getElementById('inputAdOtherComm2').disabled = false;
                document.getElementById('color_type2').disabled = false;
                document.getElementById('submission_deadline2').disabled = false;


                document.getElementById('inputAdName3').disabled = false;
                document.getElementById('inputAdSize3').disabled = false;
                document.getElementById('inputAdDate3').disabled = false;
                document.getElementById('inputAdTemplate3').disabled = false;
                document.getElementById('orientation3').disabled = false;
                document.getElementById('inputAdOtherComm3').disabled = false;
                document.getElementById('color_type3').disabled = false;
                document.getElementById('submission_deadline3').disabled = false;

                document.getElementById('color_type').disabled = false;
                document.getElementById('inputAdDueDate').disabled = false;
                document.getElementById('inputBudget').disabled = false;
                document.getElementById('inputBudgetComm').disabled = false;

            } else {
                document.getElementById('inputAdName').disabled = true;
            }
        }

        function checkIfPrintAdN() {
            if (document.getElementById('printadN').value == 'No') {
                document.getElementById('inputAdName').disabled = true;
                document.getElementById('inputAdSize').disabled = true;
                document.getElementById('inputAdDate').disabled = true;
                document.getElementById('inputAdTemplate').disabled = true;
                document.getElementById('orientation').disabled = true;
                document.getElementById('inputAdOtherComm').disabled = true;
                document.getElementById('submission_deadline').disabled = true;


                document.getElementById('inputAdName2').disabled = true;
                document.getElementById('inputAdSize2').disabled = true;
                document.getElementById('inputAdDate2').disabled = true;
                document.getElementById('inputAdTemplate2').disabled = true;
                document.getElementById('orientation2').disabled = true;
                document.getElementById('inputAdOtherComm2').disabled = true;
                document.getElementById('color_type2').disabled = true;
                document.getElementById('submission_deadline2').disabled = true;


                document.getElementById('inputAdName3').disabled = true;
                document.getElementById('inputAdSize3').disabled = true;
                document.getElementById('inputAdDate3').disabled = true;
                document.getElementById('inputAdTemplate3').disabled = true;
                document.getElementById('orientation3').disabled = true;
                document.getElementById('inputAdOtherComm3').disabled = true;
                document.getElementById('color_type3').disabled = true;
                document.getElementById('submission_deadline3').disabled = true;

                document.getElementById('inputAdDueDate').disabled = true;
                document.getElementById('inputBudget').disabled = true;
                document.getElementById('inputBudgetComm').disabled = true;
            } else {
                document.getElementById('inputAdName').disabled = false;
            }
        }

        function checkIfDigiAdY() {
            if (document.getElementById('digiadY').value == 'Yes') {
                document.getElementById('digiadF').disabled = false;
                document.getElementById('digiadI').disabled = false;
                document.getElementById('digiadL').disabled = false;
                document.getElementById('digiadIndeed').disabled = false;
                document.getElementById('inputAdPhoto').disabled = false;
                document.getElementById('digiadOther').disabled = false;
                document.getElementById('inputdigiadOther').disabled = false;
                document.getElementById('inputBudgetSocial').disabled = false;

            } else {
                document.getElementById('inputAdName').disabled = true;
            }
        }

        function checkIfDigiAdN() {
            if (document.getElementById('digiadN').value == 'No') {
                document.getElementById('digiadF').disabled = true;
                document.getElementById('digiadI').disabled = true;
                document.getElementById('digiadL').disabled = true;
                document.getElementById('digiadIndeed').disabled = true;
                document.getElementById('inputAdPhoto').disabled = true;
                document.getElementById('digiadOther').disabled = true;
                document.getElementById('inputBudgetSocial').disabled = true;
            } else {
                document.getElementById('inputAdName').disabled = false;
            }
        }

        function checkIfOtherTempY(val) {
            var element = document.getElementById('inputAdOtherComm');

            if (val == 'Others')
                element.removeAttribute('disabled');
            else
                element.setAttribute('disabled', 'disabled');
        }
    </script>
@endsection
