<!-- Requester Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requester_name', 'Requester Name:') !!}
    {!! Form::text('requester_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Requester Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('requester_email', 'Requester Email:') !!}
    {!! Form::text('requester_email', null, ['class' => 'form-control']) !!}
</div>

<div>
	<h2>Print ad (Artwork)  and or Tradicional media Placement *</h2></br>
</div>
<!-- Print Add Field -->
<div class="form-group col-sm-12">

    {!! Form::label('print_add', 'Print Add:') !!}
    <label class="radio-inline">
        {!! Form::radio('print_add', "Yes", null) !!} Yes
    </label>

    <label class="radio-inline">
        {!! Form::radio('print_add', "No", null) !!} No
    </label>

</div>
<div>
    <h2>Print ad (Artwork)  and or Tradicional media Placement *</h2></br>
</div>
<!-- Name Publication Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_publication', 'Name Publication:') !!}
    {!! Form::text('name_publication', null, ['class' => 'form-control']) !!}
</div>

<!-- Ad Size Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ad_size', 'Ad Size:') !!}
    {!! Form::text('ad_size', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Publication Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_publication', 'Date Publication:') !!}
    {!! Form::date('date_publication', null, ['class' => 'form-control','id'=>'date_publication']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_publication').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Template Print Field -->
<div class="form-group col-sm-6">
    {!! Form::label('template_print', 'Template Print:') !!}
    {!! Form::select('template_print', ['1-Junior Page (7" W X 10" H) - Color' => '1-Junior Page (7" W X 10" H) - Color', '2-Junior Page (7" W X 10" H) - Full Page - B/W' => '2-Junior Page (7" W X 10" H) - Full Page - B/W', ' 3-Full Page (10.5" W X 14" H) - Color' => ' 3-Full Page (10.5" W X 14" H) - Color', ' 4-Full Page (10.5" W X 14" H) - B/W' => ' 4-Full Page (10.5" W X 14" H) - B/W', ' 5. Half Page Horizontal (10.5" W X 7" H)' => ' 5. Half Page Horizontal (10.5" W X 7" H)', 'Other' => 'Other'], null, ['class' => 'form-control']) !!}
</div>

<!-- Comments Template Field -->
<div class="form-group col-sm-6">
    {!! Form::label('comments_template', 'Comments Template:') !!}
    {!! Form::text('comments_template', null, ['class' => 'form-control']) !!}
</div>

<hr>
<!-- Book Media Field -->
<div class="form-group col-sm-12">
    {!! Form::label('book_media', 'Book Media:') !!}
    <label class="radio-inline">
        {!! Form::radio('book_media', "Yes", null) !!} Yes
    </label>

    <label class="radio-inline">
        {!! Form::radio('book_media', "No", null) !!} No
    </label>

</div>

<!-- Due Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('due_date', 'Due Date:') !!}
    {!! Form::date('due_date', null, ['class' => 'form-control','id'=>'due_date']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#due_date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endsection

<!-- Blue Print Field -->
<div class="form-group col-sm-12">
    {!! Form::label('blue_print', 'Blue Print:') !!}
    <label class="radio-inline">
        {!! Form::radio('blue_print', "Yes", null) !!} Yes
    </label>

    <label class="radio-inline">
        {!! Form::radio('blue_print', "No", null) !!} No
    </label>

</div>

<!-- Budget Field -->
<div class="form-group col-sm-6">
    {!! Form::label('budget', 'Budget:') !!}
    {!! Form::text('budget', null, ['class' => 'form-control']) !!}
</div>

<!-- Comments Bp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('comments_bp', 'Comments Bp:') !!}
    {!! Form::text('comments_bp', null, ['class' => 'form-control']) !!}
</div>

<!-- Social Media Field -->
<div class="form-group col-sm-12">
    {!! Form::label('social_media', 'Social Media:') !!}
    <label class="radio-inline">
        {!! Form::radio('social_media', "Yes", null) !!} Yes
    </label>

    <label class="radio-inline">
        {!! Form::radio('social_media', "No", null) !!} No
    </label>

</div>

<!-- Platforms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('platforms', 'Platforms:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('platforms', 0) !!}
        {!! Form::checkbox('platforms', 'Facebook', null) !!}
    </label>
</div>


<!-- Other Platform Field -->
<div class="form-group col-sm-6">
    {!! Form::label('other_platform', 'Other Platform:') !!}
    {!! Form::text('other_platform', null, ['class' => 'form-control']) !!}
</div>

<!-- Template Social Field -->
<div class="form-group col-sm-6">
    {!! Form::label('template_social', 'Template Social:') !!}
    {!! Form::select('template_social', ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12'], null, ['class' => 'form-control']) !!}
</div>

<!-- Job Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job_id', 'Job Id:') !!}
    {!! Form::number('job_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('requestAds.index') !!}" class="btn btn-default">Cancel</a>
</div>
