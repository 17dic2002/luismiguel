<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $references->id !!}</p>
</div>

<!-- Name Referee Field -->
<div class="form-group">
    {!! Form::label('name_referee', 'Name Referee:') !!}
    <p>{!! $references->name_referee !!}</p>
</div>

<!-- Job Title Field -->
<div class="form-group">
    {!! Form::label('job_title', 'Job Title:') !!}
    <p>{!! $references->job_title !!}</p>
</div>

<!-- Organization Field -->
<div class="form-group">
    {!! Form::label('organization', 'Organization:') !!}
    <p>{!! $references->organization !!}</p>
</div>

<!-- Relation Field -->
<div class="form-group">
    {!! Form::label('relation', 'Relation:') !!}
    <p>{!! $references->relation !!}</p>
</div>

<!-- Phone Number Field -->
<div class="form-group">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    <p>{!! $references->phone_number !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $references->address !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $references->application_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $references->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $references->updated_at !!}</p>
</div>

