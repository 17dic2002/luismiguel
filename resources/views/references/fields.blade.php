<!-- Name Referee Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name_referee', 'Name Referee:') !!}
    {!! Form::text('name_referee', null, ['class' => 'form-control']) !!}
</div>

<!-- Job Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('job_title', 'Job Title:') !!}
    {!! Form::text('job_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Organization Field -->
<div class="form-group col-sm-6">
    {!! Form::label('organization', 'Organization:') !!}
    {!! Form::text('organization', null, ['class' => 'form-control']) !!}
</div>

<!-- Relation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('relation', 'Relation:') !!}
    {!! Form::text('relation', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone_number', 'Phone Number:') !!}
    {!! Form::text('phone_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::number('application_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('references.index') !!}" class="btn btn-default">Cancel</a>
</div>
