@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            References
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($references, ['route' => ['references.update', $references->id], 'method' => 'patch']) !!}

                        @include('references.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection