<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('admin_email', 'Admin Email:') !!}
    {!! Form::text('admin_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('digital_ad_email', 'Digital Request Add Email Recipients:') !!}
    {!! Form::text('digital_ad_email', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('traditional_ad_email', 'Traditional Request Add Email Recipients:') !!}
    {!! Form::text('traditional_ad_email', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('options.index') !!}" class="btn btn-default">Cancel</a>
</div>
