<?php

namespace App\Repositories;

use App\Models\Jobs;
use App\Repositories\BaseRepository;

/**
 * Class JobsRepository
 * @package App\Repositories
 * @version November 14, 2019, 4:26 am UTC
*/

class JobsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'location',
        'status',
        'description',
        'stop_publish',
        'start_publish'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Jobs::class;
    }
}
