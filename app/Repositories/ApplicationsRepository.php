<?php

namespace App\Repositories;

use App\Models\Applications;
use App\Repositories\BaseRepository;

/**
 * Class ApplicationsRepository
 * @package App\Repositories
 * @version November 15, 2019, 4:00 am UTC
*/

class ApplicationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'application_title',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'home_phone',
        'mobile_number',
        'gender',
        'category',
        'other_category',
        'type',
        'years_of_experience',
        'date_of_birth',
        'currency',
        'current_salary',
        'period_cs',
        'currency_expected',
        'expected_salary',
        'period_es',
        'driver_license',
        'address',
        'resume',
        'list_skills',
        'professional_qualifications_skills',
        'other_relevant_skills',
        'hobbies',
        'how_hear',
        'tag',
        'job_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Applications::class;
    }
}
