<?php

namespace App\Repositories;

use App\Models\Languages;
use App\Repositories\BaseRepository;

/**
 * Class LanguagesRepository
 * @package App\Repositories
 * @version November 18, 2019, 7:48 pm UTC
*/

class LanguagesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'language',
        'understanding',
        'writing',
        'reading',
        'where_learn',
        'application_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Languages::class;
    }
}
