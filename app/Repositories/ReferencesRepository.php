<?php

namespace App\Repositories;

use App\Models\References;
use App\Repositories\BaseRepository;

/**
 * Class ReferencesRepository
 * @package App\Repositories
 * @version November 18, 2019, 7:48 pm UTC
*/

class ReferencesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name_referee',
        'job_title',
        'organization',
        'relation',
        'phone_number',
        'address',
        'application_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return References::class;
    }
}
