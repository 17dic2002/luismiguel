<?php

namespace App\Repositories;

use App\Models\WorkExperiences;
use App\Repositories\BaseRepository;

/**
 * Class WorkExperiencesRepository
 * @package App\Repositories
 * @version November 18, 2019, 7:51 pm UTC
*/

class WorkExperiencesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'employer_name',
        'position',
        'responsabilities',
        'supervisors_name',
        'position_start_date',
        'position_end_date',
        'reason_leaving',
        'phone',
        'addresss',
        'application_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WorkExperiences::class;
    }
}
