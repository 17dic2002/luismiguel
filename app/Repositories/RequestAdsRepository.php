<?php

namespace App\Repositories;

use App\Models\RequestAds;
use App\Repositories\BaseRepository;

/**
 * Class RequestAdsRepository
 * @package App\Repositories
 * @version November 14, 2019, 4:23 am UTC
*/

class RequestAdsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'requester_name',
        'requester_email',
        'print_add',
        'name_publication',
        'ad_size',
        'date_publication',
        'template_print',
        'comments_template',
        'book_media',
        'due_date',
        'blue_print',
        'budget',
        'comments_bp',
        'social_media',
        'platforms',
        'other_platform',
        'template_social',
        'job_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequestAds::class;
    }
}
