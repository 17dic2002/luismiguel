<?php

namespace App\Repositories;

use App\Models\Educations;
use App\Repositories\BaseRepository;

/**
 * Class EducationsRepository
 * @package App\Repositories
 * @version November 18, 2019, 7:27 pm UTC
*/

class EducationsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'institute',
        'study_area',
        'certificate',
        'other',
        'date_from',
        'date_to',
        'application_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Educations::class;
    }
}
