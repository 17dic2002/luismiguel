<?php

namespace App\Http\Controllers;

use App\DataTables\RequestAdsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRequestAdsRequest;
use App\Http\Requests\UpdateRequestAdsRequest;
use App\Models\Jobs;
use App\Repositories\RequestAdsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class RequestAdsController extends AppBaseController
{
    /** @var  RequestAdsRepository */
    private $requestAdsRepository;

    public function __construct(RequestAdsRepository $requestAdsRepo)
    {
        $this->requestAdsRepository = $requestAdsRepo;
    }

    /**
     * Display a listing of the RequestAds.
     *
     * @param RequestAdsDataTable $requestAdsDataTable
     * @return Response
     */
    public function index(RequestAdsDataTable $requestAdsDataTable)
    {
        return $requestAdsDataTable->render('request_ads.index');
    }

    /**
     * Show the form for creating a new RequestAds.
     *
     * @return Response
     */
    public function create()
    {
        $jobs = Jobs::all();

        return view('request_ads.create', compact('jobs'));
    }

    /**
     * Store a newly created RequestAds in storage.
     *
     * @param CreateRequestAdsRequest $request
     *
     * @return Response
     */
    public function store(CreateRequestAdsRequest $request)
    {
        $input = $request->all();

        if (isset($input['platforms'])) {
            $p = '';
            $input['platforms'] = implode(' | ', $input['platforms']);
        }


        //dd($input);
        if ($input['print_add'] == 'Yes') {
            $dates = explode(' - ', $input['date_publication']);
            $dates2 = explode(' - ', $input['date_publication_2']);
            $dates3 = explode(' - ', $input['date_publication_3']);


            $input['date_publication'] = date('Y-m-d h:i', strtotime($dates[0]));
            $input['date_finish'] = date('Y-m-d h:i', strtotime($dates[1]));

            $input['date_publication_2'] = date('Y-m-d h:i', strtotime($dates2[0]));
            $input['date_finish_2'] = date('Y-m-d h:i', strtotime($dates2[1]));

            $input['date_publication_3'] = date('Y-m-d h:i', strtotime($dates3[0]));
            $input['date_finish_3'] = date('Y-m-d h:i', strtotime($dates3[1]));

        }

        $requestAds = $this->requestAdsRepository->create($input);
        $job_title = Jobs::find($input['job_id']);
        $input['job_title'] = $job_title->title;


        if ($input['print_add'] == 'Yes') {
            //traditional
            MailsController::mediaRequest($input);
        }
        if ($input['social_media'] == 'Yes') {
            //digital
            MailsController::digitalMedia($input);

        }
        //Enviar mail the Media request

        Flash::success('Request Ads saved successfully and mail sent.');


        return redirect(route('requestAds.index'));
    }


    /**
     * Display the specified RequestAds.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $requestAds = $this->requestAdsRepository->find($id);

        if (empty($requestAds)) {
            Flash::error('Request Ads not found');

            return redirect(route('requestAds.index'));
        }

        return view('request_ads.show')->with('requestAds', $requestAds);
    }

    /**
     * Show the form for editing the specified RequestAds.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $requestAds = $this->requestAdsRepository->find($id);
        $jobs = Jobs::all();

        if (empty($requestAds)) {
            Flash::error('Request Ad not found');

            return redirect(route('requestAds.index'));
        }

        return view('request_ads.edit', compact('requestAds', 'jobs'));
    }

    /**
     * Update the specified RequestAds in storage.
     *
     * @param int $id
     * @param UpdateRequestAdsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequestAdsRequest $request)
    {
        $requestAds = $this->requestAdsRepository->find($id);

        if (empty($requestAds)) {
            Flash::error('Request Ads not found');

            return redirect(route('requestAds.index'));
        }

        $requestAds = $this->requestAdsRepository->update($request->all(), $id);

        Flash::success('Request Ads updated successfully.');

        return redirect(route('requestAds.index'));
    }

    /**
     * Remove the specified RequestAds from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $requestAds = $this->requestAdsRepository->find($id);

        if (empty($requestAds)) {
            Flash::error('Request Ads not found');

            return redirect(route('requestAds.index'));
        }

        $this->requestAdsRepository->delete($id);

        Flash::success('Request Ads deleted successfully.');

        return redirect(route('requestAds.index'));
    }
}
