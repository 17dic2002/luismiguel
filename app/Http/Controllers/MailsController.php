<?php

namespace App\Http\Controllers;

use App\Mail\AdminApplicationConfirmation;
use App\Mail\ApplicationConfirmation;
use App\Mail\DigitalMediaParadigm;
use App\Mail\MediaRequest;
use App\Mail\NewJob;
use App\Mail\RegisterConfirmationMail;
use App\Models\Options;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailsController extends Controller
{

    public static function mediaRequest($data)
    {
        $mails = explode(',', Options::first()->traditional_ad_email);
        $withAdmin = array_push($mails, Auth::user()->email);
        Mail::to($mails)->send(new MediaRequest($data));
    }

    public static function digitalMedia($data)
    {
        $mails = explode(',', Options::first()->digital_ad_email);
        $withAdmin = array_push($mails, Auth::user()->email);

        Mail::to($mails)->send(new DigitalMediaParadigm($data));
    }

    public static function newJobPosting($data)
    {
        Mail::to(Auth::user()->email)->send(new NewJob($data));
    }

    public static function applicationConfirmation($data)
    {
        Mail::to(Auth::user()->email)->send(new ApplicationConfirmation($data));
    }
    public static function adminApplicationConfirmation($data)
    {
        $mails = Options::first()->admin_email;
        Mail::to($mails)->send(new AdminApplicationConfirmation($data));
    }
}
