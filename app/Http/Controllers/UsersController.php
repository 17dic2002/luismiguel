<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\Models\Users;
use App\Repositories\UsersRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Spatie\Permission\Models\Role;

class UsersController extends AppBaseController
{
    /** @var  UsersRepository */
    private $usersRepository;

    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepository = $usersRepo;
    }

    /**
     * Display a listing of the Users.
     *
     * @param UsersDataTable $usersDataTable
     * @return Response
     */
    public function index(UsersDataTable $usersDataTable)
    {
        return $usersDataTable->render('users.index');
    }


    /**
     * Show the form for creating a new Users.
     *
     * @return Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('users.create', compact( 'roles'));
    }

    /**
     * Store a newly created Users in storage.
     *
     * @param CreateUsersRequest $request
     *
     * @return Response
     */
    public function store(CreateUsersRequest $request)
    {
        $input = $request->all();
        $user = Users::where('email',$input['email'])->withTrashed()->first();

        if ($user) {
            if ( $user->trashed() ) {
                $user->restore();
                Flash::success('User has been restored from trash with same password.');

                return redirect( route('users.index') );
            }
        }

        $roles = $input['role'];
        unset($input['role']);

        $input['password'] = bcrypt($request['password']);
        $user = $this->usersRepository->create($input);

        foreach ($roles as $key => $roleName) {
            $role = Role::findByName($roleName);
            $user->assignRole($role);
        }

        Flash::success('Users saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified Users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('users', $users);
    }

    /**
     * Show the form for editing the specified Users.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $users = $this->usersRepository->find($id);
        $roles = Role::all();
        $selectedRoles = $users->roles->pluck('name');
        unset($users->password);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        return view('users.edit', compact('users', 'roles', 'selectedRoles'));
    }

    /**
     * Update the specified Users in storage.
     *
     * @param int $id
     * @param UpdateUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUsersRequest $request)
    {

        $users = $this->usersRepository->find($id);
        $roleNames = $request['role'];

        if (empty($users)) {
            Flash::error('Users eliminado correctamente');

            return redirect(route('users.index'));
        }

        if ($request['password'] == null or $request['password'] == '') {
            $users = $this->usersRepository->find($id);
            $request['password'] = $users->password;
        } else {
            $request['password'] = bcrypt($request['password']);
        }



        $users = $this->usersRepository->update($request->all(), $id);

        $roles = Role::whereIn('name', $roleNames)->get();

        $users->roles()->sync($roles);

        Flash::success('Users updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified Users from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $users = $this->usersRepository->find($id);

        if (empty($users)) {
            Flash::error('Users not found');

            return redirect(route('users.index'));
        }

        $this->usersRepository->delete($id);

        Flash::success('Users deleted successfully.');

        return redirect(route('users.index'));
    }
}
