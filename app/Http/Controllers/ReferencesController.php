<?php

namespace App\Http\Controllers;

use App\DataTables\ReferencesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateReferencesRequest;
use App\Http\Requests\UpdateReferencesRequest;
use App\Repositories\ReferencesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ReferencesController extends AppBaseController
{
    /** @var  ReferencesRepository */
    private $referencesRepository;

    public function __construct(ReferencesRepository $referencesRepo)
    {
        $this->referencesRepository = $referencesRepo;
    }

    /**
     * Display a listing of the References.
     *
     * @param ReferencesDataTable $referencesDataTable
     * @return Response
     */
    public function index(ReferencesDataTable $referencesDataTable)
    {
        return $referencesDataTable->render('references.index');
    }

    /**
     * Show the form for creating a new References.
     *
     * @return Response
     */
    public function create()
    {
        return view('references.create');
    }

    /**
     * Store a newly created References in storage.
     *
     * @param CreateReferencesRequest $request
     *
     * @return Response
     */
    public function store(CreateReferencesRequest $request)
    {
        $input = $request->all();

        $references = $this->referencesRepository->create($input);

        Flash::success('References saved successfully.');

        return redirect(route('references.index'));
    }

    /**
     * Display the specified References.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $references = $this->referencesRepository->find($id);

        if (empty($references)) {
            Flash::error('References not found');

            return redirect(route('references.index'));
        }

        return view('references.show')->with('references', $references);
    }

    /**
     * Show the form for editing the specified References.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $references = $this->referencesRepository->find($id);

        if (empty($references)) {
            Flash::error('References not found');

            return redirect(route('references.index'));
        }

        return view('references.edit')->with('references', $references);
    }

    /**
     * Update the specified References in storage.
     *
     * @param  int              $id
     * @param UpdateReferencesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReferencesRequest $request)
    {
        $references = $this->referencesRepository->find($id);

        if (empty($references)) {
            Flash::error('References not found');

            return redirect(route('references.index'));
        }

        $references = $this->referencesRepository->update($request->all(), $id);

        Flash::success('References updated successfully.');

        return redirect(route('references.index'));
    }

    /**
     * Remove the specified References from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $references = $this->referencesRepository->find($id);

        if (empty($references)) {
            Flash::error('References not found');

            return redirect(route('references.index'));
        }

        $this->referencesRepository->delete($id);

        Flash::success('References deleted successfully.');

        return redirect(route('references.index'));
    }
}
