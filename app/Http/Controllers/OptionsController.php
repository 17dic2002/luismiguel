<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateOptionsRequest;
use App\Models\Options;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Redirect;

class OptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $options = Options::first();


        return view('options.edit', compact('options'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOptionsRequest $request, $id)
    {

        $options = Options::first();
        $input = $request->all();


        if (empty($options)) {
            Flash::error('Options not found');

            return redirect(route('options.index'));
        }
        $options->admin_email = $input['admin_email'];
        $options->traditional_ad_email = $input['traditional_ad_email'];
        $options->digital_ad_email = $input['digital_ad_email'];
        $options->save();
        Flash::success('Successfully updated options!');
        return Redirect::to('options');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
