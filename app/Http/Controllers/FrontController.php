<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationsRequest;
use App\Mail\RegisterConfirmationMail;
use App\Models\Applications;
use App\Models\Educations;
use App\Models\Jobs;
use App\Models\Languages;
use App\Models\References;
use App\Models\WorkExperiences;
use App\Repositories\ApplicationsRepository;
use App\Repositories\ReferencesRepository;
use App\Repositories\EducationsRepository;
use App\Repositories\JobsRepository;
use App\Repositories\LanguagesRepository;
use App\Repositories\WorkExperiencesRepository;
use Carbon\Carbon;
use Doctrine\DBAL\Schema\AbstractAsset;
use Illuminate\Http\Request;
use Illuminate\Queue\Jobs\Job;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Console\Input\Input;
use Flash;
use function Matrix\diagonal;


class FrontController extends Controller
{

    /** @var  JobsRepository */
    private $jobsRepository;
    private $applicationsRepository;
    private $educationsRepository;
    private $languagesRepository;
    private $referencesRepository;
    private $workExperiencesRepository;

    public function __construct(JobsRepository $jobsRepo, ApplicationsRepository $applicationsRepo, EducationsRepository $educationsRepo, LanguagesRepository $languagesRepo, ReferencesRepository $referencesRepo, WorkExperiencesRepository $workExperiencesRepo)
    {
        $this->jobsRepository = $jobsRepo;
        $this->applicationsRepository = $applicationsRepo;
        $this->educationsRepository = $educationsRepo;
        $this->languagesRepository = $languagesRepo;
        $this->referencesRepository = $referencesRepo;
        $this->workExperiencesRepository = $workExperiencesRepo;
    }

    public function indexFront()
    {
        $jobs = Jobs::where('status', '=', 'Active')->whereDate('start_publish', '<=', Carbon::now())->whereDate('stop_publish', '>=', Carbon::now())->take(5)->get();


        return view('front.home', compact('jobs'));
    }


    public function thankyou()
    {
        $lang = session('lang');
        $this->setLocale($lang);

        return view('front.forms.thankyou');
    }

    public function careers()
    {
        $jobs = Jobs::where('status', '=', 'Active')->where('all_jobs' ,'=', true)->whereDate('start_publish', '<=', Carbon::now())->whereDate('stop_publish', '>=', Carbon::now())->get();


        return view('front.jobList', compact('jobs'));
    }

    public function careersInternal()
    {
        $jobs = Jobs::where('status', '=', 'Active')->where('internal' ,'=', true)->whereDate('start_publish', '<=', Carbon::now())->whereDate('stop_publish', '>=', Carbon::now())->get();

        return view('front.jobList', compact('jobs'));
    }

    public function viewJob($idJob)
    {
        $job = $this->jobsRepository->find($idJob);
        return view('front.singleJob', compact('job'));
    }

    /**
     * @param $email
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description Verify if this applications belongs to the Auth user
     */
    public function verifyOwner($email)
    {

        if (Auth::user()->email != $email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }
        return;

    }

    public function verifyStep($step, $idApp)
    {
        switch ($step) {
            case 1:
                return redirect()->route('applyForm', [$idApp]);
                break;
            case 2:
                return redirect()->route('applyForm.education', [$idApp]);
                break;
            case 3:
                return redirect()->route('applyForm.experience', [$idApp]);
            case 4:
                return redirect()->route('applyForm.skills', [$idApp]);
            case 5:
                return redirect()->route('applyForm.references', [$idApp]);
            case 6:
                return redirect()->route('applyForm.languages', [$idApp]);
            case 7:
                return redirect()->route('applyForm.resume', [$idApp]);
                break;
        }
    }


    public function verifyLanguage($job, $application, $step, $edit, $data = null)
    {



        /*$hideFields = false;
        $locationsEnglish = array('Corporate Office', 'Anguila', 'Antigua & Barbuda', 'Bahamas', 'Barbados', 'Belize', 'Bermuda', 'BVI', 'Cayman Island', 'Dominica', 'Grenada', 'Guyana', 'Jamaica', 'St. Kitts & Nevis', 'St. Lucia', 'St. Maarten', 'St. Vincent & The Grenadines', 'Suriname');
        $locationsSpanish = array('Dominican Republic', 'Puerto Rico');
        $locationsFrench = array('French Guiana', 'Guadelope', 'Martinique');


        if (in_array($job->location, $locationsEnglish)) {
            app()->setLocale('en');
        } elseif (in_array($job->location, $locationsFrench)) {
            app()->setLocale('fr');
        } elseif (in_array($job->location, $locationsSpanish)) {

            if ($job->location == 'Puerto Rico') {
                $hideFields = true;
            }
            app()->setLocale('es');
        } else {
            abort(404, 'Page not found.');
        }*/

        $lang = session('lang');
        $this->setLocale($lang);
        $hideFields = session('hideFields');
        return view('front.forms.' . $step, compact('job', 'application', 'edit', 'hideFields', 'data'));
    }


    public function applyFormPost(Request $request, $idJob, $data = null)
    {

        $job = $this->jobsRepository->find($idJob);
        $hideFields = false;

        if ($job->status == 'Inactive' || ! ($job->start_publish <= Carbon::now() && $job->stop_publish >= Carbon::now())) {
            Flash::error('This job is not receiving more applicants.');
            return redirect()->route('home');
        }

        $application = Applications::where('email', '=', Auth::user()->email)->where('job_id', $idJob)->get()->first();
        /* var_dump($application);
         die();*/

        if ($job->location == 'Puerto Rico') {
            $hideFields = true;
        }
        session(['hideFields'=>$hideFields]);
        $input = $request->all();
        $lang = $input['lang'];
        $edit = false;

        $this->setLocale($lang);

        if (!empty($application)) {
            $edit = true;
        }
        //return $this->verifyLanguage($job, $application, 'personal', $edit);
        //verifyLanguage($job, $application, $step, $edit, $data = null)
        return view('front.forms.' . 'personal', compact('job', 'application', 'edit', 'hideFields', 'data'));

    }

    public function setLocale($lang)
    {
        session(['lang'=>$lang]);
        switch ($lang) {
            case 'en':
                app()->setLocale('en');
                break;
            case 'es':
                app()->setLocale('es');
                break;
            case 'fr':
                app()->setLocale('fr');
                break;

        }

    }

    public function applyForm($idJob)
    {
        $job = $this->jobsRepository->find($idJob);

        if ($job->status == 'Inactive' || ! ($job->start_publish <= Carbon::now() && $job->stop_publish >= Carbon::now())) {
            Flash::error('This job is not receiving more applicants.');
            return redirect()->route('home');
        }

        $application = Applications::where('email', '=', Auth::user()->email)->where('job_id', $idJob)->get()->first();
        /* var_dump($application);
         die();*/
        if (!empty($application)) {
            return $this->verifyLanguage($job, $application, 'personal', true);
        }

        return $this->verifyLanguage($job, null, 'personal', false);

    }


    public function savePersonalInformation(Request $request, $jobID)
    {
        $input = $request->all();

        $job = $this->jobsRepository->find($jobID);
        $input['application_title'] = $job->title;
        $input['job_id'] = $job->id;
        $input['email'] = $email = Auth::user()->email;
        $input['driver_license'] = $input['driver_license'];
        $input['step'] = '1';

        $applications = $this->applicationsRepository->create($input);
        Flash::success('Personal Information saved successfully.');

        return redirect()->route('applyForm.education', [$applications->id]);
    }


    public function updatePersonalInformation(Request $request, $jobID)
    {
        $input = $request->all();
        $application = Applications::where('email', '=', Auth::user()->email)->where('job_id', $jobID)->get()->first();

        $applications = $this->applicationsRepository->update($input, $application->id);
        Flash::success('Personal Information updated successfully.');
        return redirect()->route('applyForm.education', [$applications->id]);

    }


    public function applyFormEducation($appID)
    {

        $educations = $this->educationsRepository->find($appID);

        $application = $this->applicationsRepository->find($appID);


        //Verify if this applications belongs to the Auth user
        if (Auth::user()->email != $application->email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }
        $job = $this->jobsRepository->find($application->job_id);
        //Verify if there is educations for this application
        $educations = Educations::where('application_id', '=', $appID)->get();
        if (!empty($educations[0])) {
            return $this->verifyLanguage($job, $application, 'education', true, $educations);
        }
        return $this->verifyLanguage($job, $application, 'education', false);
    }

    public function saveEducation(Request $request, $appID)
    {
        $input = $request->all();
        $application = $this->applicationsRepository->find($appID);


        foreach ($input['education'] as $key => $educ) {
            $educ['application_id'] = $application->id;
            $education = $this->educationsRepository->create($educ);
        }

        $step['step'] = '2';
        $applications = $this->applicationsRepository->update($step, $appID);


        Flash::success('Educations saved successfully.');

        return redirect()->route('applyForm.experience', [$appID]);

    }

    public function updateEducation(Request $request, $appID)
    {

        //Buscar todos los registros de cada uno y verificar si viene en el array
        //del request, si no, eliminarlo usar idApp para ello
        $actualEduc = Educations::where('application_id', '=', $appID)->get();

        $input = $request->all();
        $application = $this->applicationsRepository->find($appID);
        $i = 0;
        foreach ($input['education'] as $key => $educ) {

            if (empty($input['idApp' . $i])) {
                $educ['application_id'] = $application->id;
                $education = $this->educationsRepository->create($educ);
            } else {
                $id = $input['idApp' . $i];
                $educ['application_id'] = $application->id;
                $educations = $this->educationsRepository->find($id);
                $education = $this->educationsRepository->update($educ, $id);
            }
            $i++;
        }
        Flash::success(__('form.education') . ' ' . __('form.updated'));

        return redirect()->route('applyForm.experience', [$appID]);

    }

    public function applyFormExperience($appID)
    {
        $application = $this->applicationsRepository->find($appID);

        //Verify if this applications belongs to the Auth user
        if (Auth::user()->email != $application->email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }

        $job = $this->jobsRepository->find($application->job_id);

        $experience = WorkExperiences::where('application_id', '=', $appID)->get();
        if (!empty($experience[0])) {
            return $this->verifyLanguage($job, $application, 'experience', true, $experience);
        }

        return $this->verifyLanguage($job, $application, 'experience', 'false');

    }

    public function saveExperience(Request $request, $appID)
    {
        $input = $request->all();

        foreach ($input['experience'] as $key => $exp) {
            $exp['application_id'] = $appID;

            $experience = $this->workExperiencesRepository->create($exp);
        }

        $step['step'] = '3';
        $applications = $this->applicationsRepository->update($step, $appID);


        Flash::success('Experiences saved successfully.');
        return redirect()->route('applyForm.skills', [$appID]);

    }

    public function updateExperience(Request $request, $appID)
    {
        $input = $request->all();
        $application = $this->applicationsRepository->find($appID);
        $i = 0;
        foreach ($input['experience'] as $key => $exp) {

            if (empty($input['idApp' . $i])) {
                $exp['application_id'] = $application->id;
                $education = $this->workExperiencesRepository->create($exp);
            } else {
                $id = $input['idApp' . $i];
                $exp['application_id'] = $application->id;
                $educations = $this->workExperiencesRepository->find($id);
                $education = $this->workExperiencesRepository->update($exp, $id);
            }
            $i++;
        }
        Flash::success(__('form.experiences') . ' ' . __('form.updated'));

        return redirect()->route('applyForm.skills', [$appID]);

    }

    public function applyFormSkills($appID)
    {
        $application = $this->applicationsRepository->find($appID);

        //Verify if this applications belongs to the Auth user
        if (Auth::user()->email != $application->email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }

        $job = $this->jobsRepository->find($application->job_id);
        $skills = Applications::select('list_skills', 'professional_qualifications_skills', 'other_relevant_skills', 'hobbies')->where('id', '=', $appID)->get()->first();

        if (!empty($skills)) {
            return $this->verifyLanguage($job, $application, 'skills', true, $skills);
        }

        return $this->verifyLanguage($job, $application, 'skills', false);

    }

    public function saveSkills(Request $request, $appID)
    {
        $input = $request->all();
        $input['step'] = '4';
        $applications = $this->applicationsRepository->update($input, $appID);
        return redirect()->route('applyForm.references', [$appID]);

    }

    public function updateSkills(Request $request, $appID)
    {
        $input = $request->all();
        $input['step'] = '4';
        $applications = $this->applicationsRepository->update($input, $appID);
        return redirect()->route('applyForm.references', [$appID]);

    }

    public function applyFormReferences($appID)
    {
        $application = $this->applicationsRepository->find($appID);

        //Verify if this applications belongs to the Auth user
        if (Auth::user()->email != $application->email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }
        $job = $this->jobsRepository->find($application->job_id);
        $references = References::where('application_id', '=', $appID)->get();
        if (!empty($references[0])) {

            return $this->verifyLanguage($job, $application, 'references', true, $references);
        }

        return $this->verifyLanguage($job, $application, 'references', false);

    }

    public function saveReferences(Request $request, $appID)
    {
        $input = $request->all();

        foreach ($input['references'] as $key => $ref) {
            $ref['application_id'] = $appID;
            $reference = $this->referencesRepository->create($ref);
        }
        $step['step'] = '5';
        $applications = $this->applicationsRepository->update($step, $appID);


        Flash::success('References saved successfully.');

        return redirect()->route('applyForm.languages', [$appID]);

    }

    public function updateReferences(Request $request, $appID)
    {
        $input = $request->all();
        $application = $this->applicationsRepository->find($appID);
        $i = 0;

        foreach ($input['references'] as $key => $ref) {

            if (empty($input['idApp' . $i])) {
                $ref['application_id'] = $application->id;
                $reference = $this->referencesRepository->create($ref);
            } else {
                $id = $input['idApp' . $i];
                $ref['application_id'] = $application->id;
                $reference = $this->referencesRepository->update($ref, $id);
            }
            $i++;
        }
        Flash::success(__('form.references') . ' ' . __('form.updated'));

        return redirect()->route('applyForm.languages', [$appID]);

    }

    public function applyFormLanguages($appID)
    {
        $application = $this->applicationsRepository->find($appID);

        //Verify if this applications belongs to the Auth user
        if (Auth::user()->email != $application->email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }

        $job = $this->jobsRepository->find($application->job_id);


        $languages = Languages::where('application_id', '=', $appID)->get();
        if (!empty($languages[0])) {
            return $this->verifyLanguage($job, $application, 'languages', true, $languages);
        }
        return $this->verifyLanguage($job, $application, 'languages', false);
    }

    public function saveLanguages(Request $request, $appID)
    {
        $input = $request->all();
        foreach ($input['languages'] as $key => $lang) {
            $lang['application_id'] = $appID;
            $reference = $this->languagesRepository->create($lang);
        }
        $step['step'] = '6';
        $applications = $this->applicationsRepository->update($step, $appID);


        Flash::success('Languages saved successfully.');

        return redirect()->route('applyForm.resume', [$appID]);
    }

    public function updateLanguages(Request $request, $appID)
    {
        $input = $request->all();
        $application = $this->applicationsRepository->find($appID);
        $i = 0;

        foreach ($input['languages'] as $key => $lang) {

            if (empty($input['idApp' . $i])) {
                $lang['application_id'] = $application->id;
                $languages = $this->languagesRepository->create($lang);
            } else {
                $id = $input['idApp' . $i];
                $lang['application_id'] = $application->id;
                $reference = $this->languagesRepository->update($lang, $id);
            }
            $i++;
        }
        Flash::success(__('form.references') . ' ' . __('form.updated'));

        return redirect()->route('applyForm.resume', [$appID]);

    }

    public function applyFormResume($appID)
    {
        $application = $this->applicationsRepository->find($appID);

        //Verify if this applications belongs to the Auth user
        if (Auth::user()->email != $application->email) {
            Flash::error('You are not applying to this job');
            return redirect()->route('home');
        }

        $job = $this->jobsRepository->find($application->job_id);
        $resume = Applications::select('resume', 'how_hear')->where('id', '=', $appID)->get()->first();

        if (!empty($resume)) {
            return $this->verifyLanguage($job, $application, 'resume', true, $resume);
        }

        return $this->verifyLanguage($job, $application, 'resume', false);

    }

    public function saveResume(Request $request, $appID)
    {

        $input = $request->all();

        if ($input['how_hear'] == 'Other') {
            $input['how_hear'] = $input['otherhear'];
        }

        $input['step'] = '7';
        $input['resume']->move(public_path('uploads/applications'), $input['resume']->getClientOriginalName());
        $input['resume'] = $input['resume']->getClientOriginalName();
        $applications = $this->applicationsRepository->update($input, $appID);


        exit();
        //Send email
        MailsController::applicationConfirmation($applications);
        MailsController::adminApplicationConfirmation($applications);

        app()->setLocale($input['locale']);

        return redirect()->route('thankyou');

    }

    public function updateResume(Request $request, $appID)
    {
        $input = $request->all();
        $input['step'] = '4';
        $input['resume']->move(public_path('uploads/applications'), $input['resume']->getClientOriginalName());
        $input['resume'] = $input['resume']->getClientOriginalName();

        //dd($input);

        $applications = $this->applicationsRepository->update($input, $appID);
        MailsController::applicationConfirmation($applications);
        MailsController::adminApplicationConfirmation($applications);
        $this->setLocale($input['locale']);

        return redirect()->route('thankyou', [$appID]);

    }


}
