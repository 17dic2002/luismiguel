<?php

namespace App\Http\Controllers;

use App\DataTables\WorkExperiencesDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateWorkExperiencesRequest;
use App\Http\Requests\UpdateWorkExperiencesRequest;
use App\Repositories\WorkExperiencesRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class WorkExperiencesController extends AppBaseController
{
    /** @var  WorkExperiencesRepository */
    private $workExperiencesRepository;

    public function __construct(WorkExperiencesRepository $workExperiencesRepo)
    {
        $this->workExperiencesRepository = $workExperiencesRepo;
    }

    /**
     * Display a listing of the WorkExperiences.
     *
     * @param WorkExperiencesDataTable $workExperiencesDataTable
     * @return Response
     */
    public function index(WorkExperiencesDataTable $workExperiencesDataTable)
    {
        return $workExperiencesDataTable->render('work_experiences.index');
    }

    /**
     * Show the form for creating a new WorkExperiences.
     *
     * @return Response
     */
    public function create()
    {
        return view('work_experiences.create');
    }

    /**
     * Store a newly created WorkExperiences in storage.
     *
     * @param CreateWorkExperiencesRequest $request
     *
     * @return Response
     */
    public function store(CreateWorkExperiencesRequest $request)
    {
        $input = $request->all();

        $workExperiences = $this->workExperiencesRepository->create($input);

        Flash::success('Work Experiences saved successfully.');

        return redirect(route('workExperiences.index'));
    }

    /**
     * Display the specified WorkExperiences.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $workExperiences = $this->workExperiencesRepository->find($id);

        if (empty($workExperiences)) {
            Flash::error('Work Experiences not found');

            return redirect(route('workExperiences.index'));
        }

        return view('work_experiences.show')->with('workExperiences', $workExperiences);
    }

    /**
     * Show the form for editing the specified WorkExperiences.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $workExperiences = $this->workExperiencesRepository->find($id);

        if (empty($workExperiences)) {
            Flash::error('Work Experiences not found');

            return redirect(route('workExperiences.index'));
        }

        return view('work_experiences.edit')->with('workExperiences', $workExperiences);
    }

    /**
     * Update the specified WorkExperiences in storage.
     *
     * @param  int              $id
     * @param UpdateWorkExperiencesRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWorkExperiencesRequest $request)
    {
        $workExperiences = $this->workExperiencesRepository->find($id);

        if (empty($workExperiences)) {
            Flash::error('Work Experiences not found');

            return redirect(route('workExperiences.index'));
        }

        $workExperiences = $this->workExperiencesRepository->update($request->all(), $id);

        Flash::success('Work Experiences updated successfully.');

        return redirect(route('workExperiences.index'));
    }

    /**
     * Remove the specified WorkExperiences from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $workExperiences = $this->workExperiencesRepository->find($id);

        if (empty($workExperiences)) {
            Flash::error('Work Experiences not found');

            return redirect(route('workExperiences.index'));
        }

        $this->workExperiencesRepository->delete($id);

        Flash::success('Work Experiences deleted successfully.');

        return redirect(route('workExperiences.index'));
    }
}
