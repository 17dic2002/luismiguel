<?php

namespace App\Http\Controllers;

use App\DataTables\ApplicationsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateApplicationsRequest;
use App\Http\Requests\UpdateApplicationsRequest;
use App\Models\Applications;
use App\Repositories\ApplicationsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ApplicationsController extends AppBaseController
{
    /** @var  ApplicationsRepository */
    private $applicationsRepository;

    public function __construct(ApplicationsRepository $applicationsRepo)
    {
        $this->applicationsRepository = $applicationsRepo;
    }

    /**
     * Display a listing of the Applications.
     *
     * @param ApplicationsDataTable $applicationsDataTable
     * @return Response
     */
    public function index(ApplicationsDataTable $applicationsDataTable)
    {
        return $applicationsDataTable->render('applications.index');
    }

    public function indexSavedForLater(ApplicationsDataTable $applicationsDataTable)
    {

        return $applicationsDataTable->render('applications.index');
    }

    public function indexShortList(ApplicationsDataTable $applicationsDataTable)
    {

        return $applicationsDataTable->render('applications.index');
    }

    public function indexRejected(ApplicationsDataTable $applicationsDataTable)
    {

        return $applicationsDataTable->render('applications.index');
    }

    /**
     * Show the form for creating a new Applications.
     *
     * @return Response
     */
    public function create()
    {
        return view('applications.create');
    }

    /**
     * Store a newly created Applications in storage.
     *
     * @param CreateApplicationsRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationsRequest $request)
    {
        $input = $request->all();

        $applications = $this->applicationsRepository->create($input);

        Flash::success('Applications saved successfully.');

        return redirect(route('applications.index'));
    }

    /**
     * Display the specified Applications.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $applications = Applications::with('educations')->with('job')->with('workExperiences')->with('references')->with('languages')->where('id', '=', $id)->get();
        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }

        return view('applications.show')->with('applications', $applications[0]);
    }

    /**
     * Show the form for editing the specified Applications.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applications = $this->applicationsRepository->find($id);

        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }

        return view('applications.edit')->with('applications', $applications);
    }

    /**
     * Update the specified Applications in storage.
     *
     * @param int $id
     * @param UpdateApplicationsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationsRequest $request)
    {

        $applications = $this->applicationsRepository->find($id);

        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }

        $applications = $this->applicationsRepository->update($request->all(), $id);

        Flash::success('Applications updated successfully.');

        return redirect(route('applications.index'));
    }

    /**
     * Remove the specified Applications from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applications = $this->applicationsRepository->find($id);

        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }

        $this->applicationsRepository->delete($id);

        Flash::success('Applications deleted successfully.');

        return redirect(route('applications.index'));
    }

    public function saveForLater($id)
    {
        $applications = $this->applicationsRepository->find($id);

        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }
        $tag['tag'] = 'Saved For Later';

        $applications = $this->applicationsRepository->update($tag, $id);

        Flash::success('Applications saved for later successfully.');

        return redirect(route('applications.index'));
    }

    public function reject($id)
    {
        $applications = $this->applicationsRepository->find($id);

        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }

        $tag['tag'] = 'Rejected';

        $applications = $this->applicationsRepository->update($tag, $id);

        Flash::success('Applications rejected successfully.');

        return redirect(route('applications.index'));
    }

    public function addShortList($id)
    {
        $applications = $this->applicationsRepository->find($id);

        if (empty($applications)) {
            Flash::error('Applications not found');

            return redirect(route('applications.index'));
        }
        $tag['tag'] = 'Short List';

        $applications = $this->applicationsRepository->update($tag, $id);

        Flash::success('Application added to short list  successfully.');

        return redirect(route('applications.index'));
    }
}
