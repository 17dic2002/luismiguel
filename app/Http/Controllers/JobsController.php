<?php

namespace App\Http\Controllers;

use App\DataTables\JobsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJobsRequest;
use App\Http\Requests\UpdateJobsRequest;
use App\Repositories\JobsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\File;
use Response;

class JobsController extends AppBaseController
{
    /** @var  JobsRepository */
    private $jobsRepository;

    public function __construct(JobsRepository $jobsRepo)
    {
        $this->jobsRepository = $jobsRepo;
    }

    /**
     * Display a listing of the Jobs.
     *
     * @param JobsDataTable $jobsDataTable
     * @return Response
     */
    public function index(JobsDataTable $jobsDataTable)
    {
        return $jobsDataTable->render('jobs.index');
    }

    /**
     * Show the form for creating a new Jobs.
     *
     * @return Response
     */
    public function create()
    {
        return view('jobs.create');
    }

    /**
     * Store a newly created Jobs in storage.
     *
     * @param CreateJobsRequest $request
     *
     * @return Response
     */
    public function store(CreateJobsRequest $request)
    {
        $input = $request->all();

        $input['description']->move(public_path('uploads/jobs'), $input['description']->getClientOriginalName());
        $input['description'] = $input['description']->getClientOriginalName();

        $jobs = $this->jobsRepository->create($input);
        MailsController::newJobPosting($jobs);

        Flash::success('Jobs saved successfully.');

        return redirect(route('jobs.index'));
    }

    /**
     * Display the specified Jobs.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jobs = $this->jobsRepository->find($id);

        if (empty($jobs)) {
            Flash::error('Jobs not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.show')->with('jobs', $jobs);
    }

    /**
     * Show the form for editing the specified Jobs.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jobs = $this->jobsRepository->find($id);

        if (empty($jobs)) {
            Flash::error('Jobs not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.edit')->with('jobs', $jobs);
    }

    /**
     * Update the specified Jobs in storage.
     *
     * @param int $id
     * @param UpdateJobsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobsRequest $request)
    {
        $jobs = $this->jobsRepository->find($id);

        if (empty($jobs)) {
            Flash::error('Jobs not found');
            return redirect(route('jobs.index'));
        }

        $input = $request->all();

        if(!isset($input['internal'])){
            $input['internal'] = 0;
        }

        if ($input['stop_publish'] === null or $input['start_publish'] === null) {
            $input['stop_publish'] = $jobs->stop_publish;
            $input['start_publish'] = $jobs->start_publish;
        }

        if (isset($input['description'])) {

            File::delete(public_path('uploads/jobs') . '/' . $jobs->description);

            $input['description']->move(public_path('uploads/jobs'), $input['description']->getClientOriginalName());
            $input['description'] = $input['description']->getClientOriginalName();
        }
        $jobs = $this->jobsRepository->update($input, $id);

        Flash::success('Jobs updated successfully.');

        return redirect(route('jobs.index'));
    }

    /**
     * Remove the specified Jobs from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $jobs = $this->jobsRepository->find($id);

        if (empty($jobs)) {
            Flash::error('Jobs not found');

            return redirect(route('jobs.index'));
        }

        File::delete(public_path('uploads/jobs') . '/' . $jobs->description);

        $this->jobsRepository->delete($id);

        Flash::success('Jobs deleted successfully.');

        return redirect(route('jobs.index'));
    }
}
