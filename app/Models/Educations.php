<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Educations
 * @package App\Models
 * @version November 18, 2019, 7:27 pm UTC
 *
 * @property \App\Models\Applications application
 * @property string institute
 * @property string study_area
 * @property string certificate
 * @property string other
 * @property string date_from
 * @property string date_to
 * @property integer application_id
 */
class Educations extends Model
{
    use SoftDeletes;

    public $table = 'educations';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute',
        'study_area',
        'certificate',
        'other',
        'date_from',
        'date_to',
        'application_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute' => 'string',
        'study_area' => 'string',
        'certificate' => 'string',
        'other' => 'string',
        'date_from' => 'date',
        'date_to' => 'date',
        'application_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'institute' => 'required',
        'study_area' => 'required',
        'certificate' => 'required',
        'other' => 'required',
        'date_from' => 'required',
        'date_to' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function application()
    {
        return $this->belongsTo(\App\Models\Applications::class, 'application_id');
    }
}
