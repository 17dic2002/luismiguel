<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WorkExperiences
 * @package App\Models
 * @version November 18, 2019, 7:51 pm UTC
 *
 * @property \App\Models\Applications application
 * @property string employer_name
 * @property string position
 * @property string responsabilities
 * @property string supervisors_name
 * @property string position_start_date
 * @property string position_end_date
 * @property string reason_leaving
 * @property string phone
 * @property string addresss
 * @property integer application_id
 */
class WorkExperiences extends Model
{
    use SoftDeletes;

    public $table = 'work_experiences';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'employer_name',
        'position',
        'responsabilities',
        'supervisors_name',
        'position_start_date',
        'position_end_date',
        'reason_leaving',
        'phone',
        'address',
        'application_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'employer_name' => 'string',
        'position' => 'string',
        'responsabilities' => 'string',
        'supervisors_name' => 'string',
        'position_start_date' => 'date',
        'position_end_date' => 'date',
        'reason_leaving' => 'string',
        'phone' => 'string',
        'address' => 'string',
        'application_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'employer_name' => 'required',
        'position' => 'required',
        'responsabilities' => 'required|max:2000',
        'supervisors_name' => 'required',
        'position_start_date' => 'required',
        'position_end_date' => 'required',
        'reason_leaving' => 'required|max:2000',
        'phone' => 'required',
        'address' => 'required',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function application()
    {
        return $this->belongsTo(\App\Models\Applications::class, 'application_id');
    }
}
