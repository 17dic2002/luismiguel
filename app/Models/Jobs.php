<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Jobs
 * @package App\Models
 * @version November 14, 2019, 4:26 am UTC
 *
 * @property \App\Models\RequestAds requestAds
 * @property \Illuminate\Database\Eloquent\Collection applications
 * @property string title
 * @property string location
 * @property string status
 * @property string description
 * @property string stop_publish
 * @property string start_publish
 */
class Jobs extends Model
{
    use SoftDeletes;

    public $table = 'jobs';


    //protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'location',
        'status',
        'description',
        'all_jobs',
        'internal',
        'stop_publish',
        'start_publish'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'location' => 'string',
        'status' => 'string',
        'description' => 'string',
        'internal' => 'boolean',
        'all_jobs' => 'boolean',
        'stop_publish' => 'date',
        'start_publish' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'location' => 'required',
        'status' => 'required',
        'description' => 'file',

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function requestAds()
    {
        return $this->hasOne(\App\Models\RequestAds::class, 'job_id')->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function applications()
    {
        return $this->hasMany(\App\Models\Applications::class, 'job_id');
    }
}
