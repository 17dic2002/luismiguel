<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Options extends Model
{
    use SoftDeletes;
    public $table = 'options';
    protected $dates = ['deleted_at'];



    public $fillable = [
        'admin_email',
        'media_add_email',
        'request_add_email',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'admin_email' => 'string',
        'traditional_ad_email' => 'string',
        'digital_ad_email' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'admin_email' => 'required',
        'traditional_ad_email' => 'required',
        'digital_ad_email' => 'required',

    ];
}
