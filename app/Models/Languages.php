<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Languages
 * @package App\Models
 * @version November 18, 2019, 7:48 pm UTC
 *
 * @property \App\Models\Applications application
 * @property string language
 * @property string understanding
 * @property string writing
 * @property string reading
 * @property string where_learn
 * @property integer application_id
 */
class Languages extends Model
{
    use SoftDeletes;

    public $table = 'languages';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'language',
        'understanding',
        'writing',
        'reading',
        'where_learn',
        'application_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'language' => 'string',
        'understanding' => 'string',
        'writing' => 'string',
        'reading' => 'string',
        'where_learn' => 'string',
        'application_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'language' => 'required',
        'understanding' => 'required',
        'writing' => 'required',
        'reading' => 'required',
        'where_learn' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function application()
    {
        return $this->belongsTo(\App\Models\Applications::class, 'application_id');
    }
}
