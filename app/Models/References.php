<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class References
 * @package App\Models
 * @version November 18, 2019, 7:48 pm UTC
 *
 * @property \App\Models\Applications application
 * @property string name_referee
 * @property string job_title
 * @property string organization
 * @property string relation
 * @property string phone_number
 * @property string address
 * @property integer application_id
 */
class References extends Model
{
    use SoftDeletes;

    public $table = 'references';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name_referee',
        'job_title',
        'organization',
        'relation',
        'phone_number',
        'address',
        'application_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name_referee' => 'string',
        'job_title' => 'string',
        'organization' => 'string',
        'relation' => 'string',
        'phone_number' => 'string',
        'address' => 'string',
        'application_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        /*'name_referee' => 'required',
        'job_title' => 'required',
        'organization' => 'required',
        'relation' => 'required',
        'phone_number' => 'required',
        'address' => 'required',*/
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function application()
    {
        return $this->belongsTo(\App\Models\Applications::class, 'application_id');
    }
}
