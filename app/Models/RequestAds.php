<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RequestAds
 * @package App\Models
 * @version November 14, 2019, 4:23 am UTC
 *
 * @property \App\Models\Jobs jobs
 * @property string requester_name
 * @property string requester_email
 * @property string print_add
 * @property string name_publication
 * @property string ad_size
 * @property string date_publication
 * @property string template_print
 * @property string comments_template
 * @property string book_media
 * @property string due_date
 * @property string blue_print
 * @property string budget
 * @property string comments_bp
 * @property string social_media
 * @property string platforms
 * @property string other_platform
 * @property string template_social
 * @property integer job_id
 */
class RequestAds extends Model
{
    use SoftDeletes;

    public $table = 'request_ads';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'requester_name',
        'requester_email',
        'print_add',
        'name_publication',
        'ad_size',
        'date_publication',
        'template_print',
        'comments_template',
        'book_media',
        'due_date',
        'blue_print',
        'budget',
        'comments_bp',
        'social_media',
        'platforms',
        'other_platform',
        'color_type',
        'job_id' ,
        'request_ads',
        'orientation',
        'colour',
        'date_finish',
        'name_publication_2',
        'ad_size_2',
        'date_publication_2',
        'date_finish_2',
        'template_print_2',
        'orientation_2',
        'colour_2',
        'comments_template_2',
        'name_publication_3',
        'ad_size_3',
        'date_publication_3',
        'date_finish_3',
        'template_print_3',
        'orientation_3',
        'colour_3',
        'comments_template_3',
        'submission_deadline',
        'submission_deadline2',
        'submission_deadline3',
        'budget_social',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'requester_name' => 'string',
        'requester_email' => 'string',
        'print_add' => 'string',
        'name_publication' => 'string',
        'ad_size' => 'string',
        'template_print' => 'string',
        'comments_template' => 'string',
        'color_type' => 'string',
        'book_media' => 'string',
        'due_date' => 'string',
        'blue_print' => 'string',
        'budget' => 'string',
        'comments_bp' => 'string',
        'social_media' => 'string',
        'platforms' => 'string',
        'other_platform' => 'string',
        'template_social' => 'string',
        'submission_deadline' => 'string',
        'submission_deadline2' => 'string',
        'submission_deadline3' => 'string',
        'budget_social' => 'string',
        'job_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'requester_name' => 'required',
        'requester_email' => 'required',
        'print_add' => 'required',
        'social_media' => 'required',
        'job_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function jobs()
    {
        return $this->belongsTo(\App\Models\Jobs::class, 'job_id')->withTrashed();;
    }
}
