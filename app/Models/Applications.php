<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Applications
 * @package App\Models
 * @version November 15, 2019, 4:00 am UTC
 *
 * @property \App\Models\Jobs job
 * @property \Illuminate\Database\Eloquent\Collection educations
 * @property \Illuminate\Database\Eloquent\Collection wordExperiences
 * @property \Illuminate\Database\Eloquent\Collection skills
 * @property \Illuminate\Database\Eloquent\Collection references
 * @property \Illuminate\Database\Eloquent\Collection languages
 * @property string application_title
 * @property string first_name
 * @property string middle_name
 * @property string last_name
 * @property string email
 * @property string home_phone
 * @property string mobile_number
 * @property string gender
 * @property string category
 * @property string other_category
 * @property string type
 * @property string years_of_experience
 * @property string date_of_birth
 * @property string currency
 * @property string current_salary
 * @property string period_cs
 * @property string currency_expected
 * @property string expected_salary
 * @property string period_es
 * @property string driver_license
 * @property string address
 * @property string resume
 * @property string list_skills
 * @property string professional_qualifications_skills
 * @property string other_relevant_skills
 * @property string hobbies
 * @property string how_hear
 * @property string tag
 * @property string step
 * @property integer job_id
 */
class Applications extends Model
{
    use SoftDeletes;

    public $table = 'applications';


    protected $dates = ['deleted_at'];



    public $fillable = [

        'application_title',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'home_phone',
        'mobile_number',
        'gender',
        'category',
        'other_category',
        'type',
        'years_of_experience',
        'date_of_birth',
        'currency',
        'current_salary',
        'period_cs',
        'currency_expected',
        'expected_salary',
        'period_es',
        'driver_license',
        'address',
        'resume',
        'list_skills',
        'professional_qualifications_skills',
        'other_relevant_skills',
        'hobbies',
        'how_hear',
        'tag',
        'step',
        'job_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'application_title' => 'string',
        'first_name' => 'string',
        'middle_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'home_phone' => 'string',
        'mobile_number' => 'string',
        'gender' => 'string',
        'category' => 'string',
        'other_category' => 'string',
        'type' => 'string',
        'years_of_experience' => 'string',
        'date_of_birth' => 'date',
        'currency' => 'string',
        'current_salary' => 'string',
        'period_cs' => 'string',
        'currency_expected' => 'string',
        'expected_salary' => 'string',
        'period_es' => 'string',
        'driver_license' => 'string',
        'address' => 'string',
        'resume' => 'string',
        'list_skills' => 'string',
        'professional_qualifications_skills' => 'string',
        'other_relevant_skills' => 'string',
        'hobbies' => 'string',
        'how_hear' => 'string',
        'tag' => 'string',
        'step' => 'integer',
        'job_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_title' => 'required',
        'first_name' => 'required',
        'middle_name' => 'required',
        'last_name' => 'required',
        'email' => 'required',
        'category' => 'required',
        'other_category' => 'required',
        'type' => 'required',
        'years_of_experience' => 'required',
        'date_of_birth' => 'required',
        'currency' => 'required',
        'current_salary' => 'required',
        'period_cs' => 'required',
        'currency_expected' => 'required',
        'expected_salary' => 'required',
        'period_es' => 'required',
        'driver_license' => 'required',
        'address' => 'required',
        'resume' => 'required',
        'list_skills' => 'required',
        'professional_qualifications_skills' => 'required',
        'other_relevant_skills' => 'required',
        'hobbies' => 'required',
        'how_hear' => 'required',
        'job_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function job()
    {
        return $this->belongsTo(\App\Models\Jobs::class, 'job_id')->withTrashed();;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function educations()
    {
        return $this->hasMany(\App\Models\Educations::class, 'application_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function workExperiences()
    {
        return $this->hasMany(\App\Models\WorkExperiences::class, 'application_id');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function references()
    {
        return $this->hasMany(\App\Models\References::class, 'application_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function languages()
    {
        return $this->hasMany(\App\Models\Languages::class, 'application_id');
    }
}
